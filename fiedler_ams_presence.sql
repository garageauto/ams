-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 24 Octobre 2018 à 17:08
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fiedler_ams_presence`
--

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `id` int(11) NOT NULL,
  `actif` int(1) DEFAULT '1',
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `datenaiss` date NOT NULL,
  `code` varchar(4) NOT NULL,
  `dateentree` date DEFAULT '2018-01-01',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `employes`
--

INSERT INTO `employes` (`id`, `actif`, `nom`, `prenom`, `tel`, `photo`, `datenaiss`, `code`, `dateentree`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'TOMETY', 'Teko Joshua Thierry', '91260777', 'prod_5bccf88eed06c.png', '1990-10-19', '0000', '2018-01-01', '2018-10-18 20:37:13', '2018-10-21 22:07:12', NULL),
(2, 1, 'ABALO', 'Koukou', '90 00 00 00', 'prod_5bcb01ef7c10d.png', '1997-10-19', '0001', '2018-01-01', '2018-10-20 10:22:42', '2018-10-21 16:56:22', NULL),
(3, 1, 'AMAH', 'Kwatcha', '99 99 99 99', 'prod_5bcb0618884a0.jpg', '1990-10-03', '0002', '2018-01-01', '2018-10-20 10:40:24', '2018-10-21 17:00:33', NULL),
(4, 1, 'Doe', 'John', '91260777', 'prod_5bcb08a884651.jpg', '1993-10-10', '3030', '2018-01-01', '2018-10-20 10:51:20', '2018-10-20 10:51:20', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `journees`
--

CREATE TABLE `journees` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL,
  `heuredeb_taf` time DEFAULT NULL,
  `heurefin_taf` time DEFAULT NULL,
  `heuredeb_pause` time DEFAULT NULL,
  `heurefin_pause` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `journees`
--

INSERT INTO `journees` (`id`, `type`, `date`, `heuredeb_taf`, `heurefin_taf`, `heuredeb_pause`, `heurefin_pause`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '2018-10-22', '07:30:00', '18:00:00', '12:00:00', '14:00:00', '2018-10-20 12:45:40', '2018-10-20 14:34:28', NULL),
(2, 2, '2018-10-21', '08:00:00', '18:00:00', '12:00:00', '14:00:00', '2018-10-20 13:15:04', '2018-10-20 13:46:32', NULL),
(3, 1, '2018-10-20', '08:00:00', '18:00:00', '12:00:00', '14:00:00', '2018-10-20 13:35:54', '2018-10-20 13:35:54', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `urlmenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nommenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iconemenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `newsletter` int(1) NOT NULL DEFAULT '0',
  `fr` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `menus`
--

INSERT INTO `menus` (`id`, `urlmenu`, `nommenu`, `iconemenu`, `menu_id`, `newsletter`, `fr`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'administration', NULL, NULL, 0, 'Administration', '2018-01-29 14:42:52', '2018-01-29 14:42:52', NULL),
(3, 'menus', 'menus', NULL, 1, 0, 'menus', '2018-01-29 14:46:01', '2018-01-29 14:46:01', NULL),
(16, NULL, 'securite', NULL, NULL, 0, 'securite', '2018-01-30 21:12:24', '2018-01-30 21:12:50', NULL),
(17, 'users', 'users', NULL, 16, 0, 'utilisateurs', '2018-01-30 21:13:29', '2018-01-30 21:14:42', NULL),
(18, 'mouchards', 'mouchards', NULL, 16, 0, 'journal', '2018-01-30 21:13:58', '2018-01-30 21:13:58', NULL),
(19, 'profils', 'profils', NULL, 16, 0, 'profils', '2018-01-30 21:14:12', '2018-01-30 21:14:12', NULL),
(21, 'journees', 'journees', NULL, 1, 0, 'journées', '2018-10-18 20:19:37', '2018-10-18 20:21:46', '2018-10-18 20:21:46'),
(22, 'journees', 'journees', NULL, 1, 0, 'journées', '2018-10-18 20:19:38', '2018-10-18 20:19:38', NULL),
(23, 'employes', 'employes', NULL, 1, 0, 'employes', '2018-10-18 20:20:24', '2018-10-18 20:21:36', '2018-10-18 20:21:36'),
(24, 'employes', 'employes', NULL, 1, 0, 'employes', '2018-10-18 20:20:25', '2018-10-18 20:20:25', NULL),
(25, 'parametres', 'parametres', NULL, 1, 0, 'parametres', '2018-10-18 20:20:41', '2018-10-18 20:20:41', NULL),
(26, 'parametres', 'parametres', NULL, 1, 0, 'parametres', '2018-10-18 20:20:42', '2018-10-18 20:21:26', '2018-10-18 20:21:26'),
(27, 'permissions', 'permissions', NULL, 1, 0, 'permissions', '2018-10-18 20:21:00', '2018-10-18 20:21:00', NULL),
(28, 'permissions', 'permissions', NULL, 1, 0, 'permissions', '2018-10-18 20:21:01', '2018-10-18 20:21:14', '2018-10-18 20:21:14'),
(29, 'presences', 'presences', NULL, 1, 0, 'presences', '2018-10-18 20:22:20', '2018-10-20 12:29:57', '2018-10-20 12:29:57'),
(30, 'presences', 'presences', NULL, 1, 0, 'presences', '2018-10-18 20:22:21', '2018-10-18 20:22:32', '2018-10-18 20:22:32'),
(31, NULL, 'dsf', NULL, NULL, 0, 'ewr', '2018-10-18 20:24:22', '2018-10-18 20:24:40', '2018-10-18 20:24:40'),
(32, NULL, 'dsf', NULL, NULL, 0, 'ewr', '2018-10-18 20:24:23', '2018-10-18 20:24:42', '2018-10-18 20:24:42'),
(33, NULL, 'ewrwer', NULL, NULL, 0, 'esr', '2018-10-18 20:26:08', '2018-10-18 20:26:19', '2018-10-18 20:26:19'),
(34, NULL, 'etats', NULL, NULL, 0, 'etats', '2018-10-22 18:45:34', '2018-10-22 18:45:34', NULL),
(35, 'etatpresence', 'etat-des-presences', NULL, 34, 0, 'etat des présences', '2018-10-22 18:46:13', '2018-10-22 18:46:31', NULL),
(36, 'journalentrees', 'journal-des-presences', NULL, 34, 0, 'Journal des presences', '2018-10-22 21:01:02', '2018-10-22 21:01:02', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `mouchards`
--

CREATE TABLE `mouchards` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `modele` varchar(254) DEFAULT NULL,
  `etat` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `parametres`
--

CREATE TABLE `parametres` (
  `id` int(11) NOT NULL,
  `heuredeb_taf` time NOT NULL,
  `heurefin_taf` time NOT NULL,
  `heuredeb_pause` time NOT NULL,
  `heurefin_pause` time NOT NULL,
  `raisonsociale` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `adresse` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `parametres`
--

INSERT INTO `parametres` (`id`, `heuredeb_taf`, `heurefin_taf`, `heuredeb_pause`, `heurefin_pause`, `raisonsociale`, `logo`, `adresse`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '08:00:00', '18:00:00', '12:00:00', '14:00:00', 'Fiedler AMS', NULL, '-', '2018-10-20 12:24:56', '2018-10-20 12:24:56', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `motif` text,
  `datedeb` date NOT NULL,
  `datefin` date NOT NULL,
  `employe_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `permissions`
--

INSERT INTO `permissions` (`id`, `motif`, `datedeb`, `datefin`, `employe_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'lkjlk', '2018-10-18', '2018-01-22', 1, '2018-10-18 21:17:03', '2018-10-18 21:17:03', NULL),
(2, NULL, '2018-10-20', '2018-10-20', 3, '2018-10-20 14:50:13', '2018-10-20 14:51:42', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `presences`
--

CREATE TABLE `presences` (
  `id` int(11) NOT NULL,
  `journee_id` int(11) NOT NULL,
  `employe_id` int(11) NOT NULL,
  `heure_deb` time NOT NULL,
  `heure_fin` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `presences`
--

INSERT INTO `presences` (`id`, `journee_id`, `employe_id`, `heure_deb`, `heure_fin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, '10:00:00', '18:00:00', '2018-10-21 22:58:25', '2018-10-21 23:00:47', NULL),
(2, 2, 2, '11:02:00', '11:02:00', '2018-10-21 23:02:37', '2018-10-21 23:02:55', NULL),
(3, 2, 2, '11:07:00', '11:07:00', '2018-10-21 23:07:03', '2018-10-21 23:07:41', NULL),
(4, 2, 2, '11:11:00', '11:11:00', '2018-10-21 23:11:25', '2018-10-21 23:11:40', NULL),
(5, 2, 2, '11:23:00', '11:27:00', '2018-10-21 23:23:01', '2018-10-21 23:27:40', NULL),
(6, 2, 2, '11:27:00', '11:28:00', '2018-10-21 23:27:48', '2018-10-21 23:28:03', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `profils`
--

CREATE TABLE `profils` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomprofil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descprofil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `profils`
--

INSERT INTO `profils` (`id`, `nomprofil`, `descprofil`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', NULL, '2018-01-31 21:31:53', '2018-01-31 21:31:53', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `profil_menu`
--

CREATE TABLE `profil_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `autorisation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `profil_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `terminals`
--

CREATE TABLE `terminals` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `mac` varchar(255) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `dateactivation` datetime DEFAULT NULL,
  `datedesactivation` datetime DEFAULT NULL,
  `profil_id` int(10) UNSIGNED DEFAULT NULL,
  `personne_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `login`, `statut`, `email`, `password`, `actif`, `dateactivation`, `datedesactivation`, `profil_id`, `personne_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin', NULL, '$2y$10$xjqzYBH0qHX0yrdPUX0bOuZnCMFLekT5TLHtWRfpYwjMRPFtIPtAy', 1, '2017-09-22 20:18:30', NULL, 1, 3, 'vA4suTU8HlHrUHE7BsNScItn0RE8HlWGOjdbc8AweMFoXLxHmPQbimNgKDb1', '2017-07-10 10:16:39', '2018-10-24 17:03:49', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `journees`
--
ALTER TABLE `journees`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_menu_id_foreign` (`menu_id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `mouchards`
--
ALTER TABLE `mouchards`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `parametres`
--
ALTER TABLE `parametres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `presences`
--
ALTER TABLE `presences`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `profil_menu`
--
ALTER TABLE `profil_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profil_menu_menu_id_foreign` (`menu_id`),
  ADD KEY `profil_menu_profil_id_foreign` (`profil_id`);

--
-- Index pour la table `terminals`
--
ALTER TABLE `terminals`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_profil_id_foreign` (`profil_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `employes`
--
ALTER TABLE `employes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `journees`
--
ALTER TABLE `journees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `mouchards`
--
ALTER TABLE `mouchards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `parametres`
--
ALTER TABLE `parametres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `presences`
--
ALTER TABLE `presences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `profils`
--
ALTER TABLE `profils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `profil_menu`
--
ALTER TABLE `profil_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `terminals`
--
ALTER TABLE `terminals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `profil_menu`
--
ALTER TABLE `profil_menu`
  ADD CONSTRAINT `profil_menu_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profil_menu_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `profils` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `profils` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
