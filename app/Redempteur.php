<?php
/**
 * Created by Thierry Tomety & Gabriel Agbonon.
 * Date: 17/06/2015
 */

namespace App;


class Redempteur
{


    public static function getAge($datenaiss, $dateref)
    {
        $dn = explode('-', $datenaiss);
        $dr = explode('-', $dateref);
        return $dr[0] - $dn[0];
    }

    public static function getUrl($grandurl){
        $tab=explode('/',$grandurl);
        $str=$tab[3];
        for($i=4;$i<count($tab);$i++){
            $str=$str.'/'.$tab[$i];
        }
        return $str;
    }

    public static function valideTime($time,$bool=false){
        $tab=explode(':',$time);
        if($time=='' && !$bool) return true;
        if($time=='' && $bool) return false;
        if(($tab[0]<25 && $tab[1]<61 && $tab[1]<61)){
            return true;
        }
        else{
            return false;
        }
    }

    public static function getDure($d1,$d2)
    {
        $nbSecondes = 60 * 60 * 24;
        $debut_ts = strtotime($d1);
        $fin_ts = strtotime($d2);
        $diff = $fin_ts - $debut_ts;
        if ($diff < 0) return -1;
        else return round($diff / $nbSecondes).' jrs';
    }
	
	

    public static function getDureInt2($d1,$d2)
    {
        $nbSecondes = 60 * 60;
        $debut_ts = strtotime($d1);
        $fin_ts = strtotime($d2);
        $diff = $fin_ts - $debut_ts;
        if ($diff < 0) return 0;
        else return round($diff / $nbSecondes);
    }
	
	public static function getDureInt($d1,$d2)
    {
        $nbSecondes = 60 * 60 * 24;
        $debut_ts = strtotime($d1);
        $fin_ts = strtotime($d2);
        $diff = $fin_ts - $debut_ts;
        if ($diff < 0) return 0;
        else return round($diff / $nbSecondes);
    }

    public static function getDureAnnee($d1,$d2)
    {
        $nbSecondes = 60 * 60 * 24;
        $debut_ts = strtotime($d1);
        $fin_ts = strtotime($d2);
        $diff = $fin_ts - $debut_ts;
        if ($diff < 0) return -1;
        else return round($diff / ($nbSecondes*365)).' ans';
    }

    public static function formater($dat){
        $tab=explode('/',$dat);
        $annee=$tab[2];
        $mois=$tab[1];
        $jour=$tab[0];
        return $annee.'-'.$mois.'-'.$jour;
    }

    public static function affiche_mois($k)
    {
        switch ($k)
        {
            case 1: return  ' Janvier';break;case 2: return  ' Février';break;case 3: return  ' Mars';break;case 4: return  ' Avril';break;case 5: return  ' Mai';break;case 6: return  ' Juin';break;case 7: return  ' Juillet'; break;case 8: return  ' Août';break;case 9: return  ' Septembre';break;case 10: return  ' Octobre';break;case 11: return  ' Novembre';break;case 12: return  ' Décembre';break;
        }
    }

    public static function getMois($val){
        if($val==12) $rep='Dec.';elseif($val==11) $rep='Nov.';elseif($val==10) $rep='Oct.';
        elseif($val==9) $rep='Sept.';elseif($val==8) $rep='Août';elseif($val==7) $rep='Juillet';
        elseif($val==6) $rep='Juin';elseif($val==5) $rep='Mai';elseif($val==4) $rep='Avril';
        elseif($val==3) $rep='Mars';elseif($val==2) $rep='Fev.';elseif($val==1) $rep='Janv.';
        return $rep;
    }

    public static function affiche_date($d1)
    {
        $dd=explode('-',$d1);
        $ad=(int)($dd[0]);
        $md=(int)($dd[1]);
        $jd=(int)($dd[2]);
        return $s=$jd.' '.Redempteur::affiche_mois($md).' '.$ad;
    }

    public static function SendSMS ($host, $port, $username, $password, $phoneNoRecip, $msgText) {

        /* Parameters:
           $host - IP address or host name of the NowSMS server
           $port - "Port number for the web interface" of the NowSMS Server
           $username - "SMS Users" account on the NowSMS server
           $password - Password defined for the "SMS Users" account on the NowSMS Server
           $phoneNoRecip - One or more phone numbers (comma delimited) to receive the text message
           $msgText - Text of the message
        */

        $fp = fsockopen($host, $port, $errno, $errstr);
        if (!$fp) {
            echo "errno: $errno \n";
            echo "errstr: $errstr\n";
            return $result;
        }
        fwrite($fp, "GET /?Phone=" . rawurlencode($phoneNoRecip) . "&Text=" .
            rawurlencode($msgText) . " HTTP/1.0\n");
        if ($username != "") {
            $auth = $username . ":" . $password;
            $auth = base64_encode($auth);
            fwrite($fp, "Authorization: Basic " . $auth . "\n");
        }

        fwrite($fp, "\n");
        $res = "";
        while(!feof($fp)) {
            $res .= fread($fp,1);
        }
        fclose($fp);

        return $res;

    }

    public static function GoSendSMS($sender, $message)
    {
        $x   = Redempteur::SendSMS("127.0.0.1", 8800, "forceone", "forceone", $sender, $message) ;
        echo $x;
    }

    public static function enleverCaracteresSpeciaux($chaine)
    {
      $caracteres=[ 'a', 'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
		'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
		'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
		'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
		'Œ' => 'oe', 'œ' => 'oe',
		'$' => 's'];

	  $chaine = strtr($chaine, $caracteres);
	  $chaine = preg_replace('#[^A-Za-z0-9]+#', '-', $chaine);
	  $chaine = trim($chaine, '-');
	  $chaine = strtolower($chaine);

	    return $chaine;
      return preg_replace("#[^a-zA-Z]#", "", $m);
    }

  public static function enleverCaracteresSpeciauxSansMettreTiret($chaine)
  {
    $caracteres=[ 'a', 'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
      'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
      'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
      'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
      'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
      'Œ' => 'oe', 'œ' => 'oe',
      '$' => 's'];

    $chaine = strtr($chaine, $caracteres);
    $chaine = preg_replace('#[^A-Za-z0-9]+#', ' ', $chaine);

    return $chaine;
  }
}
