<?php namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Publicite;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Menu extends Base {

  /*
  * Fonctionnalites de base ecrites par thierry tomety
  * pour la gestion des profils utilisateurs et des droits associes
  */

  public $table='menus';
  use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $softDelete = true;


  public $required_on_inserts=['nommenu'];
  public $other_inputs_on_inserts=['fr','en','ar','es','title_fr','title_en','title_ar','title_es','iconemenu','urlmenu','menu_id','disposition_id','newsletter'];
  //nombre d'article a charger par default pour chaque menu
  public $nbr_article_charge=100;
  public $nbr_actualite_charge=9;
  public $nbr_pub_charge=15;
  public $nbr_produits_charge=99;



  //validation
  public static  function validate($input){
    $rules = array(
      'nommenu' => 'required|min:3',

    );
    $messages = array(
      'nommenu.required'    =>  trans('validation.libelle_required'),
      'nommenu.min'         =>  trans('validation.libelle_min'),
    );

    return Validator::make($input, $rules, $messages);
  }

  public static function  principaux()
  {
    return self::select('*')->whereNull('menu_id')->orderby('nommenu')->get();

  }

  //autres fonctions

    public function menu(){
      return $this->belongsTo('App\Menu');
    }


    public function sousmenus(){
        return $this->hasMany('App\Menu')->orderBy('nommenu');
    }

  public function touslessousmenus(){
    return $this->hasMany('App\Menu')->orderBy('nommenu');
  }



  public static function nbrsArticles(){
    $menus=Article::select('*')
      ->count();
    return $menus;
  }

  public static function nbrInternautes(){
    $menus=Internaute::select('*')
      ->count();
    return $menus;
  }
  public static function nbrInternautesAvecAvis(){
    $menus=Internaute::select('*')
      ->where('advice','>',0)
      ->count();
    return $menus;
  }

  public static function noteInternautes(){
    $menus=Internaute::select('advice')
      ->where('advice','>',0)
      ->sum('advice');
    $i=self::nbrInternautesAvecAvis(); if($i<=0) $i=1;
    return (int) $menus / $i ;
  }

  public static function noteInternautesDetail(){
    $nbr_etoiles=5; $avis=[];
    for($i=1;$i<=$nbr_etoiles;$i++){
      $avis[$i]=Internaute::select('id')->where('advice',$i)->count();
    }
    return  collect($avis);  ;
  }
public static function derniersAvis($page=0,$limit=10){
    $avis=Internaute::select('*')->where('advice','>',0 )->where('message','<>','' )->whereNotNull('message')
      ->orderby('updated_at','desc')
      ->skip($page)
      ->take($limit)
      ->get();
    return  collect($avis);
  }

  public static function nbrAnnonceurs(){
    $menus=Annonceur::select('*')
      ->count();
    return $menus;
  }




    public static function getMenuByName($name){

        $menu=Self::select('*')
            ->where('nommenu','=',''.$name.'')
            ->first();
        return $menu;
    }

    public function getName(){return $this->nommenu;}


  /*nouveau */

    //droit d'acces a un menu avec en passant en parametre le code de son autorisation

    public static function droitMenu($name){
        if(Auth::user() && Auth::user()->statut=='admin') return 'clamis';
        $profil=0;
        if(Auth::user()!= null && Auth::user()->currentProfil()!=null) { $profil=Auth::user()->currentProfil()->id; } else {return 0;}
        $menu=Self::select('*')
            ->where('urlmenu','=',''.$name.'')
            ->first();


        if($menu!=null){
          $droit=DB::table('profil_menu')
            ->where('menu_id','=',''.$menu->id.'')
            ->where('profil_id','=',''.$profil.'')
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->pluck('autorisation');
          return $droit;
        }

    }

    public static function droitLecture($name){
        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'l')) return 1; else return 0;
    }

    public static function droitAjout($name){
        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'a')) return 1; else return 0;
    }

    public static function droitModification($name){

        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'m')) return 1; else return 0;
    }

    public static function droitSuppression($name){
        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'s')) return 1; else return 0;
    }

    public static function droitImport($name){
        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'i')) return 1; else return 0;
    }

    public static function droitConfirmation($name){
        $droit=Menu::droitMenu($name);
        if (strrchr($droit,'e')) return 1; else return 0;
    }


/*nouveau */
}
