<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Employe extends Base {

    //
    public $table='employes';
   	use SoftDeletes;
    public $storefolder='public/employes';
  	public $required_on_inserts=['nom','dateentree'];


  	public $other_inputs_on_inserts=['actif','prenom','photo','datenaiss','code',	'tel'];

  //validation
 	public static  function validate($input){
	    $rules = array(

	    );
	    $messages = array(

	    );

	    return Validator::make($input, $rules, $messages);
  	}

	 
    public static function getone(){return self::find(1);}

}
