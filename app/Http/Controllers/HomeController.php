<?php namespace App\Http\Controllers;

use App\Personne;
use App\Repositories\ImageFile;


class HomeController extends Controller {
  private $backend_dir='backend.';
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index2()
	{
		return view('backend.index');
	}
	public function index()
	{
		return view('frontend.components.home');
	}

	public function DownloadMiniPicture($taille,$type,$filename ){
		//pourcentage de réduction
    $div=1;
    $a=\App\User::getDeviceAgent();
    if($a->isMobile() && $taille>20 ) $div=4;
    elseif($a->isTablet() && $taille>40 ) $div=2;
		$pourcentage = $taille/$div;

		$filepath = public_path('storage/app/public/'.$type).'/'. $filename;
		$pathto ='public/'.$type.'/mini-'.$pourcentage.'/'.$filename;

		$path = public_path('storage/app/public/'.$type).'/mini-'.$pourcentage.'/'. $filename ;

		$itemsavage = new ImageFile();
		if (file_exists($path)) {

		   return response()->download($path);
		}else{
		  	$itemsavage->moveExistingTo($pourcentage,$filepath, $pathto);
		  	return response()->download($path);

		}

	}

}
