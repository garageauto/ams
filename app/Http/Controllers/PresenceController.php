<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Presence\PresenceRepository;
use Illuminate\Http\Request;
use App\Redempteur;
use App\Journee;
use App\Employe;
use App\Presence;

class PresenceController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(PresenceRepository $presence)
  {
    $this->presence = $presence;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet presences sera null
    //mais nous recuperons neamoins la liste des 'presencess' disponibles
    $presence = Null;
    $presences = $this->presence->getAll();
    return view($this->backend_dir.'presence.context',compact('presence','presences'));
  }
  
  
  public function etat(Request $request)
  {
    
    $nbrheures = 0;
    $datedeb = '';
    $datefin = '';
    $employes = Employe::all();
	
    if($request->has('datedeb')) $datedeb=$request->get('datedeb');
    if($request->has('datefin')) $datefin=$request->get('datefin');
	if($datedeb != '' && $datefin != ''){
	$nbrheures = Presence::getNbrHeuresPeriode($datedeb,$datefin);
	}
    return view($this->backend_dir.'presence.journal',compact('nbrheures','datedeb','datefin','employes'));
  }
public function journalentrees(Request $request)
  {
    
    $datedeb = '';
    $presences = Presence::all();
	
    if($request->has('datedeb')) {
		$datedeb=$request->get('datedeb');
		 $presences=Presence::findPresence3($datedeb);
	}
    return view($this->backend_dir.'presence.journalentrees',compact('datedeb','presences'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $presence = $this->presence->find($id);
    $presences = $this->presence->getAll();
    return view($this->backend_dir.'presence.context',compact('presences','presence'));
  }


  public function store(Request $request)
  {
	  
    $obj=$request->input();
	$obj['id']=0;
	if(!Journee::findDate2()) return 'Journée non programmée...';
	$journee=Journee::findDate3();
	$journee=$journee[0];
    //recuperer l'ID de l'objet
    $id=$request->id;
	
	$employe=Employe::find($obj['employe_id']);
	if($employe==null or $employe->actif!=1) return 'Action impossible';
    $v=$this->presence->validate($obj);
    if($v->passes()){
      //en ajout
	 $obj['journee_id']=$journee->id;
	 
	 if($obj['code']!=$employe->code) return 'Code invalide';
	 
	 $presence=Presence::findPresence($obj['journee_id'],$obj['employe_id']);
	 if(count($presence)==0){
		 $obj['heure_deb']=date('h:i');
	 }else{
		 $presence=$presence[0];
		 $obj['id']=$presence->id;
		 $obj['heure_fin']=date('h:i');
	 }
        
      Helper::startTransaction();
      
      try {
        //en ajout
        $this->presence->saveIt($obj);

        Helper::completeTransaction();
              
        if($obj['id']==0) return trans('validation.enregistrementeffectue');
        else return 'Au revoir, et à bientôt';

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return $v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->presence->delete($id);
  }
}
