<?php
namespace App\Http\Controllers;

use App\User;
use App\Mouchard;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class UsersController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(UserRepository $user)
  {
    $this->user = $user;
  }




  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function mouchards()
  {
    $mouchards=Mouchard::journalDuJour();
    return view($this->backend_dir.'user.mouchards',[ 'mouchards'=>$mouchards  ]);
  }

  public function rapport()
  {
    $mouchards=Mouchard::journalDuJour();
    return view($this->backend_dir.'user.rapport',[ 'mouchards'=>$mouchards  ]);
  }

  public function mouchardsDate(Request $request)
  {
    $deb=$request->get('datedeb');
    $fin=$request->get('datefin');
    $mouchards=Mouchard::journalPeriode($deb,$fin);
    return view('user.mouchards',[ 'mouchards'=>$mouchards ]);
  }

  public function connexions()
  {
    $connexions=Connexion::connexionDuJour();
    return view('user.connexions',[ 'connexions'=>$connexions  ]);
  }

  public function connexionsDate(Request $request)
  {
    $deb=$request->get('datedeb');
    $fin=$request->get('datefin');
    $connexions=Connexion::journalPeriode($deb,$fin);
    return view('user.connexions',[ 'connexions'=>$connexions  ]);
  }

  public function store(Request $request)
  {
    if($request->input('id')==0){
      $v=User::validate($request->all());
      if($v->passes()){

        if($request->input('profil_id')==1 && !$request->input('client_id')>0){
            return '0'.'Veuiller selectionner l\'entreprise à laquelle vous êtes affiliée !';
        }
        else{
                if($request->input('raisonsociale')=='' || $request->input('pay_id')==0 || $request->input('mail')=='' || $request->input('tel')=='') return '0'.'Les informations relatives à l\'entreprise sont incomplètes !';
        }

        if(!Redempteur::valideTime($request->input('heureactivation')) || !Redempteur::valideTime($request->input('heuredesactivation')) || !Redempteur::valideTime($request->input('heurefin')) || !Redempteur::valideTime($request->input('heuredebut')) ){
          return '0'.'Veuillez saisir des heures correctes !';
        }

        if($request->input('password')!=$request->input('confirm')){
          return '0'.'Confirmation de mot de passe échouée !';
        }
        $u=new User();
        $u->login = strtolower($request->input('login'));
        $u->password = md5($request->input('password'));
        $u->actif =true;
        $u->statut='user';
        $datedes=null;
        $dateact=date('Y-m-d H:i:s');
        if($request->input('heuredesactivation')!=''){
          if($request->input('datedesactivation')=='') return '0'.'Veuillez renseigner la date de désactivation !';
        }

        if($request->input('heurefin')!=''){
          if($request->input('datefin')=='') return '0'.'Veuillez renseigner la date de retrait du profil !';
        }

        if($request->input('datedesactivation')!=''){
          if($request->input('heuredesactivation')!=''){
            $datedes=$request->input('datedesactivation').' '.$request->input('heuredesactivation');
          }else{
            $datedes=$request->input('datedesactivation').' '.date('H:i:s');
          }
        }

        if($request->input('dateactivation')!=''){
          if($request->input('heureactivation')!=''){
            $dateact=$request->input('dateactivation').' '.$request->input('heureactivation');
          }else{
            $dateact=$request->input('dateactivation').' '.date('H:i:s');
          }
        }

        $u->dateactivation=$dateact;
        $u->datedesactivation=$datedes;



        if($datedes!=null){
          if(strtotime(str_replace('/','-',$u->datedesactivation))<=strtotime(str_replace('/','-',$u->dateactivation))){
            return '0'.'La date et l\'heure de désactivation doit être supérieur à la date et l\'heure d\'activation !';
          }
        }

        $datedesp=null;
        if($request->input('datefin')!=''){
          if($request->input('heurefin')!=''){
            $datedesp=$request->input('datefin').' '.$request->input('heurefin');
          }else{
            $datedesp=$request->input('datefin').' '.date('H:i:s');
          }
        }
        $datedeb=$dateact;
        if ($request->has('datedebut') && $request->has('heuredebut') && $request->input('datedebut')!='' && $request->input('heuredebut') !='')
          $datedeb=Redempteur::formater($request->input('datedebut')).' '.$request->input('heuredebut');

        if($datedesp!=null){
          if(strtotime(str_replace('/','-',$datedesp))<=strtotime(str_replace('/','-',$datedeb))){
            return '0'.'La date et l\'heure de retratit doit être supérieur à la date et l\'heure d\'attribution !';
          }
        }
        if($request->has('client_id') && $request->client_id>0) $u->client_id=$request->client_id;
        if($request->has('caisse_id') && $request->caisse_id>0) $u->caisse_id=$request->caisse_id;
        if($u->save()){
          $datedeb=$dateact;
          if ($request->has('datedebut') && $request->has('heuredebut') && $request->input('datedebut')!='' && $request->input('heuredebut') !='') $datedeb=$request->input('datedebut').' '.$request->input('heuredebut');

          $u->profils()->attach($request->input('profil_id'),['datedebut'=>$datedeb, 'datefin'=>$datedesp]);
          //$action_info='Enrégistrement d\'un utilisateur';
          //Mouchard::Ecrire($action_info,$request->except('_token'));
          return '1'.'Enregistrement effectué  avec succès!';
        }
        return '0'.'Erreur lors de l\'enrégistrement !';
      }
      return '0'.$v->messages()->all()[0];
    }
    else{
      $u=User::find($request->input('id'));
      if($request->input('info')=='U'){
        if(!Redempteur::valideTime($request->input('heureactivation'),true) || !Redempteur::valideTime($request->input('heuredesactivation'))){
          return '0'.'Veuillez saisir des heures correctes !';
        }

        if($request->input('password')!=$request->input('confirm')){
          return '0'.'Confirmation de mot de passe échouée !';
        }

        if($request->input('password')!='') $u->password = md5($request->input('password'));

        $datedes=null;
        if($request->input('heuredesactivation')!=''){
          if($request->input('datedesactivation')=='') return '0'.'Veuillez renseigner la date de désactivation !';
        }

        if($request->input('heurefin')!=''){
          if($request->input('datefin')=='') return '0'.'Veuillez renseigner la date de retrait du profil !';
        }

        if($request->input('datedesactivation')!=''){
          if($request->input('heuredesactivation')!=''){
            $datedes=$request->input('datedesactivation').' '.$request->input('heuredesactivation');
          }else{
            $datedes=$request->input('datedesactivation').' '.date('H:i:s');
          }
        }

        $dateact=date('Y-m-d H:i:s');
        if($request->input('dateactivation')!=''){
          if($request->input('heureactivation')!=''){
            $dateact=$request->input('dateactivation').' '.$request->input('heureactivation');
          }else{
            $dateact=$request->input('dateactivation').' '.date('H:i:s');
          }
        }

        $u->dateactivation=$dateact;
        $u->datedesactivation=$datedes;
      }
      else{
        $user = array('login' => $u->login, 'password' => $request->input('oldpassword'));
        if(!Auth::attempt($user)){
          return '0'.'L\'ancien mot de passe est incorrecte';
        }
        if($request->input('password')==''){
          return '0'.'Veuillez saisir le nouveau mot de passe !';
        }
        if($request->input('password')!=$request->input('confirm')){
          return '0'.'Confirmation de mot de passe échouée !';
        }
        $u->password = md5($request->input('password'));
      }
      if($request->has('client_id') && $request->client_id>0) $u->client_id=$request->client_id;
      if($request->has('caisse_id') && $request->caisse_id>0) $u->caisse_id=$request->caisse_id;
      if($u->save()){
        //$action_info='Modification d\'un compte utilisateur';
        //Mouchard::Ecrire($action_info,$request->except('_token'));
        return '1'.'Modification effectué  avec succes !';
      }
      return '0'.'Erreur lors de la modification !';
    }
  }


  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet user sera null
    //mais nous recuperons neamoins la liste des 'users' disponibles
    $user = Null;
    $users = $this->user->getAll();
    //return view($this->backend_dir.'user.context',compact('users','user'));
    //$users = $this->user->getAll();
    return view($this->backend_dir.'user.context',compact('user','users'));
  }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $user=User::find($id);
    $users=User::all();
    return view($this->backend_dir.'user.context',compact('user','users'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request)
  {
    if(!Redempteur::valideTime($request->input('heuredebut'),true) || !Redempteur::valideTime($request->input('heurefin'))){
      return '0'.'Veuillez saisir des heures correctes !';
    }
    $datedeb=Redempteur::formater($request->input('datedebut'));
    $datedes=null;
    if($request->input('datefin')!=''){
      if($request->input('heurefin')!=''){
        $datedes=Redempteur::formater($request->input('datefin')).' '.$request->input('heurefin');
      }else{
        $datedes=Redempteur::formater($request->input('datefin')).' '.date('H:i:s');
      }
    }
    $u=User::find($request->input('id'));
    $olprofil=$u->currentProfil();
    if($olprofil!=null){
      $old=$u->currentProfil()->id;
      if($olprofil->profil_id==$request->input('profil_id') &&  Auth::user()->verifierProfil($olprofil->datedebut,$olprofil->datefin)){
        $up=DB::table('profil_user')
          ->where('id','=',''.$old.'')
          ->update(['datefin'=>$datedes]);
        if($up){
          return '1'.'Compte utilisateur modifié avec succes !';
        }
        return '0'.'Erreur lors de cette opération !';
      }else{
        if(Auth::user()->verifierProfil($olprofil->datedebut,$olprofil->datefin)){
          $up=DB::table('profil_user')
            ->where('id','=',''.$old.'')
            ->update(['datefin' => date('Y-m-d H:i:s')]);
        }
        $u->profils()->attach($request->input('profil_id'),['datedebut'=>$datedeb.' '.$request->input('heuredebut'), 'datefin'=>$datedes]);
        return '1'.'Compte utilisateur modifié avec succes !';
      }
    }else{
      $u->profils()->attach($request->input('profil_id'),['datedebut'=>$datedeb.' '.$request->input('heuredebut'), 'datefin'=>$datedes]);
    }
    return '1'.'Compte utilisateur modifié avec succes !';
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    if(ends_with($id, 'check'))
    {
      $x = explode('check', $id);
      $f=User::find($x[0]);
      if($f->actif==0) $f->actif=1;
      else $f->actif=0;
      if($f->save()){
        return '1'.trans('message.actioneffectue');
      }
      return '0'.trans('actionechouee.actionechouee');
    }
    else{
      $f=User::find($id);
      if ($f->delete()) {
        return '1' . trans('message.supressioneffectue');
      }
      return '0'.trans('message.erreuroperation');
    }
  }


}
