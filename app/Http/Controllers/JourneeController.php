<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Journee\JourneeRepository;
use Illuminate\Http\Request;
use App\Journee;
use App\Parametre;

class JourneeController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(JourneeRepository $journee)
  {
    $this->journee = $journee;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet journees sera null
    //mais nous recuperons neamoins la liste des 'journeess' disponibles
    $journee = Null;
    $journees = $this->journee->getAll();
    return view($this->backend_dir.'journee.context',compact('journee','journees'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $journee = $this->journee->find($id);
	$journees = $this->journee->getAll();
	$datejour=$this->journee->find($id) ; 
	if($datejour!=null) {
		$datejour=date('Y-m-d',strtotime($datejour->date));
		$dateref=date('Y-m-d');
		if ($datejour < $dateref) $journee=Null;
	}
    return view($this->backend_dir.'journee.context',compact('journees','journee'));
  }


  public function store(Request $request)
  {
	  
    
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->journee->validate($obj);
	if($obj['type']==1) {
		$param=Parametre::findLast();
		if($param==null || ($param->heuredeb_taf==''  || $param->heurefin_taf==''  || $param->heuredeb_pause==''  || $param->heurefin_pause==''  ) ) return '0 Les paramètres n\'ont pas été configuré!';
		
		$obj['heuredeb_taf']=$param->heuredeb_taf;
		$obj['heurefin_taf']=$param->heurefin_taf;
		$obj['heuredeb_pause']=$param->heuredeb_pause;
		$obj['heurefin_pause']=$param->heurefin_pause;
	}
	$journeExistante = Journee::findDate($obj['date'],$obj['id']);
	if($journeExistante>0) return '0 Cette journée a déjà été enregistrée !';
	$date=date('Y-m-d',strtotime($obj['date']));
	$dateref=date('Y-m-d');
	if ($date < $dateref) return '0 Impossible d\'enregistrer un date antécédante!';
	$dateref=date('Y-m-d');
    if($v->passes()){
      //en ajout
     
        
      Helper::startTransaction();
      
      try {
        //en ajout
        $this->journee->saveIt($obj);

        Helper::completeTransaction();
              
        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
	$datejour=$this->journee->find($id) ; 
	if($datejour!=null) {
		$date=date('Y-m-d',strtotime($datejour->date));
		$dateref=date('Y-m-d');
		if ($datejour < $dateref) return '0';
		return $this->journee->delete($id);
	}
	else return 0;
  }
}
