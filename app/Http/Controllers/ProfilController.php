<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Repositories\Profil\ProfilRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfilController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(ProfilRepository $profil)
  {
    $this->profil = $profil;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet profil sera null
    //mais nous recuperons neamoins la liste des 'profils' disponibles
  $menus=Menu::principaux();
    $profil = Null;
    $profils = $this->profil->getAll();
    return view($this->backend_dir.'profil.context',compact('profils','profil','menus'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $menus=Menu::principaux();
    $profil = $this->profil->find($id);
    $profils = $this->profil->getAll();
    return view($this->backend_dir.'profil.context',compact('profils','profil','menus'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $v=$this->profil->validate($obj);
    if($v->passes()){
        $saved=$this->profil->saveAndReturnId($obj);
        if($saved>0)
        {
          $f=$this->profil->find($saved);
          //toujour bien verifier les entrees
          if($f!=null)
          {

            DB::delete('delete from profil_menu where profil_id = ?', [$f->id]);
            $rules=explode('_',$request->input('rules'));
            $menus=Menu::all();
            foreach ($menus as $menu)
            {
              $i=$menu->id;
              $droit='';
              foreach($rules as $rule)
              {
                if(substr($rule,1)==$i) $droit.=substr($rule,0,1);
              }
                DB::insert('insert into profil_menu (autorisation, menu_id , profil_id ) values (?, ? ,?)', [$droit, $i, $f->id]);

            }
            return '1' .trans('validation.enregistrementeffectue');
          }

        }
        return '0'.trans('validation.enregistrementechoue');

    }
    return '0'.$v->messages()->all()[0];

  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->profil->delete($id);
  }
}
