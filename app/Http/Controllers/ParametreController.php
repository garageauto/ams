<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Parametre\ParametreRepository;
use Illuminate\Http\Request;

use App\Repositories\ImageFile;

class ParametreController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(ParametreRepository $parametre)
  {
    $this->parametre = $parametre;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet parametres sera null
    //mais nous recuperons neamoins la liste des 'parametress' disponibles
    $parametre = Null;
    $parametres = $this->parametre->getAll();
    return view($this->backend_dir.'parametre.context',compact('parametre','parametres'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $parametre = $this->parametre->find($id);
    $parametres = $this->parametre->getAll();
    return view($this->backend_dir.'parametre.context',compact('parametres','parametre'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->parametre->validate($obj);
    if($v->passes()){
      //en ajout
     
        
      Helper::startTransaction();
      
      try {
		  if($request->hasfile('imageupload')){

              $name='prod_'.uniqid().'.'.$request->file('imageupload')->getClientOriginalExtension();

              $itemsavage = new ImageFile();
              $itemsavage->saveToDisk($request->file('imageupload'),$this->parametre->storefolder(),$this->parametre->storefolder().'/'.$name, $name);
              $obj['logo'] = $name ;
            }
        //en ajout
        $this->parametre->saveIt($obj);

        Helper::completeTransaction();
              
        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->parametre->delete($id);
  }
}
