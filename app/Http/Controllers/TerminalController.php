<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Terminal\TerminalRepository;
use Illuminate\Http\Request;

class TerminalController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(TerminalRepository $terminal)
  {
    $this->terminal = $terminal;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet terminals sera null
    //mais nous recuperons neamoins la liste des 'terminalss' disponibles
    $terminal = Null;
    $terminals = $this->terminal->getAll();
    return view($this->backend_dir.'terminal.context',compact('terminal','terminals'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $terminal = $this->terminal->find($id);
    $terminals = $this->terminal->getAll();
    return view($this->backend_dir.'terminal.context',compact('terminals','terminal'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->terminal->validate($obj);
    if($v->passes()){
      //en ajout
     
        
      Helper::startTransaction();
      
      try {
        //en ajout
        $this->terminal->saveIt($obj);

        Helper::completeTransaction();
              
        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->terminal->delete($id);
  }
}
