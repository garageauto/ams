<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Permission\PermissionRepository;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(PermissionRepository $permission)
  {
    $this->permission = $permission;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet permissions sera null
    //mais nous recuperons neamoins la liste des 'permissionss' disponibles
    $permission = Null;
    $permissions = $this->permission->getAll();
    return view($this->backend_dir.'permission.context',compact('permission','permissions'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $permission = $this->permission->find($id);
    $permissions = $this->permission->getAll();
    return view($this->backend_dir.'permission.context',compact('permissions','permission'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->permission->validate($obj);
	$datedeb=date('Y-m-d',strtotime($obj['datedeb']));
	$datefin=date('Y-m-d',strtotime($obj['datefin']));
	$dateref=date('Y-m-d');
	
	if (!$request->has('employe_id')) return '0 Veuiller sélectionner un employé!';
	if ($datedeb < $dateref) return '0 La date de début de la permission est invalide!';
	if ($datedeb > $datefin) return '0 La date de début doit être antérieure à la date de fin!';
    if($v->passes()){
      //en ajout
     
        
      Helper::startTransaction();
      
      try {
        //en ajout
        $this->permission->saveIt($obj);

        Helper::completeTransaction();
              
        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->permission->delete($id);
  }
}
