<?php

namespace App\Http\Controllers\Auth;

use Helper;
use Errorcodes;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\User_annonceur;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    protected $redirectTo = '/auth/login';
    protected $userman;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('guest')->except('logout');

        $this->user = $user;
    }

    public function username()
    {
        return 'login';
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }
    public function showRegisterForm()
    {
        return view('backend.auth.register');
    }
    
    public function annonceurlogin(Request $request)
    {
        $g = new User_annonceur();
        $obj = $request->all();
       
        if($request->has('password') && $request->has('login')){
            if($request->has('agreement') )
            {
                try{
                    $user = $g->exist($obj['login'],$obj['password']);
                    
                    Auth::guard('tmp')->login($user);
                    $request->session()->regenerate();

                    
                } catch (QueryException $e){
                    $yyi = '**** Caught exception: '. $e->getMessage() ;
                    $message = get_class($this).' ('.__METHOD__.')';
                    Helper::logDbError($e->getMessage() , $message) ;

                    throw new \Exception($yyi,Errorcodes::dberror());
                    
                }
            }else{
                Helper::logValidationError("Veuillez accepter les termes du contrat") ;
                throw new \Exception('Veuillez accepter les termes du contrat', Errorcodes::validerror());
     
            }
                
        }else{

          Helper::logValidationError("assurez vous que le login et mots de passe sont corrects" ) ;
          throw new \Exception('Assurez vous que le login et mots de passe sont corrects', Errorcodes::validerror());

        }
        
    }

    public function login(Request $request)
    {
        //$this->validateLogin($request);
        $v=$this->user->connectionvalidate($request->all());

        if($v->passes()){

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            $this->userman = $this->user->exist($request->input('login'),$request->input('password'));
           //dd($this->userman);
            
            if ($this->userman) { //si ll'utilisateur existe,il le connecte et le redirige
               
                $this->guard('web')->login($this->userman);

                return $this->sendLoginResponse($request);
            }

            // if ($this->attemptLogin($request)) {
            //     return $this->sendLoginResponse($request);
            // }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);
            return redirect()->back()->withError('Identifiant ou mot de passe incorrect');
            //return $this->sendFailedLoginResponse($request);
        }else{
          Helper::logValidationError($v->errors()->all() ) ;
          return '0'.$v->messages()->all()[0];
        }
    }

    public function logout(Request $request)
    {
        $this->guard('web')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
