<?php

namespace App\Http\Controllers;

use App\Redempteur;
use Helper;
use Errorcodes;
use App\Repositories\Menu\MenuRepository;
use App\Repositories\Typemenu\TypemenuRepository;
use Illuminate\Http\Request;

class MenuController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(MenuRepository $menu)
  {
    $this->menu = $menu;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet menu sera null
    //mais nous recuperons neamoins la liste des 'menus' disponibles
    $menu = Null;
    $menus = $this->menu->getAll();
    return view($this->backend_dir.'menu.context',compact('menus','menu'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $menu = $this->menu->find($id);
    $menus = $this->menu->getAll();
    return view($this->backend_dir.'menu.context',compact('menus','menu'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->menu->validate($obj);
    if($v->passes()){

      Helper::startTransaction();

      try {
        //en ajout

        //enlefe
        $obj['nommenu'] = Redempteur::enleverCaracteresSpeciaux($obj['nommenu']);
        $obj['newsletter'] = (array_key_exists('newsletter', $obj))? 1 : 0 ;

        $this->menu->saveIt($obj);

        Helper::completeTransaction();

        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }

  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $this->menu->delete($id);
  }
}
