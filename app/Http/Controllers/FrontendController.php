<?php

namespace App\Http\Controllers;
use App\Redempteur;
use App\Repositories\Annonceur\AnnonceurRepository;
use App\Repositories\Salon\SalonRepository;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Helper;
use App\Repositories\Produit\ProduitRepository;
use Errorcodes;
use App\Repositories\ImageFile;


class FrontendController extends Controller
{

    private $frontend_dir='frontend.components.';

   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view($this->frontend_dir.'.home');
    }


    public function deconnexion()
    {
        Session::put('logged_in',0);
        Session::put('useragent_badge',[]);
    }


    public function pagenotfound(){
        return view('frontend.errors.404');
    }






}
