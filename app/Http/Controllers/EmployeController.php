<?php

namespace App\Http\Controllers;

use Helper;
use Errorcodes;
use App\Repositories\Employe\EmployeRepository;
use Illuminate\Http\Request;
use App\Repositories\ImageFile;
use App\Employe;
use App\Redempteur;

class EmployeController extends Controller
{
  private $backend_dir='backend.';

  public function __construct(EmployeRepository $employe)
  {
    $this->employe = $employe;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //nous affichons l'index.
    //vu que nous ne sommes pas en modification l'objet employes sera null
    //mais nous recuperons neamoins la liste des 'employess' disponibles
    $employe = Null;
    $employes = $this->employe->getAll();
    return view($this->backend_dir.'employe.context',compact('employe','employes'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  public function edit($id)
  {
    $employe = $this->employe->find($id);
    $employes = $this->employe->getAll();
    return view($this->backend_dir.'employe.context',compact('employes','employe'));
  }


  public function store(Request $request)
  {
    //recuperer l'ID de l'objet
    $obj=$request->input();
    $id=$request->id;
    $v=$this->employe->validate($obj);
    if($v->passes()){
      //en ajout
	  $datenaiss=date('Y-m-d',strtotime($obj['datenaiss']));
	  $dateentree=date('Y-m-d',strtotime($obj['dateentree']));
	  $dateref=date('Y-m-d');
     if (Redempteur::getAge($datenaiss, $dateref)<15) return '0 Date de naissance invalide';
     //if ($dateentree < $dateref) return '0 La date d\'insertion semble invalide';
        
      Helper::startTransaction();
      
      try {
		  
        //en ajout
		if($request->hasfile('photo')){

              $name='prod_'.uniqid().'.'.$request->file('photo')->getClientOriginalExtension();

              $itemsavage = new ImageFile();
              $itemsavage->saveToDisk($request->file('photo'),$this->employe->storefolder(),$this->employe->storefolder().'/'.$name, $name);
              $obj['photo'] = $name ;
            }
        $this->employe->saveIt($obj);

        Helper::completeTransaction();
              
        return '1' .trans('validation.enregistrementeffectue');

      }catch (\Exception $e) {
            Helper::cancel();
            Helper::completeTransaction();

            if($e->getCode() == 100){
                return $e->getMessage() ;
            }
            if($e->getCode() == 200){

                 return '0'.trans('validation.enregistrementechoue');
            }

      }
    }else{
      Helper::logValidationError($v->errors()->all() ) ;
      return '0'.$v->messages()->all()[0];
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if(ends_with($id, 'check'))
    {
      $x = explode('check', $id);
      $f=Employe::find($x[0]);
      if($f->actif==0) $f->actif=1;
      else $f->actif=0;
      if($f->save()){
        return '1'.trans('message.actioneffectue');
      }
      return '0'.trans('actionechouee.actionechouee');
    }
    else{
      $f=Employe::find($id);
      if ($f->delete()) {
        return '1' . trans('message.supressioneffectue');
      }
      return '0'.trans('message.erreuroperation');
    }
  }

}
