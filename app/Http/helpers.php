<?php


use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Routing\ResponseFactory;
// My common functions


/**
 * Generate the URL to an asset from a custom root domain such as CDN, etc.
 *
 * @param  string  $root
 * @param  string  $path
 * @param  bool|null  $secure
 * @return string
 */

function assetFrom($path, $secure = null)
{
	$root = env('APP_URL');
	//$root = 'https://localhost/keo-learning-5.4';

	return app('url')->assetFrom($root,$path, $secure);
}

function assetfrontFrom($path, $secure = null)
{
  $root = env('APP_URL');

  return app('url')->assetFrom($root,'frontend/'.$path, $secure);
}


  //backendview est un helper qui perment d'afficher
  //le chemin d'acces au dossier contenant les les vues du backend
  if (! function_exists('backendview')) {

    function backendview($path)
    {
      $backendasset='backend.';
      return $backendasset.$path;
    }
  }
  //frontendview est un helper qui perment d'afficher
  //le chemin d'acces au dossier contenant les les vues du frontend
  if (! function_exists('frontendview')) {

    function frontendview($path)
    {
      $frontendasset='frontend.';
      return $frontendasset.$path;
    }
  }
  //backendasset est un helper qui perment d'afficher
  //le chemin d'acces au dossier contenant les fichiers a joindre au niveau du backend
  if (! function_exists('backendasset')) {

    function backendasset($path, $secure = null)
    {
      $root = env('APP_URL');
      $backendasset=$root.'/assets/backend/';
      return $backendasset.$path;
    }
  }
  //frontendasset est un helper qui perment d'afficher
  //le chemin d'acces au dossier contenant les fichiers a joindre au niveau du frontend
  if (! function_exists('frontendasset')) {

    function frontendasset($path)
    {
      $root = env('APP_URL');
      $frontendasset=$root.'/assets/frontend/';
      return $frontendasset.$path;
    }
  }




