<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Parametre;

use App\Repositories\ModelRepository as ModelRepository;
use App\Parametre;

class ParametreRepository extends  ModelRepository
{

  function __construct(Parametre $parametre) {
    $this->model = $parametre;
  }

}
