<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories;

use App\Repositories\ModelInterface as ModelInterface;

abstract class ModelRepository implements ModelInterface
{
  public $model;

  public function storefolder(){
    return $this->model->storefolder ;
  }

  public function validate($obj,$mode=null)
  {
    
    if($mode !== NULL){
      return $this->model->validate($obj,$mode);
    }
    return $this->model->validate($obj);
  }

  public function saveIt($obj)
  {
    return $this->model->saveIt($obj);
  }

  public function saveAndReturnBoolean($obj)
  {
    if(substr($this->model->saveIt($obj),0,1)) return 1;
    else return 0;
  }

  public function saveAndReturnId($obj)
  {
    return $this->model->saveAndReturnId($obj);
  }

  public function insert($obj)
  {
    return $this->model->saveIt($obj);
  }

  public function update($obj)
  {
    return $this->model->saveIt($obj);
  }

  public function getAll()
  {
    return $this->model->getAll();
  }

  public function find($id)
  {
    return $this->model->findIt($id);
  }

  public function findwhere($attribut,$param){
    return $this->model->findItWhere($attribut,$param);
  }

  public function attachIt($id,$relation,$array)
  {
    return $this->model->attachIt($id,$relation,$array);
  }

  //simpleattachIt l'objet a d'autre en passant l'id de l'objet, la relation et un array en parametre
  public function simpleattachIt($id,$relation,$array)
  {
    return $this->model->simpleattachIt($id,$relation,$array);
  }

  public function delete($id)
  {
    if(ends_with($id, 'check'))
    {
      $x = explode('check', $id);
      $f=$this->model->find($x[0]);
      if($f->annuler==0) $f->annuler=1;
      else return '0'.trans('message.actionechouee'); // il est impossible de reactiver une operation
      if($f->save()){
        return '1'.trans('message.actioneffectue');
      }
      return '0'.trans('message.actionechouee');
    }
    else{
      $f=$this->model->find($id);
      if ($f->delete()) {
        return '1' . trans('message.supressioneffectue');
      }
      return '0'.trans('message.erreuroperation');
    }

  }
/*
  public function ExcelContenu($path) {
    $data = Excel::load($path, function($reader) {
    })->noHeading()->skip(1)->get();
    $i=0;
    $v=[];
    if(!empty($data) && $data->count()>0){
      foreach ($data as $key => $value) {
        $i++;
        $n=1;
        foreach ($listecolones as $l)
        {
          $b[''.$l.'']=$value[$n];
          $n++;
        }
        $insert[$i] =$b;
        $v[$i]=$this->model->validate($insert[$i]);

        if($v[$i]->passes()) {
          $this->model->save($insert[$i]);
        }

        //-----------
        $insert[$i] = [

          'titre'=>$value[1],
          'description'=>$value[2],
          'langue_id'=>$value[3],
          'article_id'=>$value[4]

        ];

        $v[$i]=Contenu::validate($insert[$i]);

        if($v[$i]->passes()) {
          DB::table('contenus')->insert($insert[$i]);
        }
      }
    }
  }
*/


}
