<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Employe;

use App\Repositories\ModelRepository as ModelRepository;
use App\Employe;

class EmployeRepository extends  ModelRepository
{

  function __construct(Employe $Employe) {
    $this->model = $Employe;
  }

}
