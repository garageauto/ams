<?php
namespace App\Repositories;

use Image;
use Storage;
use Errorcodes;

class ImageFile
{
	protected $_file_name;
	protected $_file_path;

	public function __construct()
	{
		// work with the protected variable later
	}

	/*
	*	@Return boolean
	*/


	public function saveToDisk($file, $destination_path,$file_path,$file_name)
	{

		if(stristr($file->getClientMimeType(),'image'))
		{


			$img = Image::make($file->getRealPath());


			$actualFile  = $img->encode('jpg', 80)
					->stream();
			Storage::disk('public')->put($file_path, $actualFile);
			return true;
		}else{


			Storage::disk('public')->putFileAs($destination_path, $file, $file_name);
			return true;
		}

			$message = get_class($this).' ('.__METHOD__.')';
      		$yyi = '**** un problème est survenu lors de l\'enrégistrement d\'un fichier ' ;

      		Helper::logFileSaveError($yyi , $message) ;

      		throw new \Exception($yyi.$message,Errorcodes::dberror());
			//return false;
	}

	public function reduceToDisk($pourcentage,$file, $destination_path,$file_path,$file_name)
	{


		if(stristr($file->getClientMimeType(),'image'))
		{


			$img = Image::make($file->getRealPath());


			//redimention
			$height = $img->height() * $pourcentage / 100;
			$width = $img->width() * $pourcentage / 100;
			$img->resize($width, $height);



			//$img->encode('jpg', 80)
			//			->save($file_path);
			$actualFile  = $img->encode('jpg', 80)->stream();
			Storage::disk('public')->put($file_path, $actualFile);

			return true;

			$message = get_class($this).' ('.__METHOD__.')';
      		$yyi = '**** un problème est survenu lors de l\'enrégistrement d\'un fichier ' ;

      		Helper::logFileSaveError($yyi , $message) ;

      		throw new \Exception($yyi.$message,Errorcodes::dberror());
			//return false;
		}


	}

	public function moveExistingTo($pourcentage,$file_path,$destination_path)
	{


			$img=Image::make($file_path);

			//redimention
			$height = $img->height() * $pourcentage / 100;
			$width = $img->width() * $pourcentage / 100;
			$img->resize($width, $height);


			$actualFile  = $img->stream();
			Storage::disk('public')->put($destination_path, $actualFile);

			return true;

			$message = get_class($this).' ('.__METHOD__.')';
      		$yyi = '**** un problème est survenu lors de l\'enrégistrement d\'un fichier ' ;

      		Helper::logFileSaveError($yyi , $message) ;

      		throw new \Exception($yyi.$message,Errorcodes::dberror());
			//return false;


	}

}
