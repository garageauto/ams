<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Presence;

use App\Repositories\ModelRepository as ModelRepository;
use App\Presence;

class PresenceRepository extends  ModelRepository
{

  function __construct(Presence $Presence) {
    $this->model = $Presence;
  }

}
