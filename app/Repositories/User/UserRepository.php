<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\User;

use App\Repositories\ModelRepository as ModelRepository;
use App\User;

use Illuminate\Support\Facades\Hash;


class UserRepository extends  ModelRepository
{

  function __construct(User $user) {
    $this->model = $user;
  }

  public function connectionvalidate($obj)
  {
    return $this->model->connectionvalidate($obj);
  }

  public function exist($login,$pass)
	{
		$user = $this->model->where('login',$login)->first();

		if($user){
			$isvalid = Hash::check($pass, $user->password);
		}
		else{
			return null;
		}


		if($isvalid)
		{
			return $user;
		}
		else{
			return null;
		}
	}

}
