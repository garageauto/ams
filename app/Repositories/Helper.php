<?php 

namespace App\Repositories;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\RotatingFileHandler;
use Log;

class Helper
{
	
	
	public static function startTransaction()
	{
		DB::beginTransaction();
	}
	public static function handleTransaction(callable $callback, array $params)
	{	
		return call_user_func($callback, $params);
	}
	
	public static function completeTransaction()
	{
		DB::commit();
	}
	
	public static function cancel()
	{
		DB::rollback();
	}

	public static function logDbError($message,$class)
	{	
		$monolog = Log::getMonolog();
		//dd($monolog->getHandlers());
		$monolog->popHandler();
		//$monolog->pushHandler(new StreamHandler(storage_path('/logs/dberror.log')));
		$monolog->pushHandler(new RotatingFileHandler(storage_path('/logs/dberror.log'),1,'error'));
		$monolog->addError('DBError in '.$class.' ,',array('message' => $message));

		//dd($monolog->getHandlers());
		//$monolog->pushHandler(new StreamHandler(storage_path('/logs/laravel.log')));
	

	}

	public static function logValidationError($message)
	{

		$monolog = Log::getMonolog();
		//dd($monolog->getHandlers());
		$monolog->popHandler();
		//$monolog->pushHandler(new StreamHandler(storage_path('/logs/validate.log')));
		$monolog->pushHandler(new RotatingFileHandler(storage_path('/logs/validate.log'),1,'error'));
		$monolog->addError('ValidateError: ,',array('message' => $message));

		

	}
	
	public static function logFileSaveError($message,$class)
	{	
		$monolog = Log::getMonolog();
		//dd($monolog->getHandlers());
		$monolog->popHandler();
		//$monolog->pushHandler(new StreamHandler(storage_path('/logs/dberror.log')));
		$monolog->pushHandler(new RotatingFileHandler(storage_path('/logs/fileerror.log'),1,'error'));
		$monolog->addError('FileError in '.$class.' ,',array('message' => $message));

		//dd($monolog->getHandlers());
		//$monolog->pushHandler(new StreamHandler(storage_path('/logs/laravel.log')));
	

	}
	
}