<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Menu;

use App\Repositories\ModelRepository as ModelRepository;
use App\Menu;
use App\Langue;
use App\Typemenu;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class MenuRepository extends  ModelRepository
{

	function __construct(Menu $Menu) {
	$this->model = $Menu;
	}

	  public static function ExcelMenu($path, $parent_id, $disposition_id) {
		$data = Excel::load($path, function($reader) {
				})->noHeading()->skip(2)->get();	
		$dataLangue = Excel::load($path, function($reader) {
				})->noHeading()->take(1)->get();
			$i=0;
			$fr = null;
			$en = null;
			$es = null;
			$ar = null;
			$date = date('Y-m-d H:i:s');
		$v=[];
		if(!empty($data) && $data->count()>0){

		$lastMenu = Menu::orderBy('id', 'DESC')->first();
		$frontend_id = Typemenu::where('nomtypemenu', '=', 'frontend')->pluck('id');					
		$contenu = array();
	  	if(!empty($data) && $data->count()>0){
			foreach ($data as $key => $value) {
				if($value !='break' && !empty($value)) {
					$i++;
					if($dataLangue[0][1]=='fr')	{$fr = $value[2]; }
					if($dataLangue[0][2]=='en')	{$en = $value[3]; }
					if($dataLangue[0][3]=='es')	{$es = $value[4]; }
					if($dataLangue[0][4]=='ar')	{$ar = $value[5]; }

					$contenu[$i] = [
					    'codemenu'=>$value[0],
						'urlmenu'=>$value[1],
						'nommenu'=>$value[2],
						'menu_id'=>$parent_id,
						'disposition_id'=>$disposition_id,
						'typemenu_id'=>$frontend_id[0],
					    'fr'=> $fr,
						'en'=>$en,
						'es'=>$es,
						'ar'=>$ar,
						'created_at'=>$date,
						'updated_at'=>$date
					];
					DB::table('menus')->insert($contenu[$i]);
				}
			
				}
		  	}
		}
	}
}
