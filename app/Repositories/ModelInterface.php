<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 * interface regrougent les diffrentes fonctions a implementer dans le repository
 * tous les models sont supposes utiliser cette interface
 */
namespace App\Repositories;

interface ModelInterface {

  public function validate($obj);

  public function insert($obj);

  public function saveIt($obj);

  public function saveAndReturnBoolean($obj);

  public function saveAndReturnId($obj);

  public function update($obj);

  public function getAll();

  public function find($id);
 
  public function attachIt($id,$relation,$array);

  public function findwhere($attribut,$param);

  public function delete($id);

}
