<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Journee;

use App\Repositories\ModelRepository as ModelRepository;
use App\Journee;

class JourneeRepository extends  ModelRepository
{

  function __construct(Journee $Journee) {
    $this->model = $Journee;
  }

}
