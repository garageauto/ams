<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Profil;

use App\Repositories\ModelRepository as ModelRepository;
use App\Profil;

class ProfilRepository extends  ModelRepository
{

  function __construct(Profil $profil) {
    $this->model = $profil;
  }

}
