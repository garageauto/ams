<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Terminal;

use App\Repositories\ModelRepository as ModelRepository;
use App\Terminal;

class TerminalRepository extends  ModelRepository
{

  function __construct(Terminal $Terminal) {
    $this->model = $Terminal;
  }

}
