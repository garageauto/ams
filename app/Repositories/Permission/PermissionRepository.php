<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */
namespace App\Repositories\Permission;

use App\Repositories\ModelRepository as ModelRepository;
use App\Permission;

class PermissionRepository extends  ModelRepository
{

  function __construct(Permission $Permission) {
    $this->model = $Permission;
  }

}
