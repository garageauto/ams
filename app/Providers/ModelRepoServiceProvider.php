<?php
/**
 * Created by IntelliJ IDEA.
 * Model: Thierry TOMETY
 * Date: 28/06/2017
 * Time: 10:08
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ModelRepoServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    //parent::boot();
  }

  /**
   * In this step we have to create Service Provider for bind ModelInterface and ModelRepository class.
   * so let's create ModelRepoServiceProvide.php file in Model folder and put following code:
   */
  public function register()
  {
    $this->app->bind('App\Repositories\ModelInterface', 'App\Repositories\ModelRepository');
  }
  /**
   * We have already Model I think, so you have just need to add getAll(), findModel() and deleteModel()
   * function. But you can also past bellow code too. So, let's put bellow code on your model.
   */
}
