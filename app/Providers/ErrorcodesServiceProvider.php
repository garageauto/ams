<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Exceptions\Errorcodes;

class ErrorcodesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('errorcodes', function(){
            return new Errorcodes;
        });
    }
}
