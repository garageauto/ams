<?php

namespace App;

use Helper;
use Errorcodes;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException as QueryException;


class Base extends Model
{
  //ceci contiendra les donnees qui rentrerons dans la base impertativement (required)
  public $required_on_inserts=[];

  //ceci contiendra les donnees qui rentrerons dans la base (les facultatifs)
  public $other_inputs_on_inserts=[];

  //liste des entrees a ne pas modifier
  public $un_updatables=[];

  //retourne tous les Base disponibles
  public function getAll(){return static::all();}
  //retourne l'objet en passant l'ID en parametre
  public function findIt($id){return static::find($id);}
  //retourne l'objet en passant l'ID en parametre
  public static  function seek($val){return static::select('*')->orderby('created_at','desc')->limit(10)->get();}
  //retourne l'objet en recherchant par un attribut donné
  public function findItWhere($attribut,$param){return static::where($attribut,$param)->first();}
  //suppression d'un utilisateur
  public function deleteIt($id){
    Mouchard::Ecrire($id,$this->getTable(),3);
    return static::find($id)->delete();
  }
  //attach l'objet a d'autre en passant l'id de l'objet, la relation et un array en parametre
  public function attachIt($id,$relation,$array){

    try {
      $this->findIt($id)->$relation()->sync($array);
      return true;
    } catch (QueryException $e){

      $yyi = '**** Caught exception: '. $e->getMessage() ;
      $message = get_class($this).' ('.__METHOD__.')';
      Helper::logDbError($e->getMessage() , $message) ;

      throw new \Exception($yyi,Errorcodes::dberror());
    }

  }

  //simpleattachIt l'objet a d'autre en passant l'id de l'objet, la relation et un array en parametre
  public function simpleattachIt($id,$relation,$array){

    try {
      $this->findIt($id)->$relation()->attach($array);
      return true;
    } catch (QueryException $e){

      $yyi = '**** Caught exception: '. $e->getMessage() ;
      $message = get_class($this).' ('.__METHOD__.')';
      Helper::logDbError($e->getMessage() , $message) ;

      throw new \Exception($yyi,Errorcodes::dberror());
    }

  }


  //permet d'editer un ojet
  public function saveIt($obj){
    //dans le cas d'une insertion
    //nous utiliserons la variable f par defaut pour specifier tout objet
    // en provenance de la base et o pour les objets a editer dans la base
    if(!is_array($obj) || !array_key_exists('id', $obj)){

      $message = get_class($this).' ('.__METHOD__.')';
      $yyi = '**** un problème est survenu avec l\'objet envoyé en parametre ' ;

      Helper::logDbError($yyi , $message) ;

      throw new \Exception($yyi.$message,Errorcodes::dberror());
    }

    if($obj['id']==0)
    {
        $action=1;
        $o=new static;
    }
    //dans le cas d'une modification
    else{
        $action=2;
        $o=$this->findIt($obj['id']);
    }

    //code d'edition
    foreach ($this->required_on_inserts as $focused_input)
    {
      //en faisant just : $obj[$focused_input]!=null , une erreur sera occasionner si un element du tableau "$this->required_on_inserts"  trouve pas dans "$obj;"
      if(!array_key_exists($focused_input, $obj)){

          Helper::logValidationError('validation.'.$focused_input.'_required') ;
          throw new \Exception('0'.'validation.'.$focused_input.'_required', Errorcodes::validerror());

      }

      if($obj[$focused_input]!=null) $o->$focused_input=$obj[$focused_input];
    }
    foreach ($this->other_inputs_on_inserts as $focused_input)
    {
      //en faisant just : $obj[$focused_input]!=null , une erreur sera occasionner si un element du tableau "$this->other_inputs_on_inserts"  trouve pas dans "$obj;"
      // if(!array_key_exists($focused_input, $obj)){

      //     Helper::logValidationError('validation.'.$focused_input.'_required') ;
      //     throw new \Exception('0validation.'.$focused_input.'_required', Errorcodes::validerror());

      // }

      //if($obj[$focused_input]!=null) $o->$focused_input=$obj[$focused_input];

      if(array_key_exists($focused_input, $obj)){

        $o->$focused_input=$obj[$focused_input];

      }
    }
    //sauvegarde
    //dans le cas d'une sauvegarde effective on retourne un message de succes
    //les messages de succes sont precedes de  1 et les message d'erreur de 0
    //le systeme se charge d'interpreter le resultat en fonction du template
    //il est important de toujours passer par le fichier de translation
    //pour afficher un texte en brute - cela permet d'anticiper la gestion de
    //la langue
    try {
      $o->save();
      Mouchard::Ecrire($o->id,$this->getTable(),$action);
      return '1' .trans('validation.enregistrementeffectue');
    } catch (QueryException $e){

      $yyi = '**** Caught exception: '. $e->getMessage() ;
      $message = get_class($this).' ('.__METHOD__.')';
      Helper::logDbError($e->getMessage() , $message) ;

      throw new \Exception($yyi,Errorcodes::dberror());
    }


    // if($o->save())
    // {
    //   return '1' .trans('validation.enregistrementeffectue');
    // }
    // return '0'.trans('validation.enregistrementechoue');

  }

  public function saveAndReturnId($obj){
    //dans le cas d'une insertion
    //nous utiliserons la variable f par defaut pour specifier tout objet
    // en provenance de la base et o pour les objets a editer dans la base
    if(!is_array($obj) || !array_key_exists('id', $obj)){

      $message = get_class($this).' ('.__METHOD__.')';
      $yyi = '**** un problème est survenu avec l\'objet envoyé en parrametre ' ;

      Helper::logDbError($yyi , $message) ;

      throw new \Exception($yyi.$message,Errorcodes::dberror());
    }

    if($obj['id']==0)
    {
        $action=1;
        $o=new static;

    }
    //dans le cas d'une modification
    else{
        $action=2;
        $o=$this->findIt($obj['id']);
    }

    //code d'edition
   // if(in_array('entete', $this->required_on_inserts)) {dd($obj);}
    foreach ($this->required_on_inserts as $focused_input)
    {
      //en faisant just : $obj[$focused_input]!=null , une erreur sera occasionner si un element du tableau "$this->required_on_inserts"  trouve pas dans "$obj;"

      if(!array_key_exists($focused_input, $obj)){

          Helper::logValidationError('validation.'.$focused_input.'_required') ;
          throw new \Exception('0'.'validation.'.$focused_input.'_required', Errorcodes::validerror());

      }

      if($obj[$focused_input]!=null) $o->$focused_input=$obj[$focused_input];
    }
    foreach ($this->other_inputs_on_inserts as $focused_input)
    {
      //en faisant just : $obj[$focused_input]!=null , une erreur sera occasionner si un element du tableau "$this->other_inputs_on_inserts  "trouve pas dans "$obj";


      // if(!array_key_exists($focused_input, $obj)){

      //     Helper::logValidationError('validation.'.$focused_input.'_required') ;
      //     throw new \Exception('0'.'validation.'.$focused_input.'_required', Errorcodes::validerror());

      // }

      //  if($obj[$focused_input]!=null) $o->$focused_input=$obj[$focused_input];
      if(array_key_exists($focused_input, $obj)){

        $o->$focused_input=$obj[$focused_input];

      }
    }

    //sauvegarde
    //dans le cas d'une sauvegarde effective on retourne un message de succes
    //les messages de succes sont precedes de  1 et les message d'erreur de 0
    //le systeme se charge d'interpreter le resultat en fonction du template
    //il est important de toujours passer par le fichier de translation
    //pour afficher un texte en brute - cela permet d'anticiper la gestion de
    //la langue

    try {

      $o->save();
      Mouchard::Ecrire($o->id,$this->getTable(),$action);
      return $o->id;

    } catch (QueryException $e){
      $yyi = '**** Caught exception: '. $e->getMessage() ;

      $message = get_class($this).' ('.__METHOD__.')';
      Helper::logDbError($e->getMessage() , $message) ;

      throw new \Exception($yyi,Errorcodes::dberror());
      //return 0;
    }

    // if($o->save())
    // {
    //   return $o->id;
    // }
    // return 0;

  }


}
