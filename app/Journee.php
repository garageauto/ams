<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Journee extends Base {

    //
    public $table='journees';
   	use SoftDeletes;
    public $storefolder='public/journees/images';
  	public $required_on_inserts=['date','type'];


  	public $other_inputs_on_inserts=['heuredeb_taf','heurefin_taf','heuredeb_pause','heurefin_pause'];

  //validation
 	public static  function validate($input){
	    $rules = array(

	    );
	    $messages = array(

	    );

	    return Validator::make($input, $rules, $messages);
  	}

	  
    public static function getone(){return self::find(1);}
    public static function findDate($date,$id){return self::select('*')->where('date',$date)->where('id','<>',$id)->count();}
    public static function findDate2(){return self::select('*')->where('date',date('Y-m-d'))->count();}
    public static function findDate3(){return self::select('*')->where('date',date('Y-m-d'))->orderby('created_at','desc')->limit(1)->get();}

}
