<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Mouchard extends Model {


    public function scopeDateBetween($query, $datedeb, $datefin)
    {
      return $query->where('created_at', '>=', $datedeb.' 00:00:00')->where('created_at', '<=', $datefin.' 23:59:59');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function Ecrire($my_id,$modele,$etat)
    {
            //etat 1-ajout 2-modification 3-suppression 4-annulation/reactivation
            //if(Auth::user()!=NULL) $id=Auth::user()->id; else $id=null;
            //$m=new Mouchard;
            //$m->user_id=$id;
            //$m->element_id=$my_id;
            //$m->etat=$etat;
            //$m->modele=$modele;
            //$m->save();
            //return $m;
    }

    public function journal(){
        $mouchard = self::select('*')->get();
        return $mouchard;

    }

    public static function journalPeriode($deb,$fin){

        $mouchard = self::select('*')
          ->where('created_at', '>=', $deb.' 00:00:00')
          ->where('created_at', '<=', $fin.' 23:59:59')
            ->get();
        return $mouchard;

    }

    public static function journalDuJour(){
        $date=date('Y-m-d');
        $mouchard = Mouchard::select('*')
          ->where('created_at', '>=', $date.' 00:00:00')
          ->where('created_at', '<=', $date.' 23:59:59')
            ->get();
        return $mouchard;

    }






}
