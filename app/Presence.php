<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Journee;
use App\Permission;
use App\Redempteur;

class Presence extends Base {

    //
    public $table='presences';
   	use SoftDeletes;
    public $storefolder='public/presences/images';
  	public $required_on_inserts=['journee_id','employe_id'];


  	public $other_inputs_on_inserts=['heure_fin','heure_deb'];

  //validation
 	public static  function validate($input){
	    $rules = array(

	    );
	    $messages = array(

	    );

	    return Validator::make($input, $rules, $messages);
  	}
	
	public function employe()
    {
        return $this->belongsTo('App\Employe');
    }
	
	public function journee()
    {
        return $this->belongsTo('App\Journee');
    }

	  
    public static function getone(){return self::find(1);}
	
    public static function getJournees($datedeb,$datefin){
		return Journee::select('*')->where('date','>=',$datedeb)->where('date','<=',$datefin)->get();
	}
	
	public static function getAbsences($employe_id,$datedeb,$datefin){
		return Permission::select('*')->where('employe_id',$employe_id)->where('datedeb','>=',$datedeb)->Where('datefin','<=',$datefin)->get();
	} 
	
	public static function getNbrHeuresPeriode($datedeb,$datefin){
		$nbrHeures=0;
		$nbrHeuresCreuse=0;
		$journees=self::getJournees($datedeb,$datefin);
		foreach($journees as $journee){
				$nbrHeures+=Redempteur::getDureInt2(date('Y-m-d').' '.$journee->heuredeb_taf,date('Y-m-d').' '.$journee->heurefin_taf);
				$nbrHeuresCreuse+=Redempteur::getDureInt2(date('Y-m-d').' '.$journee->heuredeb_pause,date('Y-m-d').' '.$journee->heuredeb_pause);
		}
		return $nbrHeures - $nbrHeuresCreuse;
	}
	public static function getNbrHeures($employe_id,$datedeb,$datefin){
		$nbrHeures=0;
		$journees=self::getJournees($datedeb,$datefin);
		foreach($journees as $journee){
			$presences=self::findPresence2($journee->id,$employe_id);
			
			foreach($presences as $presence){
				$nbrHeures+=Redempteur::getDureInt2(date('Y-m-d').' '.$presence->heure_deb,date('Y-m-d').' '.$presence->heure_fin);
			}
		}
		return $nbrHeures;
	}
	
	public static function getNbrHeuresAbsence($employe_id,$datedeb,$datefin){
		$nbrHeures=0;
		$journees=self::getAbsences($employe_id,$datedeb,$datefin);
		foreach($journees as $journee){
			
				$nbrHeures+=Redempteur::getDureInt($journee->datedeb.' 00:00:00',$journee->detefin.' 00:00:00');
			
		}
		return $nbrHeures;
	}
	
	
	public static function findPresence($journee_id,$employe_id){return self::select('*')->where('employe_id',$employe_id)->where('journee_id',$journee_id)->whereNull('heure_fin')->orderby('created_at','desc')->limit(1)->get();}
	public static function findPresence2($journee_id,$employe_id){return self::select('*')->where('employe_id',$employe_id)->where('journee_id',$journee_id)->whereNotNull('heure_fin')->orderby('created_at','desc')->get();}
	public static function findPresence3($date){return self::select('*')->where('created_at','>=',$date.' 00:00:00')->where('created_at','<=',$date.' 23:59:59')->orderby('created_at','desc')->get();}
   
}
