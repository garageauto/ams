<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Permission extends Base {

    //
    public $table='permissions';
   	use SoftDeletes;
    public $storefolder='public/permissions/images';
  	public $required_on_inserts=[	'datedeb',	'datefin',	'employe_id'];

  	public $other_inputs_on_inserts=['motif'];

  //validation
 	public static  function validate($input){
	    $rules = array(

	    );
	    $messages = array(

	    );

	    return Validator::make($input, $rules, $messages);
  	}

	  
    public static function getone(){return self::find(1);}
	
	public function employe()
    {
        return $this->belongsTo('App\Employe');
    }

}
