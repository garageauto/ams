<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class Profil extends Base {

  /*
   * Fonctionnalites de base ecrites par thierry tomety
   * pour la gestion des profils utilisateurs et des droits associes
   */

  public $table='profils';
  use SoftDeletes;

  public $required_on_inserts=['nomprofil'];

  public $other_inputs_on_inserts=[];


    public function menus(){
        return $this->hasMany('App\Menu');
    }

    public static function createclass($prefix,$addclass,$profil_id,$i)
    {

        $addclass=explode('_',$addclass);
        $result=' ';
        foreach($addclass as $maclass)
        {
            $result=$result.$prefix.$maclass.' ';
            if (Profil::verifierDroit($profil_id,$i,$prefix) ) $result=$result.'active ';
        }
        return $result;
    }

    public static function listeEffectiveDroits($id , $profil_id)
    {
        //retourne seulement les autorisations
        $droit=DB::table('profil_menu')
            //->where('profil_menu.retrait',0)
            ->where('profil_menu.deleted_at',NULL)
            ->where('profil_menu.menu_id',$id)
            ->where('profil_menu.profil_id',$profil_id)
            ->limit(1)
            ->pluck('autorisation');

        return $droit;
    }

    public static function verifierDroit($profil_id,$menu_id,$droit)
    {
      //pour un utilisateur ayant le statut admin dans la base il sera possible d'acceder a toutes les fonctionnalites de l'administration
        //if(Auth::user() && Auth::user()->statut=='admin') return 1;
        if ($profil_id==0 || $menu_id=='' || $menu_id==0) return 0;

        $droit=DB::table('profil_menu')
            ->where('menu_id',$menu_id)
            ->where('profil_id',$profil_id)
            //->where('retrait',0)
            ->where('deleted_at',NULL)
            ->where('autorisation', 'like' , '%'.$droit.'%')
            ->count();
        return $droit;
    }

    public static function macroInputDroits($i,$addclass , $profil_id){

        $cl=Profil::createclass('l',$addclass, $profil_id,$i);
        $ca=Profil::createclass('a',$addclass, $profil_id,$i);
        $cm=Profil::createclass('m',$addclass, $profil_id,$i);
        $cs=Profil::createclass('s',$addclass, $profil_id,$i);
        $ci=Profil::createclass('i',$addclass, $profil_id,$i);
        $cc=Profil::createclass('e',$addclass, $profil_id,$i);
        $ct=Profil::createclass('t',$addclass, $profil_id,$i);
        //nours prendrons 'e' pour representer le droit d'execution/confirmation
        if (strchr($ca,' active ') &&
            strchr($cl,' active ')  && strchr($cm,' active ')  &&
            strchr($cs,' active ')  && strchr($ci,' active ')  &&
            strchr($cc,' active '))
            $ct.=' active ';
        $prefix='message.';
        $ajout=$prefix.'ajout';
        $modification=$prefix.'modification';
        $suppression=$prefix.'suppression';
        $import=$prefix.'import';
        $confirmation=$prefix.'confirmation';
        $touslesdroits=$prefix.'touslesdroits';

        //macro permettant de génerer les inputs correspondants aux droits disponibles
        $formulaire='<div class="btn-group middle-pos pull-right" data-toggle="buttons">
             <label class="btn btn-primary btn-sm checkbox_for_the_rights '.$cl.'" title="lecture" id="l'.$i.'">
             <input type="checkbox" class="middle-pos-input" name="l'.$i.'" id=""  ><i class="fa fa-eye"></i>
             </label>
             <label class="btn btn-primary btn-sm '.$ca.'" title="'.trans($ajout).'" id="a'.$i.'">
             <input type="checkbox"  class="middle-pos-input" name="a'.$i.'" id=""  ><i class="fa fa-plus-square"></i>
             </label>
             <label class="btn btn-primary btn-sm '.$cm.'" title="'.trans($modification).'" id="m'.$i.'">
             <input type="checkbox"  class="middle-pos-input" name="m'.$i.'" id=""  ><i class="fa fa-pencil"></i>
             </label>
             <label class="btn btn-primary btn-sm  '.$cs.'" title="'.trans($suppression).'" id="s'.$i.'">
             <input type="checkbox"  class="middle-pos-input" name="s'.$i.'" id=""  ><i class="fa fa-trash"></i>
             </label>
             <label class="btn btn-primary btn-sm  '.$ci.'" title="'.trans($import).'" id="i'.$i.'">
             <input type="checkbox"  class="middle-pos-input" name="i'.$i.'" id=""  ><i class="fa fa-mail-reply"></i>
             </label>
             <label class="btn btn-primary btn-sm  '.$cc.'" title="'.trans($confirmation).'" id="e'.$i.'">
             <input type="checkbox"  class="middle-pos-input" name="e'.$i.'" id=""  ><i class="fa fa-mail-forward"></i>
             </label>
             <label class="btn btn-primary btn-sm checkbox_for_all_rights  '.$ct.'" title="'.trans($touslesdroits).'" id="t'.$i.'">
             <input type="checkbox"  class="middle-pos-input " name="t'.$i.'" id=""  ><i class="fa fa-circle-o-notch"></i>
             </label>
             </div>';
        echo $formulaire;
    }

    public static function lasInsered(){
        $profil=Self::select('id')
            ->orderBy('created_at','desc')
            ->withTrashed()
            ->first();
        if($profil!=null){
            $tab=explode('|',$profil->id);
            return $tab[1];
        }
        else return 0;
    }

    public static function getByName($code){
        return self::select('*')->where('nomprofil',$code)->first();
    }

    public static function listeProfils(){
        $profils=Self::select('*')
            ->orderBy('nomprofil','asc')
            ->get();
        return $profils;
    }

    //gerer la translation
    public static function validate($input)
    {
        $rules = array(
            'nomprofil' => 'required | between:2,254',
        );
        $messages = array(
            'nomprofil.required' => 'validation.nomprofil_required',
            'nomprofil.between' => 'validation.nomprofil_between.'
        );
        return Validator::make($input, $rules, $messages);
    }

}
