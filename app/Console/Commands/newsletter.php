<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;


class newsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $destinataire=\App\Internaute::getNewsRecievers(); //envoyer a tous les internautes consernes
      $destinataire=implode(',',$destinataire->all());
      $headers = "From: \"".trans('message.applicationname')."\"<info@".trans('message.applicationname').".com>\n";
      $headers .= "Reply-To: info@".trans('message.applicationname').".com\n";
      $headers .= "Content-Type: text/html; charset=\"utf-8\"";
      $sujet =trans('message.newsletter').' '.trans('message.applicationname');
      $monmessage =view('emails.newsletter');
      mail($destinataire, $sujet, $monmessage, $headers);
      return 'mail sended at -'.date('h:i');

    }

}
