<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Parametre extends Base {

    //
    public $table='parametres';
   	use SoftDeletes;
    public $storefolder='public/parametres';
  	public $required_on_inserts=['heuredeb_taf','heurefin_taf','heuredeb_pause','heurefin_pause','raisonsociale'];

  	public $other_inputs_on_inserts=['adresse','logo'];

  //validation
 	public static  function validate($input){
	    $rules = array(

		  'logo'  => 'sometimes|image|mimes:jpeg,jpg,png,mp4,ogg',
	      'raisonsociale'  => 'required',
	      'heuredeb_taf'  => 'required',
	      'heurefin_taf'  => 'required',
	      'heuredeb_pause'  => 'required',
	      'heurefin_pause'  => 'required',

	    );
	    $messages = array(
	      'raisonsociale.required'    =>  'raisonsociale requise',
	      'heuredeb_taf.required'    =>  'heure de debut requise',
	      'heurefin_taf.required'    =>  'heure de fin requise',
	      'heuredeb_pause.required'    =>  'heure de debut de la pause requise',
	      'heurefin_pause.required'    =>  'heure de fin de la pause requise',
	      'logo.mimes'    =>  trans('validation.logo_mimes'),
	      'logo.image'    =>  trans('validation.logo_image'),

	    );

	    return Validator::make($input, $rules, $messages);
  	}

  
    public static function getone(){return self::find(1);}
    public static function findLast(){
		$param=self::select('*')->orderBy('created_at','desc')->limit(1)->get();
		if($param!=null && count($param)>0) return $param[0]; 
		return null; //else case
	}

}
