<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Laravel\Cashier\Billable;
// use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Support\Facades\Validator;

use Illuminate\Contracts\Auth\Authenticatable;
use Jenssegers\Agent\Agent;

class User extends Base implements Authenticatable
{

    public $table = 'users';
    use Notifiable, AuthenticatableTrait;
    use Billable;

    protected $fillable = ['name', 'email', 'password',];
    protected $hidden = ['password', 'remember_token',];
    public $required_on_inserts = ['login',  'password', 'profil_id'];

    public $other_inputs_on_inserts = ['datedesactivation', 'statut','email', 'actif', 'park_id', 'dateactivation' ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    //retourne tous les utilisateurs disponibles
    public function getAll()
    {
        return static::all();
    }

    //retourne l'objet en passant l'ID de l'utilisateur en parametre
    public function findUser($id)
    {
        return static::find($id);
    }
    //suppression d'un utilisateur
    //cette fonction ne serait pas necessaire
    public function deleteUser($id)
    {/*return static::find($id)->delete();*/
    }

    //verifie si le compte est active et retourne un booleen
    public function isActivated()
    {
        if ($this->actif == 1) {
            return true;
        } else {
            return false;
        }
    }

    //validation
    public static function connectionvalidate($input)
    {
        $rules = array(
            // 'auteur_id'  => 'sometimes|numeric',
            // 'annonceur_id'  => 'sometimes|numeric',
            'login' => 'required',

            'password' => 'required',


        );


        $messages = array(

            'login.required' => trans('validation.login_required'),

            'password.required' => trans('validation.password_required'),

        );

        return Validator::make($input, $rules, $messages);
    }

    //validation
    public static function validate($input)
    {
        $rules = array(
            'login' => 'required|unique:users,login,NULL,id,deleted_at,NULL',
            'email' => 'nullable|email',
            'profil_id' => 'required|numeric',

        );



        $messages = array(
          'login.required' => trans('validation.login_required'),
            'email.email' => trans('validation.email_email'),
            'password.required' => trans('validation.password_required'),
            'profil_id.required' => trans('validation.profil_id_required'),

            'profil_id.numeric' => trans('validation.profil_id_numeric'),

        );

        return Validator::make($input, $rules, $messages);
    }

    //verifie l'unicite du login en ajout comme en modification
    public function isUnique($oninsert)
    {
        if ($oninsert) {
            $nb = static::select('id')
                ->where('login', '=', '' . $this->login . '')
                ->count();
        } else {
            $nb = static::select('id')
                ->where('id', '<>', '' . $this->id . '')
                ->where('login', '=', '' . $this->login . '')
                ->count();
        }
        if ($nb == 0) return true; else return false;
    }

    //verifie si le login et le mot de passe correspondent a un utilisateur
    public static function attemptLogin($login, $pass)
    {

        $nb = static::select('id')
            ->where('login', '=', '' . $login . '')
            ->where('password', '=', '' . md5($pass) . '')
            ->first();

        if ($nb != null) {
            Auth::loginUsingId($nb->id);
            return $nb->id;
        } else return 0;
    }

    public function currentProfilName()
    {
        if ($this->profil_id != 0 && $this->profil_id != null && $this->profil != null) {
            return $this->profil->nomprofil;
        }

    }

    public function currentProfil()
    {
        if ($this->profil_id != 0 && $this->profil_id != null && $this->profil != null) {
            return $this->profil;
        }

    }

    //retourne le nom  l'id de l'utilisateur en passant son ID en parametre
    //ceci est une fonction static
    public static function getUserByName($username)
    {
        $client = static::select('*')
            ->where('username', '=', '' . $username . '')->first();
        return $client;
    }

    //retourne le nom de l'utilisateur
    public function getName()
    {
        return $this->login;
    }

    //autres fonctions
    //les relations
    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function personne()
    {
        return $this->belongsTo('App\Personne');
    }


    public static function getDeviceAgent()
    {
        if (!Session::has('deviceagent')) {
            $agent = new Agent();
            Session::put('deviceagent', $agent);
        }
        return Session::get('deviceagent');
    }

    public static function getAuthId()
    {
        if (Session::get('logged_in') && Session::has('useragent_badge') && Session::has('useragent_key') && md5(Session::has('useragent_badge'))==Session::has('useragent_key')) {
            return Session::get('useragent_badge')['id'];
        } else return 0;
    }

    public static function getAuthName()
    {
        if (Session::get('logged_in') && Session::has('useragent_badge') && Session::has('useragent_key') && md5(Session::has('useragent_badge'))==Session::has('useragent_key')) {
            return Session::get('useragent_badge')['raisonsociale'];
        } else return '';
    }

    public static function LogFrontendUser($id){
        $user=Annonceur::select('id','code','raisonsociale','logo')->where('id',$id)->first();
        //$useragent_badge='badge_'.Crypt::encrypt($login.'_'.$id);
        Session::put('logged_in',1);
        $salons=[];
        if($user!=null){
            $salons=$user->salons;
            $user->listsalons=$salons;
        }
        $useragent_badge=collect($user);
        Session::put('useragent_badge',$useragent_badge);
        Session::put('useragent_key',md5($id));
        Session::put('user_salons',$salons);
        return $useragent_badge;
    }

    public static function isNice($user){
        if($user==null || $user->actif==0 || $user->id==0 ) return 1; else return 0;
    }

  public static function isMobile() { return self::getDeviceAgent()->isMobile(); }
  public static function isTablet() { return self::getDeviceAgent()->isTablet(); }

}
