<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Terminal extends Base {

    //
    public $table='terminals';
   	use SoftDeletes;
    public $storefolder='public/terminals/images';
  	public $required_on_inserts=[];


  	public $other_inputs_on_inserts=['imei','mac','ip'];

  //validation
 	public static  function validate($input){
	    $rules = array(

	    );
	    $messages = array(

	    );

	    return Validator::make($input, $rules, $messages);
  	}

	 
    public static function getone(){return self::find(1);}

}
