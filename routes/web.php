<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;


//routes d'authentifications
Route::group(['namespace'=>'Auth','prefix'=> '/auth'],function(){
  Route::get('login',['as' => 'login', 'uses' => 'LoginController@showLoginForm'] );
  Route::get('register',['as' => 'register', 'uses' => 'LoginController@showRegisterForm'] );

  Route::get('logout',['as' => 'logout', 'uses' => 'LoginController@logout'] );
  Route::post('login',['as' => 'p_login', 'uses' => 'LoginController@login'] );


});

Route::post('register', 'UserController@store');




//http
Route::get('/getwebsitehttp', function () {
  return url('/');
});


//pour les tests
Route::get('/test', function () {
  return ':)';
});


//pour vider le cache de laravel
Route::get('/clearcache', function () {
  $exitCode = Artisan::call('cache:clear');
  return '<h1>Cache facade value cleared</h1>';
});


  Route::get('/control', 'FrontendController@index');

  Route::get('/etatpresence', 'PresenceController@etat');
  Route::post('/etatpresence', 'PresenceController@etat');


Route::get('/journalentrees', 'PresenceController@journalentrees');
  Route::post('/journalentrees', 'PresenceController@journalentrees');




//gestion des telechargements de fichiers excels vides
Route::get('/download/{id}','ExcelController@download');

//pour retourner le chemin d'une image mignature

Route::get('/download-mini-picture-{taille}/{type}/{filename}','HomeController@DownloadMiniPicture')->name('download-mini-picture');
//pour retourner le chemin d'une image
Route::get('/download-picture/{type}/{filename}', function($type=null ,$filename = null)
{
  $path = public_path('storage/app/public/'.$type).'/'. $filename;
  if (file_exists($path)) {
    return Response::download($path);
  }
})->name('download-picture');


//pour retourner le chemin d'une image
Route::get('images/{type}/{filename}', function($type=null ,$filename = null)
{
  $path = public_path('storage/app/public/'.$type).'/'. $filename;
  if (file_exists($path)) {
    return Response::download($path);
  }
})->name('images');

Route::get('images/{type}/{filename}', function($type=null ,$filename = null)
{
  $path = public_path('storage/app/public/'.$type).'/'. $filename;
  if (file_exists($path)) {
    return Response::download($path);
  }
})->name('images');

//envoie des mails
Route::get('/sendmail',function(){
  $to  = 'aidan@example.com' . ', '; // note the comma
  $to .= 'wez@example.com';
  $destinataire='thierrytomety@gmail.com';
  $headers = "From: \"".trans('message.applicationname')."\"<info@".trans('message.applicationname').".com>\n";
  $headers .= "Reply-To: info@".trans('message.applicationname').".com\n";
  $headers .= "Content-Type: text/html; charset=\"utf-8\"";
  $sujet ='hello';
  $monmessage =view('emails.newsletter');
  mail($destinataire, $sujet, $monmessage, $headers);


});





  Route::resource('/','HomeController');

//---------------------------BANCKEND---------------------------
Route::group(['middleware' => ['auth']],function(){

  Route::get('/home', 'HomeController@index2');


  //racine
  Route::get('/admin', function () {
    return view('backend.home');
  });


  Route::resource('/parametres','ParametreController');
  Route::resource('/permissions','PermissionController');
  Route::resource('/presences','PresenceController');
  Route::resource('/terminals','TerminalController');
  Route::resource('/employes','EmployeController');
  Route::resource('/users','UsersController');
  Route::resource('/journees','JourneeController');
  Route::resource('/menus','MenuController');
  //gestion des profils utilisateur
  Route::resource('/profils','ProfilController');

  //gestion des telechargements de fichiers excels vides
  Route::get('/download/{id}','ExcelController@download');


  //route utilisee pour retourner des vues par des requettes ajax
  Route::get('ecran', function (Illuminate\Http\Request $request) {
      if (!$request->has('fichier')) return 0;
      $fichier = $request->fichier;
      if ($request->has('val')) $val = $request->val; else $val ='';
      if ($request->has('param')) $param = $request->param; else $param='';
      return view($fichier, array('val' => $val, 'param' => $param));
  });

  /*mouchard*/
  Route::get('mouchards', 'UserController@mouchards');
  Route::get('rapport', 'UserController@rapport');

  Route::get('connexions', 'UserController@connexions');

  Route::get('journaloperations', 'UserController@journaloperations');

  Route::post('journaloperationsdate', 'UserController@journaloperationsdate');

  Route::post('mouchardsdate', 'UserController@mouchardsdate');

  Route::post('connexionsdate', 'UserController@connexionsdate');

  /*mouchard*/



});
