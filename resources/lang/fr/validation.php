<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

    /* CONVENTION
     *les valeurs retournees doivent commencer par une lettre
     * et le nom des elements de tableau doit etre sans espace
     * et pourrait etre separe par le caractere '_'
     * majuscule
     */


	//"accepted"             => "The :attribute must be accepted.",
	"confirmationechoue" => "Confirmation de mot de passe échouée !",
	"enregistrementechoue" => "Enregistrement échouée !",
	"loginutilise" => "Le login est déja utilisé !",
	"enregistrementeffectue" => "Enregistrement effectué  avec succès !",
	"erreurenregistrement" => "Erreur lors de l\'enrégistrement !",
	"comptedesactive" => "Le compte a été désactivé avec succès",
	"libelleutilise" => "Ce libellé est déjà utilisé",

    'libelle_required' => 'Le libellé est obligatoire.',
    'imagerequise' => 'L\'illustration (image) est requise.',
    'duree_required' => 'La durée est requise.',
    'code_required' => 'Le code est requis.',
    'typemenu_id_required' => 'Le type de menu est obligatoire.',
    'redacteur_id_required' => 'Le rédacteur est obligatoire.',
    'nomprofil_required' => 'Veuiller saisir une designation pour le profil..',
    'nomprofil_between' => 'La designation du profil doit être comprise entre :min et :max caractères..',
    'annee_required' => 'L\'année du bilan n\'a pas été spécifié.',
    'login_required' => 'Le login est obligatoire.',
    'pays_required' => 'Veuillez renseigner le pays.',
    'login_min' => 'Le login doit dépasser :min caractères.',
    'mail_e_mail' => 'Le mail saisi est invalide.',
    'password_required' => 'Le mot de passe est obligatoire.',
    'password_min' => 'Le mot de passe doit dépasser :min caractères.',
    'raisonsociale_min' => 'La raison sociale doit dépasser :min caractères.',
    'libelle_min' => 'Le libellé doit dépasser :min caractères.',
    'nom_min' => 'Le nom doit dépasser :min caractères.',
    'prenom_min' => 'Le prénom doit dépasser :min caractères.',

    //------------------------------------------------------------------------
    'code_required' => 'Vous devez rensigner le code.',
    'quartier_required' => 'Vous devez rensigner le quartier.',
    'tel_required' => 'Vous devez rensigner le numéro de téléphone.',
    'imei_required' => 'Vous devez rensigner le Imei.',
    //------------------------------------------------------------------------
    "image_image" => 'Vous devez choisir une image valide.',
    "image_mimes" => 'Vous devez choisir une image au format valide.',

//----------------------------------------------------------------------------
    'pays_id_required' => 'Veuiller renseigner le pays.',
    'client_id_required' => 'Veuiller renseigner le client.',
    'partenaire_id_required' => 'Veuiller renseigner le partenaire associé.',
    'pointvente_id_required' => 'Veuillez renseigner le point de vente associé.',
    'code_required' => 'Veuiller renseigner le code.',
    'contact_required' => 'Veuiller renseigner contact.',
    'client_id_required' => 'Veuillez renseigner le client.',
    'code_required' => 'Veuillez renseigner le code.',
    "codeutilise" => "Ce code est déjà utilisé",
    "partenaireexistant" => "Un partenaire portant ces identifiants a déja été enrégistré",
    "contatcexistantt" => "Ce contact a déjà été enrégistré",
//----------------------------------------------------------------------------
    "datepublication_date" => "Veuillez renseigner une date de publication valide.",
    "datedesactivation_date" => "Veuillez renseigner une date de désactivation valide.",
    "redacteur_id_required" => "Veuillez renseigner le rédacteur.",

//-----------------------------------------------------------------------------
    "contenu_langue_id_required" => "La langue est requise pour un contenu.",
    "contenu_article_id_required" => "L'article pour un contenu absent.",
    "contenu_titre_required" => "Veuillez renseigner le titre d'un contenu.",

//-----------------------------------------------------------------------------
    "date_retrait_superieur_date_attribution" => 'La date et l\'heure de retrait doit être supérieur à la date et l\'heure d\'attribution !',
    "date_desactivation_superieur_date_activation" => 'La date et l\'heure de retrait doit être supérieur à la date et l\'heure d\'attribution !',
    "veuillez_renseigner_date_retrait_profil" => 'Veuillez renseigner la date de retrait du profil !',
    "veuillez_renseigner_date_desactivation" => 'Veuillez renseigner la date de désactivation !',
//-----------------------------------------------------------------------------
    //publicite
    "image_required" => 'Veuillez sélectionner une image valide',
    "bouquet_id_required" => 'Veuillez sélectionner un bouquet',
    "typepublicite_id_required" => 'Veuillez sélectionner un type de publicité',
    "menu_id_required" => 'Veuillez sélectionner un menu',
    "datedebut_required" => 'Veuillez renseigner une date de début de publication !',
    //ville
    "ville_pay_id_required" => 'Veuillez renseigner le pays d\'appartenance',
    "nomville_required" => 'Veuillez renseigner le nom de la ville',
    'pay_id_numeric' => 'Veuillez renseigner le pays d\'appartenance',
    //typepublicite
    "nomtypepublicite_required" => 'Veuillez renseigner le nom du type de publicité',
    "typepublicite_code_required" => 'Veuillez renseigner le code du type de publicité',
    //typeproduit
    "nomtypeproduit_required" => 'Veuillez renseigner le nom du type de produit',
    //typepieces
    "extentionstypepiece_required" => 'Veuillez renseigner un type d\'extension',
    "nomtypepiece_required" => 'Veuillez renseigner la désignation du type de pièces jointes',
    //typemenu
    "typemenu_required" => 'Veuillez renseigner le nom du type de menus',
    "typemenu_min" => 'Le nom du type de menus doit être de plus de 3 caractères',
    //redacteur
    "nomredacteur_required" => 'Veuillez renseigner le nom du rédacteur',
    "nomredacteur_min" => 'Le nom du rédacteur doit être de plus de 3 caractères',
    "prenomredacteur_required" => 'Veuillez renseigner le prénom du rédacteur',
    "prenomredacteur_min" => 'Le prénom du rédacteur doit être de plus de 3 caractères',
    //pays
    "indicatifpay_required" => 'Veuillez renseigner l\'indicatif',
    "isopay_required" => 'Veuillez renseigner le code ISO',
    "iso3pay_required" => 'Veuillez renseigner le code ISO3',
    "nom_frpay_required" => 'Veuillez renseigner le nom en français',
    "nom_enpay_required" => 'Veuillez renseigner le nom en anglais',
    "indicatifpay.numeric"  => 'L\'indicatif doit être numérique',
    //langue
    "nom_required" => 'Veuillez renseigner le nom',
    //infosbase
    "description_required" => 'Veuillez renseigner la description',
    "langue_id_required" => 'Langue obligatoire',
    "logo_image" => 'Vous devez choisir une image valide pour le logo.',
    "photo" => 'Vous devez choisir une image valide pour la photo.',
    "logo_mimes" => 'Vous devez choisir une image au format valide pour le logo.',
    //categorie
    "nomcategorieproduit_required" => 'Veuillez renseigner un nom de catégorie',

    //bouquet
    "nombouquet_required"  => 'Veuillez renseigner un libellé de bouquet',
    "duree_required" => 'Veuillez renseigner la durée en nombre de mois',
    "duree_numeric" => 'Veuillez renseigner la durée en nombre de mois',

    //produit
    "nomproduit_required" => 'Veuillez renseigner le nom du produit',
    "nomproduit_unique" => 'Veuillez renseigner un autre nom de produit',
    "typeproduit_id_required" => "Veuillez sélectionner un type de produit",
    "annonceur_id_required" => "Veuillez sélectionner un annonceur",
    "categorieproduit_id_required" => "Veuillez sélectionner une catégorie de produit",
    "imageproduit_mimes" => "Veuillez choisir une image valide",
    "imageproduit_image"  => "Veuillez choisir une image pour le produit",
    //annonceur
    "raisonsociale_required" => "Veuillez renseigner la raison sociale de l'annonceur",
    "entete_required"  => "Veuillez renseigner l'entête de l'adresse",
    "ville_id_required" => "Veuillez sélectionner une ville",
    "email_email" => "Veuillez renseigner un email valide",

  //---------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  //publicite
  "image_required" => 'Veuillez sélectionner une image valide',
  "bouquet_id_required" => 'Veuillez sélectionner un bouquet',
  "typepublicite_id_required" => 'Veuillez sélectionner un type de publicité',
  "menu_id_required" => 'Veuillez sélectionner un menu',
  "datedebut_required" => 'Veuillez renseigner une date de début de publication !',
  "datedebut_date" => 'Veuillez renseigner une date de début de publication valide !',
  "datedebut_after" => 'Veuillez renseigner une date de début de publication >= a la date du jour !',
  "datefin_date" => 'Veuillez renseigner une date de fin de publication valide !',
  "datefin_after" => 'Veuillez renseigner une date de fin de publication >= a la date de début de publication !',
  //ville
  "ville_pay_id_required" => 'Veuillez renseigner le pays d\'appartenance',
  "nomville_required" => 'Veuillez renseigner le nom de la ville',
  "nomville_unique" => 'Le nom de la ville est déja pri, renseigner un autre',
  'pay_id_numeric' => 'Veuillez renseigner le pays d\'appartenance',
  //typepublicite
  "nomtypepublicite_required" => 'Veuillez renseigner le nom du type de publicité',
  "nomtypepublicite_unique" => 'Un nom du type de publicité est déja pri, renseigner un autre',


  "typepublicite_code_required" => 'Veuillez renseigner le code du type de publicité',
  "typepublicite_code_unique" => 'Le code du type de publicité est déja pri, renseigner un autre',
  //typeproduit
  "nomtypeproduit_required" => 'Veuillez renseigner le nom du type de produit',
  "nomtypeproduit_unique" => 'Le nom du type de produit  est déja pri, renseigner un autre',
  //typepieces
  "extentionstypepiece_required" => 'Veuillez renseigner un type d\'extension',
  "nomtypepiece_required" => 'Veuillez renseigner la désignation du type de pièces jointes',
  "nomtypepiece_unique" => 'Le nom du type de pièces jointes  est déja pri, renseigner un autre',
  //typemenu
  "typemenu_required" => 'Veuillez renseigner le nom du type de menus',
  "typemenu_min" => 'Le nom du type de menus doit être de plus de 3 caractères',
  //pays
  "indicatifpay_required" => 'Veuillez renseigner l\'indicatif',
  "isopay_required" => 'Veuillez renseigner le code ISO',
  "iso3pay_required" => 'Veuillez renseigner le code ISO3',
  "nom_frpay_required" => 'Veuillez renseigner le nom en français',
  "nom_enpay_required" => 'Veuillez renseigner le nom en anglais',
  "indicatifpay.numeric"  => 'L\'indicatif doit être numérique',

  "indicatifpay_unique" => 'L\'indicatif  est déja pri, renseigner un autre',
  "isopay_unique" => 'Le code ISO est déja pri, renseigner un autre',
  "iso3pay_unique" => 'Le code ISO3  est déja pri, renseigner un autre',
  "nom_frpay_unique" => 'Le nom en français est déja pri, renseigner un autre',
  "nom_enpay_unique" => 'Le nom en anglais est déja pri, renseigner un autre',

  //langue
  "nom_required" => 'Veuillez renseigner le nom',
  //infosbase
  "description_required" => 'Veuillez renseigner la description',
  "langue_id_required" => 'Langue obligatoire',
  "logo_image" => 'Vous devez choisir une image valide pour le logo.',
  "logo_mimes" => 'Vous devez choisir une image au format valide pour le logo.',
  //categorieproduit
  "nomcategorieproduit_required" => 'Veuillez renseigner un nom de catégorie',
  "nomcategorieproduit_unique" => 'Le nom de catégorie est déja pri, renseigner un autre',

  //bouquet
  "nombouquet_unique" => 'Un libellé de bouquet est déja pri, renseigner un autre',
  "nombouquet_required"  => 'Veuillez renseigner un libellé de bouquet',
  "duree_required" => 'Veuillez renseigner la durée de validité en nombre de mois',
  "duree_numeric" => 'Veuillez renseigner la durée en nombre de mois',

  //produit
  "nomproduit_required" => 'Veuillez renseigner le nom du produit',
  "nomproduit_unique" => 'Veuillez renseigner un autre nom de produit',
  "typeproduit_id_required" => "Veuillez sélectionner un type de produit",
  "annonceur_id_required" => "Veuillez sélectionner un annonceur",
  "categorieproduit_id_required" => "Veuillez sélectionner une catégorie de produit",
  "imageproduit_mimes" => "Veuillez choisir une image valide",
  "imageproduit_image"  => "Veuillez choisir une image pour le produit",
  //annonceur
  "raisonsociale_required" => "Veuillez renseigner la raison sociale de l'annonceur",
  "raisonsociale_unique" => "La raison sociale de l'annonceur est déja pri, renseigner un autre",
  "entete_required"  => "Veuillez renseigner l'entête de l'adresse",
  "ville_id_required" => "Veuillez sélectionner une ville",
  "email_email" => "Veuillez renseigner un email valide",
  //disposition
  "nomdisposition_required" => "Veuillez renseigner le libellé de la disposition",
  "nomdisposition_unique" => "Le libellé de la disposition est déja pri, renseigner un autre",
  "codedisposition_required" => "Veuillez renseigner le code de la disposition",
  "codedisposition_unique" => 'Le code de la disposition est déja pri, renseigner un autre',
  //commande publicite
  "adejaspayepouravoircetespace" => 'Espace publicitaire déja réservé',
  "nepeutpasavoirceservice" => "Espace publicitaire non disponible pour le moment",
  "annonceurselectionnenonexistantdanslepayschoisi" => "Annonceur selectionné non existant dans le pays choisi",
  //annonceur-service
  "adejaspayepouravoirceservice"  => 'Service déja payé',
  //annonceur-location-repertoire
  "adejaspayepouretredanscerepertoire" => 'Répertoire déjà réservé',




  /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/


];
