<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lignes correspondantes aux differents menus dans backend et frontend
    |--------------------------------------------------------------------------
    |
    | Chaque ligne correspond a un menu
    */

    //'text'             => 'Content.',
    //'text'      => 'The :attribute :date.',
    //'text'              => [
    //    'val' => 'content',
    //],

    /* CONVENTION
     *tout les menus contenant une URL devront
     *se terminer par 's' et celles qui ne contiendront pas
     *d'URL seront sans 's'
      *les valeurs retournees doivent commencer par une lettre majuscule
     * et le nom des elements de tableau doit etre sans espace
     * et pourrait etre separe par le caractere '_'
     *
     */

    //-------------backend-----------------------
    'menus'              => 'Liste des menus',
    'administration'              => 'Administration',
    'pays'              => 'Liste des pays',
    'typemenus'              => 'Type de menu',
    'profils'              => 'Profils utilisateur',
    'users'              => 'Comptes utilisateurs',
    'langues'              => 'Liste des langues',
    'categorieproduits'     => 'Catégorie de produits',
    'typeproduits'              => 'Liste des types de produits',
    'typepieces'              => 'Liste des types de pièces jointes',
    'infobases'              => 'Les informations de bases',
    'annonceurs'              => 'Liste des annonceurs',
    'produits'              => 'Liste des produits',
    'publicites'              => 'Les publicités',
    'typepublicites'              => 'Types de publicités',
    'bouquets'              => 'Les différents bouquets',
    'villes'              => 'Les villes',
    'articles'       => 'Liste des articles',
    'redacteurs'       => 'Liste des redacteurs',
    'tarifrepertoires' => 'Liste des tarif pour un répertoires',
    "zones" => 'Zones géographiques',
    "commande-publicitaires" => "Réservation d'espaces publicitaires",

    "annonceur-services" => "Paiement service-publicitaire",
    "services" => "Service-publicitaire",
    "annonceur-location-repertoire" => "Paiement répertoire",
    "categorieannonceurs" => "Répertoire des annonceurs",
    "durees" => "Durée d'abonnement",
    "new-postulers" => "Annonceurs adhérants",



    //-------------frontend----------------------
    'banques'              => 'BANQUES',
    'assurances'              => 'ASSURANCES',
    'voyages'              => 'VOYAGE',
    'glossaires'              => 'GLOSSAIRE',
    'voirplus'              => 'VOIR PLUS',
    'agriculture'              => 'AGRICULTURE',
    'afrique'              => 'AFRIQUE',

    //la liste des types de menu sera ici
    'frontend'              => 'Frontend',
    'backend'              => 'Backend',
    'rubrique_principale'              => 'Rubrique principale',
    'rubrique_secondaire'              => 'Rubrique secondaire',
    'afrique'              => 'AFRIQUE',
    'banques'              => 'BANQUES',
    'voyages'              => 'VOYAGES',
    'tictelecoms'              => 'TICS & TÉLÉCOMS',
    'autos'              => 'AUTOS',
    'transports'              => 'TRANSPORTS',
    'transportaeriens'              => 'TRANSPORTS AERIENS',
    'transportmaritimes'              => 'TRANSPORTS MARITIMES',
    'transportterrestres'              => 'TRANSPORTS TERRESTRES',
    'actualites'              => 'ACTUALITES',
    'entreprises'              => 'ENTREPRISES',
    'particuliers'              => 'PARTICULIERS',
    'generalites'              => 'GENERALITES',
     //la liste des types de menu sera ici
];
