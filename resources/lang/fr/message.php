<?php


    /* CONVENTION
     *les valeurs retournees doivent commencer par une lettre
     * et le nom des elements de tableau doit etre sans espace
     * et pourrait etre separe par le caractere '_'
     * majuscule
     */
return [

    'applicationname' => 'Fiedler AMS - presence control',
    'entreprisename'  => 'Fiedler AMS',
    "meta_auteur" => "Thierry Tomety",
    "meta_auteur_web" => "Thierry Tomety",
    "meta_twitter_site" => "@FiedlerAMS",
    "meta_domain" => "@FiedlerAMS",
    "log" => "login",

    //---------Menus--------------

    'parametrage' => 'parametrage',
    'parametres' => 'Paramètres',
    'deconnexion' => 'Déconnexion',
    'moncompte' => 'Mon compte',
    'villes' => 'Villes',
    'administration' => 'Administration',
    'administrateur' => 'Administrateur',
    'acceuil' => 'Acceuil',
    'pleinecran' => 'Plein écran',
    'barredemenu' => 'Barre de menu',
    'panneaudecontrol' => 'Panneau de control',
    'authentification' => 'Authentification',

    //---------Menus------------------


  //---------Droits--------------

  'ajout' => 'Ajout',
  'modification' => 'Modification',
  'suppression' => 'Suppression',
  'annulation' => 'Annulation',
  'import' => 'Import',
  'confirmation' => 'Confirmation',

  //---------Menus------------------

	//---------Formulaires------------

    'tous' => 'Tous',
    'oui' => 'Oui',
	'non' => 'Non',
	'connexion' => 'Connexion',
	'afficher' => 'Afficher',
	'creationdecompte' => 'Création de compte',
    'nomdutilisateur' => 'Nom d\'utilisateur',
    'motdepasse' => 'Mot de passe',
    'chargement' => 'Chargement',
    'parametrageinfo'  => 'Paramétrage des informations de la structure et du login',
    'categorie'  => 'Catégorie',
    'categories'  => 'Catégories',
    'designation' => 'Désignation',
    'personnemorale' => 'Personne morale',
    'personnephysique' => 'Personne physique',
    'nom' => 'Nom',
    'prenom' => 'Prénom',
    'sexe' => 'Sexe',
    'masculin' => 'Masculin',
    'feminin' => 'Féminin',
    'datedenaissance' => 'Date de naissance',
    'raisonsociale' => 'Raison sociale',
    'login' => 'Login',
    'confirmmotdepasse'  => 'Confirmation du mot de passe',
    'nouveaumotdepasse'  => 'Nouveau mot de passe',
    'ancienmotdepasse'  => 'Ancien mot de passe',
    'email' => 'E-mail',
    'newsletter' => 'Newsletter',
    'numtelephone' => 'Numéro de téléphone',
    'adresse' => 'Adresse',
    'libelle' => 'Libellé',
    'libelle' => 'Libellé',
    'pointvente' => 'Point de vente',
    'pays' => 'Pays',
    'action' => 'Action',
    'fiche' => 'Fiche',
    'actions' => 'Actions',
    'id' => 'ID',
    'recommencer' => 'Recommencer',
    'rechercher' => 'Rechercher',
    'recherche' => 'Recherche',
    'valider' => 'Valider',
    'fermer' => 'Fermer',
    'payement' => 'Payement',
    'facture' => 'Facture',
    'enregistrer' => 'Enregistrer',
    'editer' => 'Editer',
    'supprimer' => 'Supprimer',
    'enregistrer' => 'Enregistrer',
    'activer' => 'Activer',
    'desactiver' => 'Désactiver',
    'actif' => 'Actif',
    'inactif' => 'Inactif',
    'annulee' => 'Annulée',
    'annuler' => 'Annuler',
    'etat' => 'Etat',
    'saisiradresseici' => 'saisir votre adresse ici...',
    'retour' => 'Retour',
    'ecrire...' => 'Ecrire...',
    'signaler' => 'Signaler',
    'alerteconsommateur' => 'Alerte aux consommateurs',
    'signalerabus' => 'Signaler un abus',
    'signalerunproduitdeclareimproprealaconsommationdansvotrepays' => 'Signaler un produit déclaré impropre à la consommation dans votre pays',
    'optionvide'=>'---Choisir un élément---',
    'changer' => 'Changer',
    'retirer' =>'Retirer',
    'importer'=>'Importer',
    'importexcel' => 'Importer Fichier Excel',
    'importimage' => 'Importer Image',
    'image' => 'Image',
    'photo' => 'Photo',
    'photopasseport' => 'Photo passeport',
    'fichierjoint' => 'Fichier joint',
    'contact' => 'Contact',
    'contacts' => 'Contacts',
    'ajouter' => 'Ajouter',
    'inverserchoix'=> 'Sélectionner / Désélectionner tout',
    'choisir'=> 'Sélectionner',
    'entete'=> 'Entête',
    'telephone'=> 'Téléphone',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'contenu' => 'Contenu',
    'langue' => 'Langue',

    'aucun' => 'Aucun',
    //-------------------------------
    'code'=> 'Code',

    //Jour de la semaine
    'lun' => 'Lun',
    'mar' => 'Mar',
    'mer' => 'Mer',
    'jeu' => 'Jeu',
    'ven' => 'Ven',
    'sam' => 'Sam',
    'dim' => 'Dim',

  //langues
    'francais' => 'Francais',
    'anglais' => 'Anglais',
    'arabe' => 'Arabe',
    'portugais' => 'Portugais',

    //categorie produits
  'codecategorie' =>'Code de la categorie',
  'libellecategorie' =>'Libellé de la categorie',

  //types de menu
  'listetypemenu' =>'Liste des types de menu',
  'typemenu' =>'Type de menu',
  'enregistrementtypemenu' =>'Enregistrement d\'un type de menu',
  'modificationtypemenu' =>'Modification d\'un type de menu',

  //redacteurs
  'listeredacteur' =>'Liste des redacteurs',
  'nomredacteur' =>'Nom du rédacteur',
  'prenomredacteur' =>'Prénom du rédacteur',
  'enregistrementredacteur' =>'Enregistrement d\'un rédacteur',
  'modificationredacteur' =>'Modification d\'un rédacteur',

  //menu
  'listemenu' =>'Liste des menus',
  'menu' =>'Menu',
  'enregistrementmenu' =>'Enregistrement d\'un menu',
  'modificationmenu' =>'Modification d\'un menu',
  'codeicone' =>'Code/Classe de l\'icone',
  'iconemenu' =>'Icone du menu',
  'lienacces' =>'Lien (url) d\'acces',
  'menuparent' =>'Menu parent',
  'nommenu' =>'Libelle du menu',



    //---------Formulaires-----------

    //---------Messages--------------

    'confirmation' => 'Confirmation',
    'voulezvoussupprimer' => 'Voulez vous effectuer cette suppression?',
    'lasupressionaechouee' => 'La suppression a échouée',
    'supressioneffectue' => 'Supression éffectuée',
    'actioneffectue' => 'Action effectuée avec succès',
    'actionechouee' => 'Action échouée',
    'connexioninterompue' => 'Connexion intérompue',
    'voulezvouseffetuersupression' => 'Voulez vous effectuer cette suppression',
    'voullezvousactiver'  => "Voulez vous proceder a l activation?",
    'voullezvousdesactiver'  => "Voulez vous proceder a la désactivation ?",
    'voullezvousannuler'  => "Voulez vous proceder a la désactivation ?",
    'authfail' => 'Nom d\'utilisateur ou mot de passe erron&eacute;!',

    //---------Messages--------------

    //---------Views-----------------

    //ville
    'listeville'  => 'Liste des villes',
    'modificationville'  => 'Modification d\'une ville',
    'enregistrementville'  => 'Enregistrement d\'une ville',

    //categorie produits
  'categorieproduit'=>'Catégorie de produit',
  'categorieproduits'=>'Catégorie de produits',
  'enregistrementcategorieproduit' =>'Enregistrement d\'une catégorie de produits',
  'modificationcategorieproduit' =>'Modification d\'une catégories de produits',
  'listecategorieproduit'=>'Liste des catégories de produits',
  'enregistrementannonceurs' =>'Enregistrement d\'un annonceur',


    //produits

    'plusdedetails'=>'Plus de détails',
    'details'=>'Détails',
    'annulerladernierelivraison'=>'Annuler la dernière livraison',
    'restant'=>'Restant',
    'produit'=>'Produit',
    'produits'=>'Produits',
    'enregistrementproduit' =>'Enregistrement de produits',
    'modificationproduit' =>'Modification de produits',
    'listeproduit'=>'Liste des produits',



    //client
    'enregistrementclient' =>'Enregistrement d\'un client',
    'modificationclient' =>'Modification d\'un client',
    'listeclient'=>'Liste des clients',
    'profil' => 'Profil',
    'gestionnaire' => 'Gestionnaire',


    //profil
    'enregistrementprofil' =>'Enregistrement d\'un profil',
    'modificationprofil' =>'Modification d\'un profil',
    'listeprofil' =>'Liste des profil',


    //partenaire
    'partenaire'=>'Partenaire',
    'partenaires'=>'Partenaires',
    'listepartenaire'  => 'Liste des partenaires',
    'nbrepartenaire'=>'Nombre de partenaires',

    'modificationpartenaire'  => 'Modification d\'un partenaire',
    'enregistrementpartenaire'  => 'Enregistrement d\'un partenaire',
    'ajouternouveauxcontacts'  => 'Ajouter de nouveaux contacts',

    'modificationcontactpartenaire'  => 'Modification du contact du partenaire :name',
    'ajoutcontactpartenaire'  => 'Ajout d\'un nouveau contact pour le partenaire :name',

    //profils utilisateurs
    'modificationprofil'  => 'Modification d\'un profil',
    'enregistrementprofil'  => 'Enregistrement d\'un profil',
    'listeprofil'  => 'Liste des profils',
    'description'  => 'Description',
    'gestiojndroitsassocies'  => 'Gestion des droits associés',


    //profils utilisateurs
    'modificationuser'  => 'Modification d\'un compte utilisateur',
    'enregistrementuser'  => 'Enregistrement d\'un compte utilisateur',
    'listeuser'  => 'Liste des utilisateurs',
    'compteprive'  => 'Compte privé',

    //users
    'dateactivation'  => 'Date d\'activation',
    'datedeb'  => 'Date de début',
    'datefin'  => 'Date de fin',
    'datedesactivation'  => 'Date de désactivation',
    'heure'  => 'Heure',
    'dateattribution'  => 'Date d\'attribution',
    'dateretrait'  => 'Date de retrait',
    'dateetheure'  => 'Date et heure',


    //journal
    'utilisateurnonauthentifie' =>'Utilisateur non authentifié',
    'ouverte' =>'Ouverte',
    'journalconnexions' =>'Journal des connexions',
    'journalactions'=>'Journal des événements',

    //article
    "enregistrementarticles" => 'Enregistrement d\'un article',
    'modificationarticles'  => 'Modification d\'un article',
    'listearticles' => 'Liste des articles',

    'listeetats'  => 'Liste des états',

    //---------Pieces jointes--------

  'ajouterpiecejointe'  => 'Ajouter une piece jointe',

    //---------Pieces jointes--------
  'dispositionutilisee'  => 'Dispositions utilisée',
  'nombredemoisdevalidite'  => 'Nombre de mois de validité',
  'saisirdescription'  => 'Saisir la description',
  'nombouquet'  => 'Libellé du bouquet',


    //---------Views------------------


    //---------Frontend------------------
    'logo'  => 'Logo',
    'qui_sommes_nous'  => 'Qui sommes nous?',
    'ecritpar'  => 'Ecrit par',
    'voirplus'  => 'Voir plus',
    'plusdecontenu'  => 'Afficher plus de contenu',
    'postuler'  => 'Postuler',
    'aucunerubriquetrouvee'  => 'Aucune rubrique trouvée...',
    'rubrique'  => 'Rubrique',
    'rubriques'  => 'Rubriques',
    'dispositionindisponible'  => 'Disposition indisponible...',
    'articleindisponible'  => 'Articles indisponible...',
    'mentionlegale'  => 'Mentions Légales',
    'kitmedia'  => 'Kit Média',
    'selectionnabledanslanewsletter'  => 'Selectionnable dans la newsletter',
    'publicites'  => 'Publicités',
    'redirigerverslien'  => 'Rediriger vers le lien',
    'produitde'  => 'Produit de',
    'tousdroitsreserves'  => 'Tous droits reservés',
    'saisirmail'  => 'Saisir votre mail ici',
    'sabonner'  => 'S\'abonner',
    'recevoirnotrenewsletter'  => 'Recevoir notre newsteller',
    'nouscontacter'  => 'Nous contacter',
    'notreobjectif'  => 'Notre objectif',
    'touslespays'  => 'Tous les pays',
    'vues'  => 'Vues',
    'vue'  => 'Vue',
    'page'  => 'Page',
    'pageprecedente'  => 'Page précédente',
    'services'  => 'Services',
    'adresses'  => 'Adresses',
    'partager'  => 'Partager',
    'motscles'  => 'Mots clées',
    'commentaires'  => 'Commentaires',
    'commentaire'  => 'Commentaire',
    'commenter'  => 'Commenter',
    'envoyer'  => 'envoyer',
    'publier'  => 'Publier',
    'deposerdesmaintenantvotreoffre'  => 'Déposer dès maintenant votre offre',
    'publieruneannonce'  => 'Publier une annonce',
    'consulterlesoffres'  => 'Consulter une offre',
    'consulterlescv'  => 'Consulter les CV',
    'deposeruncv'  => 'Déposer un CV',
    'publieruneoffre'  => 'Publier une offre',
    'publieruneoffredemploie'  => 'Publier une offre d\'emploie',
    'saisirlesinformationsdansleslanguesspecifieespourtoucherunplusgrandpublic'  => 'Saisir les informations dans les langues specifiées pour toucher un plus grand public',
    'paysconcerne'  => 'Pays concerné',
    'faireuneannonce'  => 'Faire une annonce',
    'votrenom'  => 'Votre nom',
    'email'  => 'Email',
    'laisseruncommentaire'  => 'Laisser un commentaire',
    'laisserunmessage'  => 'Laisser un message',
    'formulaireincomplet'  => 'Formulaire incomplet',
    'afficherlaboutique'  => 'Afficher la boutique',
    'desideesshopping'  => 'Des idées shopping',
    'commentnoustrouver'  => 'Comment nous trouver',
    'fichierstelechargeables'  => 'Fichiers téléchargeables',
    'noussommespresentdanscespaysdafrique'  => 'Nous sommes dans ces pays d\' Afrique',
    'donnervotreappreciationsurlesite'  => 'Donner votre appreéciation sur le site',
    'nosoffres'  => 'Nos offres',
    'paiement'  => 'Paiement',
    'suivant'  => 'Suivant',
    'siteweb' => 'Site web',
    'domainedactivité' => 'Domaine',
    'accederausite' => 'Accéder au site officiel',
    "activites" => 'Activités',


    //---------Views------------------


    //---------Backend------------------


        //---------article------------------
    'article'  => 'Article',
    'actualite'  => 'Actualité',
    'archive'  => 'Archive',
    'formulairesoumisvousrecevrezunmaildeconfirmationdanslesminutesquisuivent'  => 'Formulaire soumis. Vous recevrez un mail de confirmation dans les minutes qui suivent.',
    'annoncesuccesmessage'  => 'Publication éffectuée, nous nous chargerons de l\'afficher pour une durée de trente (30) jours.',
    'menuassocie'  => 'Menu associé',
    'rubriqueassociee'  => 'Rubrique associée',

        //---------commun-------------------
    'menuassocie' =>'Menu associé',
    'annonceur' =>'Exposant',
    'salon' =>'Salon',
    'salons' =>'Salons',
    'annonceurs' =>'Exposants',
    'articles' =>'Articles',
    'archives' =>'Archives',
        //---------annonceur----------------
    "enregistrementannonceurs" =>'Enregistrement d\'un Exposant',
    "adresses" => "Adresses",
        //---------article------------------
    'article'  => 'Article',
    'menusassocie'  => 'Menu associé',

    'datepublication'  => 'Date de publication',
    'redacteur'  => 'Rédacteur',
    'auteur'  => 'Auteur',
    'soumission'  => 'Soumission',
    'dernierdiplome'  => 'Dernier diplome',
    'dernierdiplome'  => 'Dernier diplome',
    'telechargerlecv'  => 'Charger le CV',
    'telecharger'  => 'Télécharger',
    'piecesjointes'  => 'Pieces jointes',
    'CV'  => 'CV',
    'job'  => 'Job',
    'afficherpourtouslespays'  => 'Afficher pour tous les pays',
    'erreurlorsdelenvoie'  => 'Erreur Lors de l\'envoie',
    'afficherannonceur'  => 'Afficher la boutique',
    'afficherlesproduits'  => 'Afficher la boutique',

        //---------pays------------
    'listepays' =>'Liste des pays',
    'indicatifpay'  => 'Indicatif',
    'nom_frpay'  => 'Nom du pays (Français)',
    'nom_enpay'  => 'Nom du pays (Anglais)',
    'isopay' => 'Code ISO',
    'iso3pay' => 'Code ISO3',


        //----------contenu----------
    'titre'  => 'Titre',
    'motcle'  => 'Mots clés',
        //----------publicité--------
    'enregistrementpublicites' =>'Enregistrement d\'une publicité',
    'listepublicites' =>'Liste des publicités',
    'datedebut' =>'Date de début',
    'bouquet' =>'Bouquet',
    'typepublicite' =>'Type de publicité',
    'periodedevalidite' =>'Période de validité',
    'lien' =>'Lien',
        //---------villes------------
    'enregistrementvilles' =>'Enregistrement d\'une nouvelle ville',
    'listevilles'  =>'Liste des villes',
    'nomville'  =>'Nom (français)',
    'nameville'  =>'Nom (anglais)',
        //----------typepublicite--------
    'enregistrementtypepublicites' =>'Enregistrement d\'un type de publicité',
    'nomtypepublicite' =>'Nom',
        //----------typeproduit----------
    'enregistrementypeproduits' =>'Enregistrement d\'un type de produit',
    'nomtypeproduit' => 'nom',
        //----------typepiece--------------
    'enregistrementtypepieces' =>'Enregistrement d\'un type de pièces jointes',
    "nomtypepiece" => 'Désignation',
    "extentionstypepiece" => 'Type d\'extension',
        //-----------langue-------------
    "enregistrementlangues" =>'Enregistrement d\'une langue',
    "listelangues" =>'Liste des langues',
        //----------infosdebases------------
    "modificationinfobases" => "Modification d'une information de base",
    "listeinfobases" =>'Liste des informations de bases',
    "objectif" => "Objectif",
    "mail" => "Email",
    "fix" => "Téléphone fix",
    "contact1" => "Contact (1)",
    "contact2" => "Contact (2)",
    "localisation" => "Localisation",
        //-----------categorie------------
    "enregistrementcategorieproduits" =>'Enregistrement d\'une catégorie de produit',
    "listecategorieproduit" =>'Liste des catégories de produits',
    "nomcategorieproduit" =>'Nom',
    "desccategorieproduit" =>'Dèscription',
        //-----------bouquets-------------
    "enregistrementbouquets" =>'Enregistrement d\'un bouquet',
    "listebouquets" =>'Liste des bouquets',
    "montant" =>'Montant',
    "duree" =>'Durée',
    "imagebouquet" =>'Aperçu du bouquet',
    "pourununiqueannonceur" =>'Occupable par un seul Exposant',
    "description_en"=>'Description en Anglais',

         //produits
    "enregistrementproduits" =>'Enregistrement d\'un produit',
    "listeproduits"  => 'Liste des produits',
    "typeproduit" => 'Type de produits',
    "descproduit" => 'Description',
    "nomproduit" => "Nom du produit",
    "montantproduit" =>"Montant",
    "imageproduit" => "Image du produit",
    //durees
    "enregistrementdurees" =>'Enregistrement des durées',
    "typeduree_en" => "Type durée (Anglais)",
    "listedurees" => "Listes des durées d'abonnement ",
    "modificationdurees" => "Modification des durées d'abonnement ",
        //tarifrepertoires
    "modificationtarifrepertoires" => "Modification des tarifs de repertoires ",
    "listedureerepertoires" => "listes des tarifs de repertoires ",
    "enregistrementtarifrepertoires" =>'Enregistrement des tarifs de répertoires',
    "detailsdureerepertoire" => "Durée",
    "prixdureerepertoire" => "Prix",
    "typedureerepertoire_en" => "Type durée (Anglais)",
    //zone
    "zone" => "Zones géographiques",
    "nomzone" => "nom",
    "nomzone_en" => "nom en Anglais",
    "enregistrementzones"  =>'Enregistrement des zones géographiques',
    "modificationzones" =>'Modification des zones géographiques',
    "listezones" =>'Liste des zones géographiques',
    //commandes publicitaires
    "enregistrementcommandepublicites" => "Enregistrement des réservation d'espaces publicitaires",
    "listecommandespublicites"  =>"Liste des réservation d'espaces publicitaires",
    "detailsduree" => "Durée",
    "paye" => "Payé",
    "statut" => "status",
    "modificationcommandepublicites"  => "Modification des réservation d'espaces publicitaires ",
    //annonceur-service
    "enregistrementcommandesannonceurs" => "Enregistrement des paiement service-publicitaire",
    "service" => "Genre de service",
    "listecommandesannonceurs" => "Liste des paiement service-publicitaire",
    //services
    "imageservice" => "Image",
    "nomservice" => "Nom du service",
    "detailsservice" => "Détail du service",
    "detailsservice_en" => "Détail du service en Anglais",
    "saisirdetailsservice"  => "Saisir détail du service",
    "saisirdetailsservice_en"  => "Saisir détail du service en ANglais",
    "listeservices" => "Liste des services publicitaires",
    "enregistrementservices" => "Enregistrement des services publicitaires",
    "modificationservices"  => "Modification des services publicitaires ",
    //categorie-annonceur
    "repertoire" => "Répertoires annonceurs",
    "listecategorieannonceurs"  => "Liste des répertoires annonceurs",
    "modificationcategorieannonceurs" => "Modification répertoires ",
    //annonceur-location-repertoire
    "locationrepertoires" =>"Réservation répertoires ",
    "annonceur-location-repertoire" =>"Liste des réservations de répertoires ",


    "categorieannonceurs" => "Repertoire des annonceurs",
    "enregistrementcategorieannonceurs" => "Enregistrement des repertoires",
    "nomcategorieannonceur" => "Intitule du repertoire",
    "desccategorieannonceur" => "Descriptif",
    "cacherlesannonceurs" => "Cacher les annonceurs",
    //annonceur_tmp
    "listenouveauxannonceurs" => "Liste des nouveaux annonceurs",
    "modificationnouveauxannonceurs" => "Migration des nouveaux annonceurs",
    "dejaexistant" => "Annonceur déjà existant",

    "descriptionenportugaise" => "Description en portugais",
    "descriptionenanglais" => "Description en anglais",
    "descriptionenfrancais" => "Description en francais",
    "descriptionenarabe" => "Description en arabe",


    "prix" => "Prix",
    "tel" => "Tel",


    "corbeille" => "Corbeille",
    "restaurer" => "Restaurer",



    "stat_menus" => "Menus",
    "stat_articles" => "Articles",
    "stat_articles_lus" => "Articles lus",
    "stat_internautes" => "Internautes",
    "stat_annonceurs" => "Annonceurs",
    "stat_entreprises" => "Entreprises",
    "stat_produits" => "produits",
    "combien_donneriez_vous_a_ce_site" => "Combien donneriez vous à ce site ?",
    "donner_un_avis" => "Donner un avis",
    "merci" => "Merci",

    "annoncesgratuites" => "Annonces gratuites",
    "home" => "Accueil",
    "shopping" => "Shopping",
    "entreprises" => "Entreprises",
    "entreprise" => "Entreprise",
    "meta_admin" => "metadamin",


  //newsletter
  'cesnouveauxarticlespourraientvousinteresser'=>'Ces nouveaux articles pourraient vous interesser',
  'cliquericipourneplusrecevoirnosnews'=>'Cliquer ici pour ne plus recevoir nos news',
  'mercidavoirsouscritanotrenewsletter'=>'Merci d\'avoir souscrit à notre newsletter',


  'lequipe'=>'L\'équipe',

  //
  "detailsdesbouquets" => "Description",
  "espacepub" => "Espaces publicitaires",
    "caracteristique" => "Caracteristique",
    "tarif" => "Tarif",
    "identifiantcompte"=> "Identifiant de compte",
    "confirmationmotdepasse"=> "Confirmer mot de passe",
    "connectezvous" => "Connectez - vous",
    "fonction" => "Fonction",
    "Payer"=> "Suivant",
    "reprendre" => "Reprendre",
    "terminer" => "Terminer",
    "finalisation" => "Finalisation",
    "repertoires" => "Liste de répertoire",
    "choixdurepertoire"  => "Choix de répertoire",
    "detailsderepertoires" => "Description d'un répertoire",
    "choixduservice" => "Choix du service",
    "detailsdeservices" => "Description du service",





];
