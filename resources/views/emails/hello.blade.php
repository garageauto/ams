@extends('beautymail::templates.widgets')

@section('content')

  @include('beautymail::templates.widgets.articleStart' , ['color' => '#ac0202'])
  <?php
  $infodebase=\App\Langue::getByCode('fr');
  if($infodebase!=null) $infodebase=$infodebase->infobase;
  ?>
  @if($infodebase!=null)
    <div style="width: 100%; padding: 10px; margin-bottom: 10px; background-color: #ac0202">
      <a class="logo" href="{{trans('message.applicationurl')}}" style="width: 100px; margin: auto">
        <img src="{{url('/storage/app/public/infobases/images/'.$infodebase->logo)}}" style="margin: auto" title="logo {{trans('message.applicationname')}}" width="40%" class="footer-logo" alt="Logo">
      </a>
    </div>
  @endif
  <h2 class="secondary"><strong>{{trans('message.applicationname')}}</strong></h2>
  <p>{{trans('message.mercidavoirsouscritanotrenewsletter')}}</p>


  <address class="md-margin-bottom-40">
    <p>{{$infodebase->adresse}}</p>
    {{$infodebase->contact1}}<br>
    {{$infodebase->contact2}}<br>
    Email: <a href="mailto:contact@focusafrik.com" class="">contact@focusafrik.com</a>
  </address>

  @include('beautymail::templates.minty.button', ['text' => trans('message.applicationname'), 'link' => url('/')])

  <small>{{$infodebase->description}}</small>
  <small><a href="{{url('/nonewsagain')}}">{{trans('message.cliquericipourneplusrecevoirnosnews')}}</a></small>

  <div>
    <a href="#">{{trans('message.publicites')}}</a> | <a href="#">{{trans('message.mentionlegale')}}</a>
    {{date('Y')}} © {{trans('message.tousdroitsreserves')}}  {{trans('message.produitde')}} | <a href="https://www.agenceforceone.com/">{{trans('message.entreprisename')}}</a>
  </div>

  @include('beautymail::templates.widgets.articleEnd')


@stop
