@extends('beautymail::templates.widgets')

@section('content')

  @include('beautymail::templates.widgets.articleStart' , ['color' => '#ac0202'])
  <?php
  $infodebase=\App\Langue::getByCode('fr');
  if($infodebase!=null) $infodebase=$infodebase->infobase;
  ?>
  @if($infodebase!=null)
    <div style="width: 100%; padding: 10px; margin-bottom: 10px; background-color: #ac0202">
        <a class="logo" href="{{trans('message.applicationurl')}}" style="width: 100px; margin: auto">
          <img src="{{url('/storage/app/public/infobases/images/'.$infodebase->logo)}}" style="margin: auto" title="logo {{trans('message.applicationname')}}" width="40%" class="footer-logo" alt="Logo">
        </a>
    </div>
   @endif
  <h2 class="secondary"><strong>{{trans('message.newsletter')}} {{trans('message.applicationname')}}</strong></h2>
  <p>{{trans('message.cesnouveauxarticlespourraientvousinteresser')}}</p>
  @php($lang='fr')
  @php($articles=\App\Article::getNews($lang))
  <div style="width: 100%">
  @foreach($articles as $article)
    @php
      if (strlen($article->description) > 200) $article->description = strtolower(htmlentities(strval(substr($article->description, 0, 200) . '...'), ENT_NOQUOTES));
      if (strlen($article->titre) > 70) $article->titre =  strtolower(htmlentities(strval(substr($article->titre,  0, 70) . '...'), ENT_NOQUOTES));
    @endphp
    <div style="width: 100%; padding: 10px;margin-bottom:10px;color: #0e2231">
      <h4>{{$article->titre}}</h4>
      <small>{{$article->created_at}}</small>
      <p style="color: #555555"><?php echo $article->description ?></p>
      <a style="float: right" href="{{url('/article/'.$article->code.'/'.$lang.'-all')}}">{{trans('message.voirplus')}}</a>
    </div>
  @endforeach
  </div>

  <address class="md-margin-bottom-40">
    <p>{{$infodebase->adresse}}</p>
    {{$infodebase->contact1}}<br>
    {{$infodebase->contact2}}<br>
    Email: <a href="mailto:contact@focusafrik.com" class="">contact@focusafrik.com</a>
  </address>

  @include('beautymail::templates.minty.button', ['text' => trans('message.applicationname'), 'link' => trans('message.applicationurl')])

  <small>{{$infodebase->description}}</small>
  <small><a href="{{url('/nonewsagain')}}">{{trans('message.cliquericipourneplusrecevoirnosnews')}}</a></small>

  <div style="margin-top: 5px">
    <a href="#">{{trans('message.publicites')}}</a> | <a href="#">{{trans('message.mentionlegale')}}</a>
    {{date('Y')}} © {{trans('message.tousdroitsreserves')}}  {{trans('message.produitde')}} | <a href="https://www.agenceforceone.com/">{{trans('message.entreprisename')}}</a>
  </div>

  @include('beautymail::templates.widgets.articleEnd')


@stop
