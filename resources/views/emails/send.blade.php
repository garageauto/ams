@extends(frontendview('template2'))
@section('wrapper')

  
  <?php
  $infodebase=\App\Langue::getByCode('fr');
  if($infodebase!=null) $infodebase=$infodebase->infobase;
  ?>
  @if($infodebase!=null)
    <div style="width: 100%; padding: 10px; margin-bottom: 10px; background-color: #ac0202">
      <a class="logo" href="{{trans('message.applicationurl')}}" style="width: 100px; margin: auto">
        <img src="{{url('/storage/app/public/infobases/images/'.$infodebase->logo)}}" style="margin: auto" title="logo {{trans('message.applicationname')}}" width="40%" class="footer-logo" alt="Logo">
      </a>
    </div>
  @endif
   <h2 class="secondary"><strong>{{trans('message.newsletter')}} {{trans('message.applicationname')}}</strong></h2>
  <p>{{trans('message.facture')}} </p>
  
  <div class="example-pay-grid" >
          <div class="row">
            
            <div class="col-md-4">
              <h4>
                <img class="margin-right-10" src="../../assets/images/logo-blue.png"
                alt="...">{{trans('message.applicationname')}}</h4>
              <address>
                {{$payements['annonceur']->nom_frpay}}
                <br>
                <abbr title="Mail">E-mail:</abbr>&nbsp;&nbsp;{{'info@'.trans('message.applicationname').'.com'}}
                <br>
                <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(228) 456-7890
                <br>
                <abbr title="Fax">Fax:</abbr>&nbsp;&nbsp;fax
              </address>
            </div>
            <div class="col-md-4 col-md-offset-4 text-right">
             
              <p>
                <a class="font-size-20" href="javascript:void(0)">#{{$payements['commande']['id']}}</a>
                <br> {{trans('message.to')}}:
                <br>
                <span class="font-size-20">@if($payements['annonceur'] != Null && $payements['annonceur']->raisonsociale != Null){{$payements['annonceur']->raisonsociale}}@endif</span>
              </p>
              <address>
                795 Folsom Ave, Suite 600
                <br> San Francisco, CA, 94107
                <br>
                <abbr title="Phone">P:</abbr>&nbsp;&nbsp;(123) 456-7890
                <br>
              </address>
              <span>Invoice Date: January 20, 2015</span>
              <br>
              <span>Due Date: January 22, 2015</span>
            </div>
          </div>
          @if( $payements != NULL)
          <div class="page-invoice-table table-responsive">
            <table class="table table-hover ">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Durée</th>
                  
                  
                  <th class="text-right">PU</th>
                  <th class="text-right"></th>
                </tr>
              </thead>
              <tbody>
                @foreach($payements['panier'] as $k => $v)
                <tr>
                  <td>{{$v['service']}}</td>
                  <td>{{$v['duree']}} mois</td>
                  
                  <td  class="text-right">{{$v['prix']}} Fcfa</td>
                  <td  class="text-right"></td>
                </tr>
                @endforeach
                
                
              </tbody>
            </table>
          </div>

          <div class="text-right clearfix">
            <div class="pull-right">
              <p>Sous-totale &nbsp;:
                <span>{{$payements['subtotal']}} Fcfa</span>
              </p>
              <p>TVA:
                <span>{{$payements['tva']}}%</span>
              </p>
              <p class="page-invoice-amount">Totale &nbsp;:
                <span>{{$payements['total']}} Fcfa</span>
              </p>
            </div>
          </div>
          @endif
          
</div>




@stop