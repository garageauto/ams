@extends('beautymail::templates.widgets')

@section('content')

  @include('beautymail::templates.widgets.articleStart')

  <?php
  $infodebase=\App\Langue::getByCode('fr');
  if($infodebase!=null) $infodebase=$infodebase->infobase;
  ?>
  @if($infodebase!=null)
    <div style="width: 100%; padding: 10px; margin-bottom: 10px; background-color: #ac0202">
      <a class="logo" href="{{trans('message.applicationurl')}}" style="width: 100px; margin: auto">
        <img src="{{url('/storage/app/public/infobases/images/'.$infodebase->logo)}}" style="margin: auto" title="logo {{trans('message.applicationname')}}" width="40%" class="footer-logo" alt="Logo">
      </a>
    </div>
  @endif
  <h2 class="secondary"><strong>{{trans('message.newsletter')}} {{trans('message.applicationname')}}</strong></h2>
  <p>{{trans('message.facture')}} N</p>
  {{dd($payements)}}
  <div class="example-pay-grid" style="display: none">
          <div class="row">
            
            <div class="col-md-4">
              <h4>
                <img class="margin-right-10" src="../../assets/images/logo-blue.png"
                alt="...">FOCUSAFRIC.com</h4>
              <address>
                Lomé-TOGO
                <br>
                <abbr title="Mail">E-mail:</abbr>&nbsp;&nbsp;focusafric@google.com
                <br>
                <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(228) 456-7890
                <br>
                <abbr title="Fax">Fax:</abbr>&nbsp;&nbsp;fax
              </address>
            </div>
            <div class="col-md-4 col-md-offset-4 text-right">
              <h4>Invoice Info</h4>
              <p>
                <a class="font-size-20" href="javascript:void(0)">#5669626</a>
                <br> To:
                <br>
                <span class="font-size-20">@if($payements['annonceur'] != Null && $payements['annonceur']->raisonsociale != Null){{$payements['annonceur']->raisonsociale}}@endif</span>
              </p>
              <address>
                795 Folsom Ave, Suite 600
                <br> San Francisco, CA, 94107
                <br>
                <abbr title="Phone">P:</abbr>&nbsp;&nbsp;(123) 456-7890
                <br>
              </address>
              <span>Invoice Date: January 20, 2015</span>
              <br>
              <span>Due Date: January 22, 2015</span>
            </div>
          </div>
          @if( $payements != NULL)
          <div class="page-invoice-table table-responsive">
            <table class="table table-hover ">
              <thead>
                <tr>
                  <th>Description</th>
                  <th>Durée</th>
                  
                  
                  <th class="text-right">PU</th>
                  <th class="text-right"></th>
                </tr>
              </thead>
              <tbody>
                @foreach($payements['panier'] as $k => $v)
                <tr>
                  <td>{{$v['service']}}</td>
                  <td>{{$v['duree']}} mois</td>
                  
                  <td  class="text-right">{{$v['prix']}} Fcfa</td>
                  <td  class="text-right"></td>
                </tr>
                @endforeach
                
                
              </tbody>
            </table>
          </div>

          <div class="text-right clearfix">
            <div class="pull-right">
              <p>Sous-totale &nbsp;:
                <span>{{$payements['subtotal']}} Fcfa</span>
              </p>
              <p>TVA:
                <span>{{$payements['tva']}}%</span>
              </p>
              <p class="page-invoice-amount">Totale &nbsp;:
                <span>{{$payements['total']}} Fcfa</span>
              </p>
            </div>
          </div>
          @endif
          <div class="text-right">
            <input class="form-control input-sm "  value="{{$payements['commande']['id']}}"   name="id" type="hidden"  id="hidden_id">
            <input class="form-control input-sm "  value="@if(isset($payements['commande']['datedebut'])){{$payements['commande']['datedebut']}}@endif"   name="datedebut" type="hidden"  id="hidden_id">
            <input class="form-control input-sm "  value="{{$data['annonceur']}}"   name="annonceur_id" type="hidden" id="hidden_annonceur_id">
            <input class="form-control input-sm "  value="{{$data['genre']}}"   name="genre" type="hidden" id="hidden_genre">
            @if($payements['commande']['paye'] == 0)
              <button type="button" onclick ="$('.formulaire-modal').trigger('submit')" class="btn btn-animate btn-animate-side btn-primary">
              <span><i class="icon wb-shopping-cart" aria-hidden="true"></i> Proceed
                to payment</span>
              </button>
            @else
              <button type="button"  class="btn btn-animate btn-animate-side btn-success">
              <span><i class="icon wb-check" aria-hidden="true"></i> Payed</span>
              </button>
            @endif
            <button type="button" class="btn btn-animate btn-animate-side btn-default btn-outline"
            onclick="javascript:window.print();">
              <span><i class="icon wb-print" aria-hidden="true"></i> Print</span>
            </button>
          </div>
</div>
  @include('beautymail::templates.widgets.articleEnd')




@stop
