<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
Paramètre
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >Date de parametrage</th>
  <th >Logo</th>
  <th >Raison sociale</th>
  <th >Detail</th>
  <th >Adresse</th>
</tr>
@endslot
heuredeb_taf	heurefin_taf	heuredeb_pause	heurefin_pause	raisonsociale	logo	adresse
<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')

  @if(!empty($objects))
    @foreach($objects as $f)
      <tr>
        <td>{{$f->created_at}}</td>
		<td>
			<img  class="card-img-top w-full" src="@if(isset($f) && $f->logo != null ){{route('images',['type'=>'parametres', 'filename'=> $f->logo])}} @else {{ backendasset('photos/placeholder.png') }} @endif" alt="Card image cap" width="50"/>
        </td>
        <td>{{$f->raisonsociale}}</td>
        <td>Horaire: 
		<span class="label label-primary">{{$f->heuredeb_taf}} - {{$f->heuredeb_pause}}</span>
		<span class="label label-primary">{{$f->heurefin_pause}} - {{$f->heurefin_taf}}</span>
		</td>
        <td>{{$f->adresse}}</td>
      </tr>
    @endforeach>
  @endif
@endslot


@endcomponent
