<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-10 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
@if(isset($object) && $object!=null)
  Modification du paramètre
@else
  Mise à jour du paramètre
@endif

@endslot

<!-- mettre le contenu du formulaire ici -->
<div class="row">
  <div class="col-sm-6">
    <div class="row">
		<div class="col-sm-12">
			<label >Raison sociale<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm " name="raisonsociale" value="@if(isset($object) &&  $object!=null){{$object->raisonsociale}}@endif"    type="text"  placeholder="" >
		</div>
		<div class="col-sm-12 allhours invisible">
			<label >Heure de debut de journée<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               <input data-plugin="clockpicker" data-autoclose="true" name="heuredeb_taf" value="@if(isset($object) &&  $object!=null){{$object->heuredeb_taf}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
            </div>
		</div>
		<div class="col-sm-12 allhours invisible">
			<label >Heure de din de journée<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heurefin_taf" value="@if(isset($object) &&  $object!=null){{$object->heurefin_taf}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		<div class="col-sm-12 allhours coffehours invisible">
			<label >Heure de debut de pause<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heuredeb_pause" value="@if(isset($object) &&  $object!=null){{$object->heuredeb_pause}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		<div class="col-sm-12 allhours coffehours invisible">
			<label >Heure de fin de pause<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heurefin_pause" value="@if(isset($object) &&  $object!=null){{$object->heurefin_pause}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		
	</div>
  </div>

  <div class="col-sm-6">

        <label >{{trans('message.logo')}}</label>
        <div class="card " id="img-card">
          <img id="tmpview" class="card-img-top w-full" src="@if(isset($object) && $object->logo != null ){{route('images',['type'=>'employes', 'filename'=> $object->logo])}} @else {{ backendasset('photos/placeholder.png') }} @endif" alt="Card image cap" width="100%"/>
          <img id="preview" style="display:none;" class="card-img-top w-full" src="" alt="Card image cap" width="100%"/>
          <div class="card-block">

            <p class="card-text">
              <input   type="hidden" name="image" id="image"  value="@if(isset($object) ){{$object->logo}}@endif" >
              <input class="form-control input-sm "  type="file" name="logo"   onchange="seeimage(this)" >
            </p>
          </div>
        </div>
  </div>

</div>

<!-- mettre le contenu du formulaire ici -->
@endcomponent
