@extends('../home')

@section('content')
<?php
$view='connexion';
$views=$view.'s';  $objects=$$views;
$monmenu='journalconnexions';
$retour = 'clients';
?>
<!-- Content Header (Page header) -->
@if(App\User::getSession('user.statut')=='superuser') <a value="{{URL::to($retour)}}" class="btn btn-round btn-outline btn-default btn-sm liste_opener"><i class="icon wb-arrow-left margin-right-10" aria-hidden="true"></i>Retour</a>
@endif
<h2>{{trans('menu.'.$monmenu)}} <span class="label label-danger">{{App\User::getFocusedClientName()}}</span></h2>
 <div class="col-md-12">
          <!-- Example Panel With Tool -->
          <div class="panel panel-info ladda-panel">
            <div class="panel-heading ">
              <h3 class="panel-title sm">{{trans('message.journalconnexions')}}</h3>
            </div>
            <div class="panel-body " style="margin: 10px">
            <form action="{{\Illuminate\Support\Facades\URL::to('connexionsdate')}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <table  class="table" >
              <tr>
                  <td>{{trans('message.datedeb')}}  <input type="text" class="form-control has-feedback-left" id="single_cal3" data-inputmask="'mask': '99/99/9999'" required="" placeholder="JJ/MM/AAAA" aria-describedby="inputSuccess2Status2"  name="datedeb">
                  </td>
                  <td>{{trans('message.datefin')}}<input type="text" class="form-control has-feedback-left" id="single_cal2" data-inputmask="'mask': '99/99/9999'" required="" placeholder="JJ/MM/AAAA" aria-describedby="inputSuccess2Status2"  name="datefin">
                  </td>
                  <td align="center" valign="">
                  <input type="submit" class="btn btn-primary  pull-right" value="{{trans('message.rechercher')}}">
                  </td>
              </tr>
              </table>
            </form>
             <table class="table table-hover dataTable width-full " data-plugin="dataTable"  role="grid" >
              <thead>
              <tr role="row">
              <th >{{trans('message.connexion')}}</th>
              <th >{{trans('message.deconnexion')}}</th>
              <th >{{trans('message.utilisateur')}}</th>
              <th >{{trans('message.caisse')}}</th>

               </tr>
            </thead>

              <tbody>
              @foreach($objects as $f)
               <tr role="row" class="odd">
                <td>{{date('d-m-Y H:i:s', strtotime($f->created_at)).\App\User::find(Auth::user()->id)->caisse_id}}</td>
                @if($f->created_at!=$f->updated_at)
                <td> {{date('d-m-Y H:i:s', strtotime($f->updated_at))}} </td>
                @else
                <td> <span class="label label-primary">{{trans('message.ouverte')}}</span>  </td>
                @endif
                <td> @if($f->user_id!=null && $f->user!=null)  {{'@'.$f->user->login}} @else {{trans('message.utilisateurnonauthentifie')}} @endif</td>
                <td>@if($f->caisse_id!=null  && $f->caisse!=null)  {{$f->caisse->code}} @else - @endif</td>


               </tr>
               @endforeach
               </tbody>
             </table>

          </div>
</div>
</div>
 @endsection