@extends('backend.home')

@section('content')
<?php
$view='mouchard';
$views=$view.'s';  $objects=$$views;
$monmenu='mouchards';
$retour = 'clients';
?>
<!-- Content Header (Page header) -->
<div class="col-md-12">
          <!-- Example Panel With Tool -->
          <div class="panel panel-info ladda-panel">
            <div class="panel-heading ">
              <h3 class="panel-title sm">{{trans('message.journalactions')}}</h3>
            </div>
            <div class="panel-body " style="margin: 10px">
            <form action="{{URL::to('mouchardsdate')}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <table  class="table" >
              <tr>
                  <td>{{trans('message.datedeb')}}  <input type="text" class="form-control has-feedback-left" id="single_cal3" data-inputmask="'mask': '99/99/9999'" required="" placeholder="JJ/MM/AAAA" aria-describedby="inputSuccess2Status2"  name="datedeb">
                  </td>
                  <td>{{trans('message.datefin')}}<input type="text" class="form-control has-feedback-left" id="single_cal2" data-inputmask="'mask': '99/99/9999'" required="" placeholder="JJ/MM/AAAA" aria-describedby="inputSuccess2Status2"  name="datefin">
                  </td>
                  <td align="center" valign="">
                  <input type="submit" class="btn btn-primary  pull-right" value="{{trans('message.rechercher')}}">
                  </td>
              </tr>
              </table>
            </form>
             <table class="table table-hover dataTable width-full " data-plugin="dataTable"  role="grid" >
              <thead>
              <tr role="row">
              <th >{{trans('message.dateetheure')}}</th>
              <th >{{trans('message.action')}}</th>
              <th >{{trans('message.fiche')}}</th>
              <th >{{trans('message.utilisateur')}}</th>
              <th >{{trans('message.voirplus')}}</th>

               </tr>
            </thead>

              <tbody>
              @foreach($objects as $f)
               <tr role="row" class="odd">
                <td>{{date('d-m-Y H:i:s', strtotime($f->created_at))}}</td>
                <td>
                @if($f->etat==1) <span class="label label-success">{{trans('message.ajout')}}</span> @endif
                @if($f->etat==2) <span class="label label-warning">{{trans('message.modification')}}</span> @endif
                @if($f->etat==3) <span class="label label-danger">{{trans('message.suppression')}}</span> @endif
                @if($f->etat==4) <span class="label label-default">{{trans('message.annulation')}}</span> @endif
                </td>
                <td>{{$f->modele}}</td>
                <td> @if($f->user_id!=null && $f->user!=null)  {{'@'.$f->user->login}} @else {{trans('message.utilisateurnonauthentifie')}} @endif</td>
                 <td>@if($f->element_id>0)<a href="#" class="fa fa-eye"></a>@endif</td>
               </tr>
               @endforeach
               </tbody>
             </table>

          </div>
</div>
</div>
 @endsection
