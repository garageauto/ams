<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects','info'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-12 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
@if(isset($object) && $object!=null)
  {{trans('message.modificationuser')}}
@else
  {{trans('message.enregistrementuser')}}
@endif

@endslot




<!-- mettre le contenu du formulaire ici -->
<input type="hidden" name="info" value="{{$info}}">
<input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@else{{0}}@endif">
<!-- <input type="hidden" name="pwd" value="@if(isset($object)){{$object->password}}@else{{0}}@endif"> -->

  <div class="row">
    <div class="col-xs-3">
      <label >{{trans('message.login')}} <span style="color: red">*</span></label>
      <input @if(isset($object)) readonly @endif class="form-control input-sm"  name="login"  type="text" placeholder="" value="@if(isset($object)){{old('login',$object->login)}}@endif"/>
    </div>
    @if($info=='M')
      <div class="col-xs-3">
        <label >{{trans('message.ancienmotdepasse')}}  <span style="color: red">*</span></label>
        <input class="form-control input-sm"   name="oldpassword"  type="password" placeholder="" value=""/>
      </div>
    @endif
    <div class="col-xs-3">
      <label >{{trans('message.nouveaumotdepasse')}} <span style="color: red">*</span></label>
      <input class="form-control input-sm"   name="password"  type="password" placeholder="" value=""/>
    </div>

    <div class="col-xs-3">
      <label for="password_confirmation" >{{trans('message.confirmmotdepasse')}}e <span style="color: red">*</span></label>
      <input class="form-control input-sm" id="password_confirmation" name="password_confirmation"  type="password" />
    </div>
    @if($info!='M')
      <div class="col-xs-3">
        <label for="confirm" >{{trans('message.dateactivation')}} <span style="color: red">*</span></label>
        <input id="daten" class="form-control input-sm " name="dateactivation"  type="text"   value="@if(isset($object) && $object->dateactivation!=null){{date('Y-m-d',strtotime($object->dateactivation))}}@else{{date('Y-m-d')}}@endif" placeholder = "yyyy-mm-dd" @if(isset($object) && $object->dateactivation!=null) @else data-provide = "datepicker" @endif data-date-format = "yyyy-mm-dd" data-date-language= "fr" readonly=""  />
      </div>

      <div class="col-xs-3">
        <label >{{trans('message.heure')}} <span style="color: red">*</span></label>
        <input   name="heureactivation" placeholder="H:m:s" type="text"   class="form-control  pull-right timepicker" value="@if(isset($object) && $object->dateactivation!=null) {{date('H:i:s',strtotime($object->dateactivation))}} @else {{date('H:i:s')}} @endif" @if(isset($object) && $object->dateactivation!=null) readonly="" @endif />
      </div>

      <div class="col-xs-3">
        <label for="confirm" >{{trans('message.datedesactivation')}}</label>
        <input id="daten" name="datedesactivation"  type="text"  class="form-control input-sm " value="@if(isset($object) && $object->datedesactivation!=null){{date('Y-m-d',strtotime($object->datedesactivation))}}@endif"  placeholder = "yyyy-mm-dd" data-provide = "datepicker" data-date-format = "yyyy-mm-dd" data-date-language= "fr" readonly=""/>
      </div>

      <div class="col-xs-3">
        <label >{{trans('message.heure')}}</label>
        <input   name="heuredesactivation" placeholder="H:m:s" type="text"  class="form-control  pull-right timepicker" value="@if(isset($object) && $object->datedesactivation!=null){{date('H:i:s',strtotime($object->datedesactivation))}}@endif"  />
      </div>
    @endif
  </div>
  <div align="center" class="box-header with-border" style="background-color: #FFFFFF;margin-top: 10px" >
      <div class="row">
        <div class="col-xs-12 text-uppercase" >
          {{trans('message.utilisateur')}}
        </div>
      </div>
    </div>
    <div align="center" class="box-header with-border" style="background-color: #FFFFFF;margin-top: 10px" >
      <div class="row">
        <div class="col-xs-12 text-uppercase" >
          {{trans('message.profil')}}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-3">
        <label  for="profil">{{trans('message.profil')}} <span style="color: red">*</span></label>
        <select id="profil" class="form-control" required name="profil_id" >
          <option value="">{{trans('message.aucun')}}</option>
          @foreach(\App\Profil::all() as $l)
              <option value="{{$l->id}}" @if(isset($object) && $object->profil->id == $l->id) selected @endif >{{$l->nomprofil}}</option>
          @endforeach
        </select>
      </div>
      @if(!isset($object))
      <div class="col-xs-3">
        <label for="confirm" >{{trans('message.dateattribution')}} <span style="color: red">*</span></label>
        <input id="daten" name="datedebut"  type="text"  class="form-control  pull-right datepicker" value="{{date('Y-m-d')}}" placeholder = "yyyy-mm-dd" data-provide = "datepicker" data-date-format = "yyyy-mm-dd" data-date-language= "fr" readonly="" />
      </div>

      <div class="col-xs-3">
        <label >{{trans('message.heure')}} <span style="color: red">*</span></label>
        <input   name="heuredebut" placeholder="H:m:s" type="text"   class="form-control  pull-right timepicker" value="{{date('H:i:s')}}" />
      </div>

      <div class="col-xs-3">
        <label for="confirm" >{{trans('message.dateretrait')}}</label>
        <input id="daten" name="datefin"  type="text"  class="form-control  pull-right datepicker" value=""  placeholder = "yyyy-mm-dd" data-provide = "datepicker" data-date-format = "yyyy-mm-dd" data-date-language= "fr" readonly=""/>
      </div>

      <div class="col-xs-3">
        <label >{{trans('message.heure')}}</label>
        <input   name="heurefin" placeholder="H:m:s" type="text"  class="form-control  pull-right timepicker" value=""  />
      </div>
      @endif
    </div>

<!-- /.box-body -->
<!-- mettre le contenu du formulaire ici -->
@endcomponent
