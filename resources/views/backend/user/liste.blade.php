<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
{{trans('message.listetypemenu')}}
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >{{trans('message.login')}}</th>
  <th >{{trans('message.profil')}}</th>
  <th >{{trans('message.actif')}}</th>
  <th width="100">{{trans('message.action')}}</th>
</tr>
@endslot

<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')

@foreach($objects as $f)
  <tr>
    <td>{{$f->login}}</td>
    <td> {{$f->currentProfilName()}} </td>
    <td>@include(backendview('includes.checkbutton'))</td>
    <td>
      @if (\App\Menu::droitModification($monmenu))
        <a title="{{trans('message.editer')}}" style="text-decoration: none;" class="btn btn-outline btn-round btn-primary " href="{{url($views.'/'.$f->id.'/edit')}}">
          <i class="icon wb-pencil" aria-hidden="true"></i>{{trans('message.editer')}}
        </a>
      @endif
    </td>
  </tr>
@endforeach
@endslot


@endcomponent
