<span title="@if($f->actif==0){{trans('message.activer')}} @else {{trans('message.desactiver')}} @endif" @if (\App\Menu::droitModification($monmenu))
onclick="supprimer_enregistrement('{{csrf_token()}}',  '{{URL::to($views.'/'.$f->id.'check')}}',  '{{URL::to($views)}}' ,'@if($f->actif==0){{trans('message.voullezvousactiver')}}@else{{trans('message.voullezvousdesactiver')}}@endif' )"  @endif>
    @if($f->actif==0)
    <span class="switchery switchery-small" style="box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255); transition: border 0.4s, box-shadow 0.4s;">
    <small style="left: 0px; transition: background-color 0.4s, left 0.2s;"></small>
    </span>
    @else
    <span class="switchery switchery-small" style="box-shadow: rgb(98, 168, 234) 0px 0px 0px 11px inset; border-color: rgb(98, 168, 234); background-color: rgb(98, 168, 234); transition: border 0.4s, box-shadow 0.4s, background-color 1.2s;">
    <small style="left: 13px; transition: background-color 0.4s, left 0.2s; background-color: rgb(255, 255, 255);"></small>
    </span>
    @endif
</span>