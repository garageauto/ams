@if (\App\Menu::droitSuppression($monmenu))
  <button data-original-title="{{trans('message.restaurer')}}" class="btn btn-sm btn-outline btn-icon btn-primary btn-round" onclick="supprimer_enregistrement('{{csrf_token()}}',  '{{URL::to($monmenu.'/'.$f->id)}}',  '{{URL::to($monmenu)}}')">
    <i class="icon fa fa-circle" aria-hidden="true"></i>
  </button>
@endif
