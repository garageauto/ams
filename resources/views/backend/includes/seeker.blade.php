<?php
if(!isset($view) || $view==null){
  $view=$param;
  $views=$view.'s'; $object=NULL; $objects=NULL;
  $monmenu=$views;
}
?>
@php($ajaxview='backend.'.$view.'.liste')
<div class="col-md-6" id="zonederecherche">
  <div class="panel ">
    <div class="panel-heading">
      <h3 class="panel-title">
        {{trans('message.rechercher')}}
      </h3>

    </div>
    <div class="panel-body" style="padding: 10px">
      <div class="col-md-8">
        <input type="text" id="search_table_val" class="form-control">
      </div>
      <div class="col-md-4">
        <button type="submit" onclick="ecran($('#search_table_val').val(),'search_table_content','{{$ajaxview}}','{{$view}}')" class="btn btn-primary"><i class="fa fa-search"></i>{{trans('message.rechercher')}}</button>
      </div>
    </div>
  </div>
</div>

