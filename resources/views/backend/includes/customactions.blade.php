
@if (\App\Menu::droitModification($monmenu))
       <a data-original-title="{{trans('message.editer')}}" style="text-decoration: none;" class="btn btn-sm btn-primary btn-round" href="{{url($monmenu.'/'.$f->id.'/edit')}}">
           Modifier
       </a>
@endif
@if (\App\Menu::droitSuppression($monmenu))
       <button data-original-title="{{trans('message.supprimer')}}" class="btn btn-sm btn-danger btn-round" onclick="supprimer_enregistrement('{{csrf_token()}}',  '{{URL::to($monmenu.'/'.$f->id)}}',  '{{URL::to($monmenu)}}')">
           Supprimer
       </button>
@endif
