@if(isset($adresses) && count($adresses)>0)
<script src="{{ backendasset('vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ backendasset('vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js') }}"></script>

<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map-canvas'),{
    center  :{
      lat: 6.37029,
      lng: 2.3912361999999803
    },
    zoom: 20

  });

  marker = new google.maps.Marker({
    position : {
      lat:  6.37029,
      lng:  2.3912361999999803
    },
    map: map,
    draggable: true
  });

  var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
  google.maps.event.addListener(searchBox, 'places_changed', function(){
    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i, place;

    for (i=0; place=places[i];i++){
      bounds.extend(place.geometry.location);
      marker.setPosition(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(15);
  });

  google.maps.event.addListener(marker, 'position_changed', function(){
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();
    $('#lat').val(lat);
    $('#lng').val(lng);
  });
</script>

@endif
