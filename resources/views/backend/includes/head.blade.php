<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>{{trans('message.applicationname')}}</title>

  <link rel="apple-touch-icon" href="{{ backendasset('images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ backendasset('images/favicon.ico') }}">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ backendasset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ backendasset('css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ backendasset('css/site.css') }}">


  <link rel="stylesheet" href="{{ backendasset('vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/toastr/toastr.css') }}">

  <!-- Plugin  -->
  <link rel="stylesheet" href="{{ backendasset('vendor/chartist-js/chartist.css') }}">

  <link rel="stylesheet" href="{{ backendasset('vendor/jvectormap/jquery-jvectormap.css') }}">

  <link rel="stylesheet" href="{{ backendasset('vendor/datatables-bootstrap/dataTables.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/datatables-fixedheader/dataTables.fixedHeader.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/datatables-responsive/dataTables.responsive.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/icheck/icheck.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/bootstrap-select/bootstrap-select.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/alertify-js/alertify.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/clockpicker/clockpicker.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/ascolorpicker/asColorPicker.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/jasny-bootstrap/jasny-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/toastr/toastr.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/ladda-bootstrap/ladda.css') }}">
  <link rel="stylesheet" href="{{ backendasset('vendor/card/card.css') }}">



  <!-- Page -->
  <link rel="stylesheet" href="{{ backendasset('css/../fonts/weather-icons/weather-icons.css') }}">

  <link rel="stylesheet" href="{{ backendasset('vendor/select2/select2.css')}}">


  @yield('css')
  <link rel="stylesheet" href="{{ backendasset('css/dashboard/v1.css') }}">
  <link rel="stylesheet" href="{{ backendasset('css/../fonts/font-awesome/font-awesome.css') }}">
  <link rel="stylesheet" href="{{ backendasset('css/../fonts/7-stroke/7-stroke.css') }}">
  <link rel="stylesheet" href="{{ backendasset('css/../fonts/themify/themify.css') }}">




  <!-- Fonts -->
  <link rel="stylesheet" href="{{ backendasset('fonts/web-icons/web-icons.min.css') }}">
  <link rel="stylesheet" href="{{ backendasset('fonts/brand-icons/brand-icons.min.css') }}">
  <link rel='stylesheet' href="{{ backendasset('css/googlefonts.css') }}">


  <!--[if lt IE 9]>
  <script src="{{ backendasset('vendor/html5shiv/html5shiv.min.js') }}"></script>
  <![endif]-->

  <!--[if lt IE 10]>
  <script src="{{ backendasset('vendor/media-match/media.match.min.js') }}"></script>
  <script src="{{ backendasset('vendor/respond/respond.min.js') }}"></script>
  <![endif]-->

  <!-- Scripts -->
  <script src="{{ backendasset('vendor/modernizr/modernizr.js') }}"></script>
  <script src="{{ backendasset('vendor/breakpoints/breakpoints.js') }}"></script>
  <script>

    Breakpoints();
  </script>


  <!-- add -->
  <link href="{{ backendasset('plugins/jconfirm/jquery-confirm.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('plugins/animate/_css/animate.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('plugins/animate/_css/animated-notifications.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('vendor/jquery-wizard/jquery-wizard.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ backendasset('vendor/jquery-labelauty/jquery-labelauty.css') }}" rel="stylesheet" type="text/css" />


@yield('css')



  <!-- Core  -->
  <script src="{{ backendasset('vendor/jquery/jquery.js') }}"></script>
  <script type="text/javascript">
   $(function () {
      $('select:not(.no-select2)').each(function(e) {
        $(this).attr({"data-plugin":"select2"});
        
      });
    });
   
  
  </script>
  <script src="{{ backendasset('vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ backendasset('vendor/animsition/jquery.animsition.js') }}"></script>
  <script src="{{ backendasset('vendor/asscroll/jquery-asScroll.js') }}"></script>
  <script src="{{ backendasset('vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ backendasset('vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
  <script src="{{ backendasset('vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <script src="{{ backendasset('vendor/datatables/jquery.dataTables.min.js') }}"></script>


  <script type="text/javascript" src="{{backendasset('plugins/animate/_scripts/animated-notifications.js')}}"></script>
  <!--Google Map-->
  <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3.exp&region=GB&language=fr-fr&key=AIzaSyAtqWsq5Ai3GYv6dSa6311tZiYKlbYT4mw&libraries=places"></script>

  <!-- geolocalisation -->
  <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3.exp&region=GB&language=fr-fr&key=AIzaSyAtqWsq5Ai3GYv6dSa6311tZiYKlbYT4mw&libraries=places"></script>



  <style>

    @import url(http://fonts.googleapis.com/css?family=Roboto+Mono);
    [data-plugin="formatter"] {
      font-family: 'Roboto Mono', Menlo, Monaco, Consolas, "Courier New", monospace;
    }

    .bootstrap-select {
      width: 100% !important;
    }

    .input-daterange-to {
      float: left;
      width: 40px;
      height: 40px;
      line-height: 40px;
      text-align: center;
    }

    @media (max-width: 1360px) {
      .input-daterange-wrap,
      .input-daterange-to {
        display: block;
        float: none;
      }
    }


    .toast-example {
      position: static;
      margin: 10px 0 30px;
    }

    .toast-example.padding-0 {
      margin-bottom: 30px;
    }

    .toast-example > div {
      width: auto;
      padding-top: 10px;
      padding-bottom: 10px;
      margin-bottom: 0;
    }

    .position-example {
      position: relative;
      height: 330px;
      margin-bottom: 20px;
    }

    .position-example > div {
      position: absolute;
      width: 100%;
      padding: 20px;
    }

    .position-example > .btn-block + .btn-block {
      margin-top: 215px;
    }
  </style>

  <style>
  #loadingPic{
  background: #fff;
  position: fixed;
  top:0px;
  padding: 5px;
  left: 0px;
  width: 100%;
  z-index: 2000;
  }
  </style>

  @include(backendview('includes.upload_script'))

</head>
