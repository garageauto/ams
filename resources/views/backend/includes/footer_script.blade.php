<!-- Plugins -->
<script src="{{ backendasset('vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
<script src="{{ backendasset('vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
<script src="{{ backendasset('vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
<script src="{{ backendasset('vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
<script src="{{ backendasset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

<script src="{{ backendasset('vendor/icheck/icheck.min.js') }}"></script>
<script src="{{ backendasset('vendor/bootstrap-select/bootstrap-select.js') }}"></script>



<script src="{{ backendasset('vendor/switchery/switchery.min.js') }}"></script>
<script src="{{ backendasset('vendor/intro-js/intro.js') }}"></script>
<script src="{{ backendasset('vendor/screenfull/screenfull.js') }}"></script>
<script src="{{ backendasset('vendor/slidepanel/jquery-slidePanel.js') }}"></script>

<script src="{{ backendasset('vendor/skycons/skycons.js') }}"></script>
<script src="{{ backendasset('vendor/aspieprogress/jquery-asPieProgress.min.js') }}"></script>
<script src="{{ backendasset('vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ backendasset('vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js') }}"></script>
<script src="{{ backendasset('vendor/matchheight/jquery.matchHeight-min.js') }}"></script>
<script src="{{ backendasset('vendor/formatter-js/jquery.formatter.js') }}"></script>
<script src="{{ backendasset('vendor/toastr/toastr.js') }}"></script>
<script src="{{ backendasset('vendor/alertify-js/alertify.js') }}"></script>
<script src="{{ backendasset('vendor/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ backendasset('vendor/switchery/switchery.min.js') }}"></script>
<script src="{{ backendasset('vendor/ladda-bootstrap/spin.js') }}"></script>
<script src="{{ backendasset('vendor/ladda-bootstrap/ladda.js') }}"></script>
<script src="{{ backendasset('vendor/card/jquery.card.js') }}"></script>
<script src="{{ backendasset('vendor/select2/select2.min.js') }}"></script>

@yield('js-vendor')

<!-- Scripts -->
<script src="{{ backendasset('js/core.js') }}"></script>
<script src="{{ backendasset('js/site.js') }}"></script>

<script src="{{ backendasset('js/sections/menu.js') }}"></script>
<script src="{{ backendasset('js/sections/menubar.js') }}"></script>
<script src="{{ backendasset('js/sections/sidebar.js') }}"></script>

<script src="{{ backendasset('js/configs/config-colors.js') }}"></script>
<script src="{{ backendasset('js/configs/config-colors.js') }}"></script>
<script src="{{ backendasset('js/configs/config-tour.js') }}"></script>

<script src="{{ backendasset('js/components/asscrollable.js') }}"></script>
<script src="{{ backendasset('js/components/animsition.js') }}"></script>
<script src="{{ backendasset('js/components/slidepanel.js') }}"></script>
<script src="{{ backendasset('js/components/switchery.js') }}"></script>
<script src="{{ backendasset('js/components/matchheight.js') }}"></script>
<script src="{{ backendasset('js/components/jvectormap.js') }}"></script>
<script src="{{ backendasset('js/components/bootstrap-select.js') }}"></script>
<script src="{{ backendasset('js/components/bootstrap-datepicker.js') }}"></script>

<script src="{{ backendasset('js/components/datatables.js') }}"></script>
<script src="{{ backendasset('js/components/panel.js') }}"></script>
<script src="{{ backendasset('js/components/icheck.js') }}"></script>
<script src="{{ backendasset('js/components/formatter-js.js') }}"></script>
<script src="{{ backendasset('js/configs/config-colors.js') }}"></script>
<script src="{{ backendasset('js/configs/config-tour.js') }}"></script>
<script src="{{ backendasset('js/components/toastr.js') }}"></script>
<script src="{{ backendasset('js/components/alertify-js.js') }}"></script>
<script src="{{ backendasset('js/components/clockpicker.js') }}"></script>
<script src="{{ backendasset('js/components/jasny-bootstrap.min.js') }}"></script>
<script src="{{ backendasset('js/components/switchery.js') }}"></script>
<script src="{{ backendasset('vendor/toastr/toastr.js') }}"></script>
<script src="{{ backendasset('js/components/buttons.js') }}"></script>
<script src="{{ backendasset('js/components/ladda-bootstrap.js') }}"></script>
<script src="{{ backendasset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ backendasset('vendor/bootstrap-tokenfield/bootstrap-tokenfield.js') }}"></script>
<script src="{{ backendasset('vendor/jquery-wizard/jquery-wizard.js') }}"></script>
<script src="{{ backendasset('vendor/jquery-wizard/jquery-wizard.min.js') }}"></script>
<script src="{{ backendasset('vendor/jquery-labelauty/jquery-labelauty.js') }}"></script>
<script src="{{ backendasset('js/wizard.min.js') }}"></script>
<script src="{{ backendasset('js/components/card.js') }}"></script>
<script src="{{backendasset('plugins/jconfirm/jquery-confirm.js')}}"></script>

<script src="{{ backendasset('js/components/select2.js') }}"></script>

@yield('js-page')
<!--  -->

<script>
  (function(document, window, $) {
    'use strict';
    
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
</script>

<script>

function afficher_chargement(){
            $('#loadingPic').removeClass('invisible');
      }
      function cacher_chargement(){
            $('#loadingPic').addClass('invisible');
      }

  $('.fermeture_alert').on('click' , function(e)
  {
    e.preventDefault();
    $(this).parent('div').remove();

  });


  $('.liste_opener').on('click', function(e) {
    e.preventDefault();
    list=$(this).attr('value');
    resource=$(this).attr('value');
    if(resource>0)
    {
      setClientSession(resource);
    }
    ouvrir_liste(list);
  });

  $(document).ready(function($) {
    
    Site.run();

    // Open / Close
    $('#exampleToggle').on('click', function() {
      api.toggle();
    });

    $('#exampleOpen').on('click', function() {
      api.open();
    });

    $('#exampleClose').on('click', function() {
      api.close();
    });


// Example inline datepicker
    // ---------------------
    (function() {
      // Reset Current
      $('#inlineDatepicker').datepicker();
      $("#inlineDatepicker").on("changeDate", function(event) {
        $("#inputHiddenInline").val(
          $("#inlineDatepicker").datepicker('getFormattedDate')
        );
      });
    })();

    // Fullscreen
    $('#exampleTogglFullscreene').on('click', function() {
      api.toggleFullscreen();
    });

    $('#exampleEnterFullscreen').on('click', function() {
      api.enterFullscreen();
    });

    $('#exampleLeaveFullscreen').on('click', function() {
      api.leaveFullscreen();
    });


    (function() {
      var snow = new Skycons({
        "color": $.colors("blue-grey", 500)
      });
      snow.set(document.getElementById("widgetSnow"), "snow");
      snow.play();

      var sunny = new Skycons({
        "color": $.colors("blue-grey", 700)
      });
      sunny.set(document.getElementById("widgetSunny"), "clear-day");
      sunny.play();
    })();

  });
  (function(document, window, $) {
    'use strict';

    var Site = window.Site;

    $(document).ready(function($) {
      Site.run();
    });

    // Fixed Header Example
    // --------------------
    (function() {
      // initialize datatable
      var table = $('#exampleFixedHeader').DataTable({
        responsive: true,
        "bPaginate": false,
        "sDom": "t" // just show table, no other controls
      });

      // initialize FixedHeader
      var offsetTop = 0;
      if ($('.site-navbar').length > 0) {
        offsetTop = $('.site-navbar').eq(0).innerHeight();
      }
      var fixedHeader = new FixedHeader(table, {
        offsetTop: offsetTop
      });

      // redraw fixedHeaders as necessary
      $(window).resize(function() {
        fixedHeader._fnUpdateClones(true);
        fixedHeader._fnUpdatePositions();
      });
    })();

    // Individual column searching
    // ---------------------------
    (function() {
      $(document).ready(function() {
        var defaults = $.components.getDefaults("dataTable");

        var options = $.extend(true, {}, defaults, {
          initComplete: function() {
            this.api().columns().every(function() {
              var column = this;
              var select = $(
                '<select class="form-control width-full"><option value=""></option></select>'
              )
                .appendTo($(column.footer()).empty())
                .on('change', function() {
                  var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                  );

                  column
                    .search(val ? '^' + val + '$' : '',
                      true, false)
                    .draw();
                });

              column.data().unique().sort().each(function(
                d, j) {
                select.append('<option value="' + d +
                  '">' + d + '</option>')
              });
            });
          }
        });

        $('#exampleTableSearch').DataTable(options);
      });
    })();

    // Table Tools
    // -----------
    (function() {
      $(document).ready(function() {
        var defaults = $.components.getDefaults("dataTable");

        var options = $.extend(true, {}, defaults, {
          "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [-1]
          }],
          "iDisplayLength": 5,
          "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
          ],
          "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
          "oTableTools": {
            "sSwfPath": "../assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
          }
        });

        $('#exampleTableTools').dataTable(options);
      });
    })();

    // Table Add Row
    // -------------

    (function() {
      $(document).ready(function() {
        var defaults = $.components.getDefaults("dataTable");

        var t = $('#exampleTableAdd').DataTable(defaults);

        $('#exampleTableAddBtn').on('click', function() {
          t.row.add([
            'Adam Doe',
            'New Row',
            'New Row',
            '30',
            '2015/10/15',
            '$20000'
          ]).draw();
        });
      });
    })();





  })(document, window, jQuery);

</script>


<script type="text/javascript">

  //pour formater la monnaie en javascript
  function format(valeur,decimal) {
    // formate un chiffre avec 'decimal' chiffres après la virgule et un separateur
    separateur=" ";
    var deci=Math.round( Math.pow(10,decimal)*(Math.abs(valeur)-Math.floor(Math.abs(valeur)))) ;
    var val=Math.floor(Math.abs(valeur));
    if ((decimal==0)||(deci==Math.pow(10,decimal))) {val=Math.floor(Math.abs(valeur)); deci=0;}
    var val_format=val+"";
    var nb=val_format.length;
    for (var i=1;i<4;i++) {
      if (val>=Math.pow(10,(3*i))) {
        val_format=val_format.substring(0,nb-(3*i))+separateur+val_format.substring(nb-(3*i));
      }
    }
    if (decimal>0) {
      var decim="";
      for (var j=0;j<(decimal-deci.toString().length);j++) {decim+="0";}
      deci=decim+deci.toString();
      val_format=val_format+"."+deci;
    }
    if (parseFloat(valeur)<0) {val_format="-"+val_format;}
    return val_format;
  }

</script>
<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map-canvas'),{
    center  :{
      lat: 6.37029,
      lng: 2.3912361999999803
    },
    zoom: 20

  });

    marker = new google.maps.Marker({
    position : {
      lat:  6.37029,
      lng:  2.3912361999999803
    },
    map: map,
    draggable: true
  });

  var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
  google.maps.event.addListener(searchBox, 'places_changed', function(){
    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i, place;

    for (i=0; place=places[i];i++){
      bounds.extend(place.geometry.location);
      marker.setPosition(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(15);
  });

  google.maps.event.addListener(marker, 'position_changed', function(){
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();
    $('#lat').val(lat);
    $('#lng').val(lng);
  });
</script>
