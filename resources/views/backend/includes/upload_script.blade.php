<!-- styles -->
<link href="{{ backendasset('vendor/fileuploader/jquery.fileuploader.css') }}" media="all" rel="stylesheet">
<link href="{{ backendasset('vendor/fileuploader/jquery.fileuploader-theme-dragdrop.css') }}" media="all" rel="stylesheet">
<link href="{{ backendasset('vendor/fileuploader/jquery.fileuploader-theme-thumbnails.css') }}" media="all" rel="stylesheet">

<!-- js -->
<script src="{{ backendasset('vendor/fileuploader/jquery.fileuploader.min.js') }}" type="text/javascript"></script>
<script src="{{ backendasset('vendor/fileuploader/custom.js') }}" type="text/javascript"></script>

<script type="text/javascript">
//TODO GERER LA TRADUCTION
function loadfile(e)
{
  var html = '<div class="col-sm-4 mediafic fadeIn">'
            +'<span class="fichier" >'
                +'<div class="row" >'
                  +'<div class="col-sm-4">'
                    +'<div class="icon-block" id="images" style="height:75px; width:75px;">'
                        +'<i class="material-icons text-muted-light md-36">photo</i>'
                    +'</div>'
                    +'<img class="w-120" src="" alt="" style="display:none;" width="100%" id="changeImage" >'
                  +'</div>'
                  +'<div class="col-sm-8 mediafic-body">'

                  +'</div>'
                  +'<div style="position: relative">'
                          +'<button type="button" onclick="javascript:dismiss(this)" class="btn btn-danger btn-xs pull-right">x</button>'
                        +'</div>'
                +'</div>'

            +'</span>'

        +'</div>';

  var el = $('#joinfiles').append(html);
  var input = $(e).clone().attr({'name': 'imageupload[]'});
  var images = $('#joinfiles .mediafic').last().find('#images');
  var changeImage = $('#joinfiles .mediafic').last().find('#changeImage');

  var f = e.files[0];

         var mot = f.name
            if (f.type.match('image.*')) {

                var reader  = new FileReader();

                 reader.addEventListener("load", function () {
                   $(changeImage).attr('src',reader.result) ;
                 }, false);

                 
                reader.readAsDataURL(f);
                 

                 var checkable = '<div id="checkable"><input name="pieceid[]" type="hidden" value="0"  />'
            +'<input name="telechargeable[]" type="hidden" value="off"></div>';
                $('#joinfiles .mediafic').last().find('.mediafic-body').append(mot).append(checkable);
                $(images).css({'display':'none'});
                $(changeImage).css({'display':'block'});

            }else{
               $(images).css({'display':'none'});
                $(changeImage).css({'display':'none'});

                var checkable = '<div id="checkable"><input name="pieceid[]" type="hidden" value="0"><input name="telechargeable[]" type="checkbox" checked="">'
            +'<label style="font-size: 11px">Téléchargeable</label><div >';
                 $('#joinfiles .mediafic').last().find('.mediafic-body').append(mot).append(checkable);
            }
   $('#joinfiles .mediafic').last().find('.mediafic-body').append(input);
}

function seeimage(e)
{
  //imageinput 
  var imageinput = $(e).closest('#img-card').find('[id="image"]');
  var images = $(e).closest('#img-card').find('#tmpview');
  var changeImage = $(e).closest('#img-card').find('#preview');

  var f = e.files[0];

    if (f.type.match('image.*')) {
        //name="telechargeable[]" type="hidden" value="off"
        var checkable = '<input name="telechargeable[]" type="hidden" value="off">';
        $(e).closest('#img-card').find('#checkable ').html(checkable);
        var reader  = new FileReader();

         reader.addEventListener("load", function () {
           $(changeImage).attr('src',reader.result) ;
         }, false);

        reader.readAsDataURL(f);
       
        $(imageinput).val(f.name);
     
        $(images).css({'display':'none'});
        $(changeImage).css({'display':'block'});

    }else{
        var checkable = '<input name="telechargeable[]" type="checkbox" checked="">'
            +'<label style="font-size: 11px">Téléchargeable</label>';

        $(e).closest('#img-card').find('#checkable ').html(checkable);

        $(imageinput).val(f.name);
        $(images).css({'display':'none'});
        $(changeImage).css({'display':'block'})
        .attr('src','{{ backendasset('photos/placeholder.png') }}');
    }
}

function see_image(e)
{
  //imageinput 
  var imageinput = $(e).closest('#img-card').find('[id="image"]');
  var images = $(e).closest('#img-card').find('#tmpview');
  var changeImage = $(e).closest('#img-card').find('#preview');
  var checkable = $(e).closest('#img-card').find('#checkable');

  var f = e.files[0];

    if (f.type.match('image.*')) {
         $(checkable).find('[type="checkbox"]').attr({'value':'off'});
        var reader  = new FileReader();

         reader.addEventListener("load", function () {
           $(changeImage).attr('src',reader.result) ;
         }, false);

        reader.readAsDataURL(f);
       
        $(imageinput).val(f.name);
     
        $(images).css({'display':'none'});
        $(checkable).css({'display':'none'});
        $(changeImage).css({'display':'block'});

    }else{
        $(checkable).find('[type="hidden"]').attr({'type':"checkbox"});
        $(checkable).find('[type="checkbox"]').removeAttr("value");
        $(imageinput).val(f.name);
        $(images).css({'display':'none'});
        $(checkable).css({'display':'block'});
        $(changeImage).css({'display':'block'})
        .attr('src','{{ backendasset('photos/placeholder.png') }}');
    }
}

function dismiss(e){

  $(e).closest('.mediafic').fadeOut("normal", function() {
    $(this).remove();
  });



}

//permet l'affichage des villes en fonction du pays choisi
function getville(e){
  var d = $(e).val();
  
  if($(e).val() == ""){
    $(e).closest('.panel-body').find('#ville_id').children().addClass("hide");
    $(e).closest('.panel-body').find('#ville_id #vide').removeClass("hide");
    
  }else{
    $(e).closest('.panel-body').find('#ville_id').children().addClass("hide");
    $(e).closest('.panel-body').find('#ville_id [data-pays="#'+d+'"]').removeClass("hide");
    $(e).closest('.panel-body').find('#ville_id #vide').removeClass("hide");
    
  }


}


//fait un collapse de la liste des pays lorsqu'on veux en selectionner
function list_pays_check(e){
  if($(e).prop('checked') == true){
   
    $('#list_pays').collapse('hide');
        $("#list_pays [type='checkbox']").prop('checked', false);
        
  }else{
    
    $('#list_pays').collapse('show');
  }

}




</script>
