<ul class="site-menu " >
	<li class="site-menu-category">
       <a class="" href="{{url('/')}}" >
          <b>Acceder Frontend</b>
       </a>
     </li>
  <?php $menus=\App\Menu::principaux(); ?>
  @foreach ($menus as $menu)
    @if (\App\Menu::droitLecture($menu->urlmenu))
      <li class="site-menu-category">
          <a  @if ($menu->urlmenu!='') href="{{url($menu->urlmenu)}}"  @endif title="{{$menu->fr}}" >
          {{$menu->fr}}
          </a>
      </li>
      <?php $mesmenus=$menu->sousmenus; ?>
      @foreach ($mesmenus as $ssmenu)
        @if (\App\Menu::droitLecture($ssmenu->urlmenu))

          <li class="site-menu-item @if(Session::has('liste') && in_array(\App\Redempteur::getUrl(Session::get('liste')),$ssmenu->sousmenus->lists('urlmenu'))) active @endif  <?php  if(count($ssmenu->sousmenus)) echo "has-sub" ; ?> " >
            <a  @if ($ssmenu->urlmenu!='') href="{{url($ssmenu->urlmenu)}}"  @endif title="{{$ssmenu->fr}}" >
              <i class="fa {{$ssmenu->iconemenu}} "></i>
              <span>{{$ssmenu->fr}}</span>
              @if (count($ssmenu->sousmenus))<span class="site-menu-arrow"></span> @endif
            </a>
            @if (count($ssmenu->sousmenus))
              <ul class="site-menu-sub">
                <?php $mesmenus2=\App\Menu::listeDesSousMenusPrincipaux($ssmenu->id); ?>
                @foreach ($mesmenus2 as $ssmenu2)
                  @if (\App\Menu::droitLecture($ssmenu2->urlmenu))
                    <li class="site-menu-item"  >
                      <a @if($ssmenu2->urlmenu!='') href="{{url($ssmenu2->urlmenu)}}"  @endif data-slug="advanced-maps-google">
                        <i class="site-menu-icon " aria-hidden="true"></i>
                        <span class="site-menu-title">{{$ssmenu2->fr}}</span>
                      </a>
                    </li>
                  @endif
                @endforeach
              </ul>
            @endif
          </li>
        @endif
      @endforeach
    @endif
  @endforeach
</ul>
<style>
  .after-run .site-menubar-unfold .site-menu > .site-menu-item > a {
    line-height: 30px;
  }
</style>
