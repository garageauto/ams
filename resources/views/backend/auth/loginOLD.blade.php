<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Login | {{trans('message.applicationname')}}</title>

  <link rel="apple-touch-icon" href="../resources/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../resources/assets/images/favicon.ico">

  <!-- Stylesheets -->

  <link rel="stylesheet" href="../resources/assets/css/bootstrap.min.css">


  <link rel="stylesheet" href="../resources/assets/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="../resources/assets/css/site.min.css">


  <link rel="stylesheet" href="../resources/assets/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../resources/assets/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../resources/assets/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../resources/assets/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../resources/assets/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../resources/assets/vendor/flag-icon-css/flag-icon.css">


  <!-- Page -->
  <link rel="stylesheet" href="../resources/assets/css/pages/login.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="../resources/assets/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="../resources/assets/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


  <!--[if lt IE 9]>
    <script src="../resources/assets/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="../resources/assets/vendor/media-match/media.match.min.js"></script>
    <script src="../resources/assets/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="../resources/assets/vendor/modernizr/modernizr.js"></script>
  <script src="../resources/assets/vendor/breakpoints/breakpoints.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="bg-blue-700 layout-full">



  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <div class="brand">
          <h2 class="brand-text"><b>{{trans('message.applicationname')}}</b></h2>
      </div>




      <form method="POST" action="{{ url('/auth/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        

        @if($errors->first())
        <div class="alert alert-danger alert-dismissible bg-red-500" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="color:white;">×</span>
          </button>
        
          <p style="color:white;">{{ $errors->first() }}</p>
 
        </div>
        @endif
        
          <div class="form-group row">
              <div class="col-md-12">
              <input type="username" class="form-control" id="inputUsername" name="username" placeholder="{{trans('message.nomdutilisateur')}}" autocomplete="off">
              </div>
            </div>
          <div class="form-group row">
              <div class="col-md-12">
                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="{{trans('message.motdepasse')}}"  autocomplete="off">
              </div>
            </div>



        <button type="submit" class="btn btn-primary btn-block">{{trans('message.connexion')}}</button>
        <a href="{{url('cpanel')}}" class="btn btn-default btn-block">{{trans('message.creationdecompte')}}</a>


      </form>


    </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->
  <script src="../resources/assets/vendor/jquery/jquery.js"></script>
  <script src="../resources/assets/vendor/bootstrap/bootstrap.js"></script>
  <script src="../resources/assets/vendor/animsition/jquery.animsition.js"></script>
  <script src="../resources/assets/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="../resources/assets/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../resources/assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src= "../resources/assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <!-- Plugins -->
  <script src="../resources/assets/vendor/switchery/switchery.min.js"></script>
  <script src="../resources/assets/vendor/intro-js/intro.js"></script>
  <script src="../resources/assets/vendor/screenfull/screenfull.js"></script>
  <script src="../resources/assets/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="../resources/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

  <!-- Scripts -->
  <script src="../resources/assets/js/core.js"></script>
  <script src="../resources/assets/js/site.js"></script>

  <script src="../resources/assets/js/sections/menu.js"></script>
  <script src="../resources/assets/js/sections/menubar.js"></script>
  <script src="../resources/assets/js/sections/sidebar.js"></script>

  <script src="../resources/assets/js/configs/config-colors.js"></script>
  <script src="../resources/assets/js/configs/config-tour.js"></script>

  <script src="../resources/assets/js/components/asscrollable.js"></script>
  <script src="../resources/assets/js/components/animsition.js"></script>
  <script src="../resources/assets/js/components/slidepanel.js"></script>
  <script src="../resources/assets/js/components/switchery.js"></script>
  <script src="../resources/assets/js/components/jquery-placeholder.js"></script>

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

</body>

</html>