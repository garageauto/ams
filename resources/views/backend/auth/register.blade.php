
@extends('backend.home')

@section('content')


  <style>
    .invisible{
      display:none;
    }
  </style>
<a href="{{url('auth/login')}}"> <- Accéder à la page de connexion</a>
<form style="background: #ffffff; margin-top:50px" class="form formulairemodal" resource="{{url('/home')}}"  id="x-tmp" method="POST" action="{{ url('register') }}" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="MAX_FILE_SIZE" value="50971520"  />
  <input type="hidden" name="id" value="@if(isset($client) &&  $client!=null){{$client->id}}@else{{'0'}}@endif" >

  <div style="padding: 10px">


    <div class="box box-primary">
      <div class="box-header with-border" >
        <div class="row">
          <div class="col-sm-12" style="padding: 20px">
            Enregistrement
          </div>
        </div>
      </div>


      <div class="box-body" style="margin: 10px">

        <div class="row">

          <div class="col-sm-3">
            <label >Selectionnez le profil qui vous correspond<sup>*</sup></label>
            <?php $profils=\App\Profil::all(); ?>
            <select class="form-control input-sm " required name="profil_id" id="profil_id" >
              <option id="default_country"></option>
              @foreach($profils as $profil)
                <option value="{{$profil->id}}">{{$profil->nomprofil}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-3 invisible" id="clientinfo2">
            <label >Selectionnez votre entreprise <sup>*</sup></label>
              <?php $clients=\App\Park::all(); ?>
            <select class="form-control input-sm form2"   name="park_id"  >
              <option >--selectionner--</option>
              @foreach($clients as $client)
                <option value="{{$client->id}}">@if($client->annonceur_id!=null && $client->annonceur!=null) {{$client->annonceur->raisonsociale}} @endif  - {{  $client->libelle }}  </option>
              @endforeach
            </select>
          </div>

          <div class="col-sm-12 invisible" id="clientinfo" >




            <div class="row">


              <span id="physique" class="hidden">
                     <div class="col-sm-3">
                         <label >{{trans('message.nom')}}<sup>*</sup></label>
                         <input class="form-control input-sm login_generator"  value="@if(isset($client) &&  $client!=null){{$client->nom}}@else{{\Illuminate\Support\Facades\Input::old('nom')}}@endif"  name="nom"  type="text" placeholder="" >
                     </div>

                     <div class="col-sm-3">
                         <label >{{trans('message.prenom')}}<sup>*</sup></label>
                         <input class="form-control input-sm " value="@if(isset($client) &&  $client!=null){{$client->prenom}}@else{{\Illuminate\Support\Facades\Input::old('prenom')}}@endif"  name="prenom"  type="text" placeholder="" >
                     </div>

                     <div class="col-sm-3">
                          <label >{{trans('message.sexe')}}<sup>*</sup></label>
                          <select class="form-control input-sm " name="sexe" >
                           <option @if((isset($client) &&  $client!=null && $client->sexe=="M") ) selected @endif value="M">{{trans('message.masculin')}}</option>
                           <option @if((isset($client) &&  $client!=null && $client->sexe=="F") ) selected @endif value="F">{{trans('message.feminin')}}</option>
                          </select>
                     </div>
                     </span>
              <span id="morale" >
                     <div class="col-sm-3">
                         <label >{{trans('message.raisonsociale')}}<sup>*</sup></label>
                         <input class="form-control input-sm login_generator"   value="@if(isset($client) &&  $client!=null){{$client->raisonsocial}}@else{{\Illuminate\Support\Facades\Input::old('raisonsocial')}}@endif" name="raisonsociale"  type="text" placeholder="" >
                     </div>
                     </span>


            </div>
            <div class="row">

              <div class="col-sm-3">
                <label >{{trans('message.email')}}</label>
                <input class="form-control input-sm "  name="mail" value="@if(isset($client) &&  $client!=null){{$client->mail}}@else{{\Illuminate\Support\Facades\Input::old('mail')}}@endif"  type="email" placeholder="" >
              </div>

              <div class="col-sm-3">
                <label >{{trans('message.pays')}}<sup>*</sup></label>
                  <?php $pays=\App\Pay::all(); ?>
                <select class="form-control input-sm "  name="pay_id" onchange="$('#indicatif_pay').html('+'+$(this).val());" >
                  <option id="default_country" value="0"></option>
                  @foreach($pays as $pay)
                    <option value="{{$pay->indicatifpay}}">{{$pay->nom_frpay}} (+{{$pay->indicatifpay}})</option>
                  @endforeach
                </select>
              </div>

              <div class="col-sm-3">
                <label >{{trans('message.numtelephone')}}<sup>*</sup></label>
                <div class="input-group">
                  <span class="input-group-addon" id="indicatif_pay">___</span>
                  <input class="form-control input-sm phone" name="tel"   value="@if(isset($client) &&  $client!=null){{$client->tel}}@else{{\Illuminate\Support\Facades\Input::old('tel')}}@endif"  type="text" placeholder="Ex: 99 99 99 99" >
                </div>
              </div>


            </div>
            <div class="row">
              <div class="col-sm-12">
                <label >{{trans('message.adresse')}}</label>
                <textarea class="form-control input-sm" name="adresse" placeholder="{{trans('message.saisiradresseici')}}">@if(isset($client) &&  $client!=null){{$client->adresse}}@else{{\Illuminate\Support\Facades\Input::old('adresse')}}@endif</textarea>
              </div>
            </div>



          </div><!-- /.box-body -->
          @if(!isset($object) || $object==null)
            <div class="">

              <h4 class="col-md-12">{{trans('message.authentification')}}</h4>

              <div class="col-sm-3">
                <label >{{trans('message.login')}}<sup>*</sup></label>
                <input id="client_login"  class="form-control input-sm lowercase "  value="@if(isset($client) &&  $client!=null){{$client->login}}@else{{\Illuminate\Support\Facades\Input::old('login')}}@endif" required="required" name="login" type="text" placeholder="" >
              </div>

              <div class="col-sm-3">
                <label >{{trans('message.motdepasse')}}<sup>*</sup></label>
                <input class="form-control input-sm confirm"  required="required"  name="password" id="password"  type="password" placeholder="">
              </div>


              <div class="col-sm-3">
                <label >{{trans('message.confirmmotdepasse')}}<sup>*</sup></label>
                <input class="form-control input-sm confirm"  required="required" name="confirm" id="confirm"  type="password" placeholder="">
              </div>
              <div class="col-sm-3">
                <label >afficher le mot de passe</label><br/>
                <bouton class="btn btn-primary btn-sm confirm-shower"  ><i class="fa fa-eye"></i>afficher le mot de passe</bouton>
              </div>
            </div>
          @endif
<br/>
<br/>
          <div class="col-sm-12">
            <br/>
            <br/>
            <input name="cgu" required value="0" type="checkbox"> J'accepte les conditions générales de l’application
          </div>

        </div>




      </div>


      <div class="box-footer" style="padding: 20px;" >
        <div id="box_resultRequest" class="alert invisible alert-dismissible " role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="color:white;">×</span>
          </button>
          <p style="color:white;" id="resultRequest"></p>
        </div>
        <div class="row" style=" ">
          <div class="col-xs-6">
            <input type="reset" class="btn btn-flat  btn-default" id="reseter" value="{{trans('message.recommencer')}}"/>
          </div>

          <div class="col-xs-6">
            <input type="submit" class="btn btn-flat btn-primary pull-right" value="{{trans('message.valider')}}" />
          </div>
        </div>
      </div>

    </div>

  </div>

</form>






<script>
  $('.lowercase').css("text-transform", "lowercase")
  $('#client_login').change(  function(){
    $('#login_caisse').val('c'+$(this).val())
  });

  $('#profil_id').change(  function(){
      if($(this).val()==1){
          if($('#clientinfo').hasClass('invisible')) $('#clientinfo').removeClass('invisible');
          if(!$('#clientinfo2').hasClass('invisible')) $('#clientinfo2').addClass('invisible');
      }
      else {
          if($('#clientinfo2').hasClass('invisible')) $('#clientinfo2').removeClass('invisible');
          if(!$('#clientinfo').hasClass('invisible'))$('#clientinfo').addClass('invisible');
      }

  });




  $('.confirm').keyup(function () {
    if($('#password').val()!=$('#confirm').val())
    {
      $('.confirm').css("border", "thin solid #ff9999");
    }
    else {
      $('.confirm').css("border", "thin solid #009999");
    }
  })


  var confirmshowed=1;
  var confirmshowed_caisse=1;

  $('.confirm-shower').click(function () {
    if(confirmshowed==0){
      $('.confirm').attr('type','password');confirmshowed=1;
    }else{
      $('.confirm').attr('type','text');confirmshowed=0;
    }
  }) ;



</script>



@endsection
