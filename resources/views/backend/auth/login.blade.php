<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login | {{trans('message.applicationname')}}</title>

  <style>
      a{
          margin-top: 20px;
          margin-bottom: 20px;
          color: #FFFFFF;
          float: right;
      }
    .form-control{
      padding-bottom: 10px;
      padding-top: 10px;
      background: #ffffff;
      border: thin solid #eee8d5;
      color: #0a001f;
      width: 100%;
    }
    .page-content{
      background: #333333;
      padding: 50px;
      width: 20%;
      margin: auto;
    }
    .btn-primary
    {
      border: none;
      background: #ff4444;
      color: #ffffff;
      text-align: center;
      padding: 10px;
      width: 100%;
    }
    .page-login
    {
      background: #fdfdfd;
    }
  </style>

</head>
<body class="page-login">

<br/>
<br/>
<br/>
<br/>
<br/>
  <!-- Page -->
    <div class="page-content">
      <div class="brand">

        <img src="{{backendasset('logo.png')}}" alt="{{trans('message.logo')}}" style="margin:10%" width="80%">
        <br/>

      </div>

      <form method="POST" action="{{ route('p_login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">


        @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible bg-red-500" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="color:white;">×</span>
          </button>

          <p style="color:#000" > {{session('error') }}</p>

        </div>
        @endif



          <div class="form-group row" style="margin-bottom: 15px">
              <div class="col-md-12">
              <input type="username" class="form-control" id="inputUsername" name="login" placeholder="{{trans('message.nomdutilisateur')}}" autocomplete="off">
              </div>
            </div>

          <div class="form-group row">
              <div class="col-md-12">
                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="{{trans('message.motdepasse')}}"  autocomplete="off">
              </div>
            </div>

        <br/>

        <button type="submit" class="btn-primary ">{{trans('message.connexion')}}</button>

          <a href="{{url('/')}}"  title="cliquer sur ce lien pour créer un compte ">Control des presences</a>

      </form>

    </div>

  <!-- End Page -->


</body>

</html>
