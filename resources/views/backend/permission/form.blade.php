<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-12 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
Edition des permissions

@endslot



<!-- mettre le contenu du formulaire ici -->
<div class="row">

  <div class="col-sm-12">
    <label >Employé concerné</label>
			<table class="table table-hover  width-full " >
              <thead>
              <tr role="row">
                <th >Nom et prénoms</th>
                <th >Téléphone</th>
                <th >Choisir</th>
              </tr>
              </thead>
              <tbody>
              <?php $employes=\App\Employe::all(); ?>
              @if(!empty($employes))
                @foreach($employes as $f)
                  <tr style="cursor:pointer" onclick="$('#employe_{{$f->id}}').click()">
                    <td>{{$f->nom}} {{$f->prenom}}</td>
                    <td>{{$f->tel}}</td>
                    <td>
						<input name="employe_id" id="employe_{{$f->id}}" value="{{$f->id}}"  type="radio"  @if((isset($object) and $object->employe_id==$f->id) ) checked  @endif>
						<label >{{trans('message.choisir')}}</label>
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
  </div>
  <div class="col-sm-6">
    <label >Date de début</label>
    <input class="form-control input-sm " value="@if(isset($object) &&  $object!=null){{$object->datedeb}}@endif"   name="datedeb" type="date"  placeholder="" >
  </div>

  <div class="col-sm-6">
    <label >Date de fin</label>
    <input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->datefin}}@endif"   name="datefin" type="date"  placeholder="" >
  </div>
  
  <div class="col-sm-12">
	<label >Motif</label>
	<textarea class="form-control input-sm " name="motif" placeholder="Saisir le motif de la permission" >@if(isset($object) &&  $object!=null){{$object->motif}}@endif</textarea>
  </div>

</div>
<!-- mettre le contenu du formulaire ici -->
@endcomponent
