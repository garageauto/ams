@if (\App\Menu::droitAjout($monmenu) || (\App\Menu::droitModification($monmenu) && isset($object) &&  $object!=null))

  <div class="col-md-8">
    <div class="panel ">
      <div class="panel-heading">
        <h3 class="panel-title">
          @if(isset($object) && $object!=null)
            {{trans('message.modificationmenu')}}
          @else
            {{trans('message.enregistrementmenu')}}
          @endif
        </h3>

      </div>

      <div class="panel-body" style="padding: 10px">

        <!-- formulaire -->

          <div class="box-body" >
            <div class="">
                <div class="">
                  <div>
                    <form class="form formulairemodal "  method="POST" action="{{ url($views) }}" resource="{{URL::to($views)}}" enctype="multipart/form-data" >
                      <?php $objects=\App\Menu::all();  ?>
                      @include(backendview('includes.basic_hidden_button'))
                      <div class="row" >
                        <div class="col-sm-6">
                          <label >{{trans('message.code')}}</label>
                          <input required class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->nommenu}}@endif"   name="nommenu" type="text"  placeholder="" >
                        </div>
                        <div class="col-sm-6">
                          <label >{{trans('message.menuparent')}}</label>
                          <select name="menu_id"  class="form-control input-sm " id="menu_id" >
                            <option value>{{trans('message.optionvide')}}</option>
                            @foreach($objects as $t)
                              <option @if((isset($object) && $object!=null && $object->menu_id==$t->id) ) selected @endif  value="{{$t->id}}">@if($t->menu_id!=null && $t->menu!=null){{$t->menu->getName().' -> '}}@endif{{$t->getName()}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                        <div class="row" >
                          <div class="col-sm-12">
                            <label >{{trans('message.libelle')}}</label>
                            <input required class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->fr}}@endif"   name="fr" type="text"  placeholder="" >
                          </div>

                        </div>
                      <div class="row" >
                        <div class="col-sm-6">
                          <label >{{trans('message.codeicone')}}</label>
                          <input class="form-control input-sm " value="@if(isset($object) &&  $object!=null){{$object->iconemenu}}@endif"   name="iconemenu" type="text"  placeholder="" >
                        </div>

                        <div class="col-sm-6">
                          <label >{{trans('message.lienacces')}}</label>
                          <input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->urlmenu}}@endif"   name="urlmenu" type="textmenu"  placeholder="" >
                        </div>
                      </div>

                        <hr/>
                      @include(backendview('includes.registerbutton'))
                    </form>
                  </div>

              </div>

              </div>
            </div>
          </div><!-- /.box-body -->


      </div>
    </div>
  </div>

@endif
