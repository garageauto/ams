<div class="col-md-12">
  <div class="panel panel-info ladda-panel">
    <div class="panel-heading ">
      <h3 class="panel-title sm">{{trans('message.listemenu')}}</h3>
    </div>
    <div class="panel-body " style="margin: 10px">
      <!-- Example Tabs Line -->
        <div class="">


          <div class="">

              <div >
                  <table class="table table-hover " >
                  <thead>
                  <tr role="row">
                    <th >{{trans('message.id')}}</th>
                    <th >{{trans('message.iconemenu')}}</th>
                    <th width="300">{{trans('message.code')}}</th>
                    <th >{{trans('message.designation')}}</th>
                    <th >{{trans('message.lienacces')}}</th>
                    <th >{{trans('message.menuparent')}}</th>
                    <th width="90">{{trans('message.action')}}</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($objects as $f)
                    <tr>
                      <td>{{$f->id}}</td>
                      <td>@if(isNonEmptyString($f->iconememnu))<i class="{{$f->iconememnu}}"></i>@endif
                      </td>
                      <td>
                        {{$f->getName()}}
                      </td>
                      <td>
                        <span class="badge badge-success">{{$f->fr}}</span>
                      </td>
                      <td>{{$f->urlmenu}}</td>
                      <td>@if($f->menu_id!=null && $f->menu!=null)<span class="badge badge-success" >{{$f->menu->getName()}}</span>@endif</td>
                      <td>@include('backend.includes.customactions')</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      <!-- End Example Tabs Line -->
    </div>
  </div>
</div>

