@extends('backend.home')

@section('content')
<?php
$retour = 'clients';
?>
<!-- Content Header (Page header) -->
<div class="col-md-12">
          <!-- Example Panel With Tool -->
          <div class="panel panel-info ladda-panel">
            <div class="panel-heading ">
              <h3 class="panel-title sm">ETAT DES PRESENCES</h3>
            </div>
            <div class="panel-body " style="margin: 10px">
            <form action="{{URL::to('etatpresence')}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <table  class="table" >
              <tr>
                  <td>{{trans('message.datedeb')}}  <input type="date" class="form-control has-feedback-left" id="single_cal3" required="" name="datedeb">
                  </td>
                  <td>{{trans('message.datefin')}}<input type="date" class="form-control has-feedback-left" id="single_cal2"     required=""  name="datefin">
                  </td>
                  <td align="center" valign="">
                  <input type="submit" class="btn btn-primary  pull-right" value="{{trans('message.rechercher')}}">
                  </td>
              </tr>
              </table>
            </form>
			@if( $nbrheures!=0 )
			<h3>Nombre d'heure règlementaire prévu du {{$datedeb}} au {{$datefin}}:</h3> <h2>{{$nbrheures}} Heure(s) </h2>
             <table class="table table-hover dataTable width-full " data-plugin="dataTable"  role="grid" >
              <thead>
              <tr role="row">
              <th >Collaborateur</th>
              <th >Nbr. Heures Total Effectué</th>
              <th >Nbr. Jours d'absence</th>

               </tr>
            </thead>

              <tbody>
              @foreach($employes as $f)
               <tr role="row" class="odd">
                <th >{{$f->nom}} {{$f->prenom}}</th>
				<th >{{\App\Presence::getNbrHeures($f->id,$datedeb,$datefin)}}</th>
				<th >{{\App\Presence::getNbrHeuresAbsence($f->id,$datedeb,$datefin)}}</th>
               </tr>
               @endforeach
               </tbody>
             </table>
			@endif
          </div>
</div>
</div>
 @endsection
