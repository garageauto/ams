@extends('backend.home')

@section('content')
<?php
$retour = 'clients';
?>
<!-- Content Header (Page header) -->
<div class="col-md-12">
          <!-- Example Panel With Tool -->
          <div class="panel panel-info ladda-panel">
            <div class="panel-heading ">
              <h3 class="panel-title sm">JOURNAL DES PRESENCES</h3>
            </div>
            <div class="panel-body " style="margin: 10px">
            <form action="{{URL::to('journalentrees')}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <table  class="table" >
              <tr>
                  <td>Date de reférence  <input type="date" class="form-control has-feedback-left" id="single_cal3" required="" name="datedeb">
                  </td>
                
                  <td align="center" valign="">
                  <input type="submit" class="btn btn-primary  pull-right" value="{{trans('message.rechercher')}}">
                  </td>
              </tr>
              </table>
            </form>
			@if($datedeb!='' )
			<h3>Journal des présences du {{$datedeb}} </h3> 
             <table class="table table-hover dataTable width-full " data-plugin="dataTable"  role="grid" >
              <thead>
              <tr role="row">
              <th >Collaborateur</th>
              <th >Heure d'entrée</th>
              <th >Heure de sortie</th>

               </tr>
            </thead>

              <tbody>
              @foreach($presences as $f)
               <tr role="row" class="odd">
                <th >@if($f->employe_id!=null && $f->employe){{$f->employe->nom}} {{$f->employe->prenom}}@endif</th>
				<th >{{$f->heure_deb}}</th>
				<th >{{$f->heure_fin}}</th>
               </tr>
               @endforeach
               </tbody>
             </table>
			@endif
          </div>
</div>
</div>
 @endsection
