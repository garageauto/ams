<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
Liste des terminaux pouvant acceder au frontend de la plateforme
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >{{trans('message.id')}}</th>
  <th >IP</th>
  <th >MAC</th>
  <th >IMEI</th>
  <th width="300">{{trans('message.action')}}</th>
</tr>
@endslot

<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')

  @if(!empty($objects))
    @foreach($objects as $f)
      <tr>
        <td>{{$f->id}}</td>
        <td>{{$f->ip}}</td>
        <td>{{$f->mac}}</td>
        <td>{{$f->imei}}</td>
        <td>@include('backend.includes.customactions')</td>
      </tr>
    @endforeach
  @else
    <tr>
        <td></td>
        <td>{{trans('message.Aucun résultat')}}</td>
        <td></td>
        <td></td>
        
      </tr>
  @endif
@endslot

@endcomponent
