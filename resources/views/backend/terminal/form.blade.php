<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-6 @endslot
@slot('title')
<!-- 
mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
@if(isset($object) && $object!=null)
  Ajout des terminaux
@else
  Modification des terminaux
@endif
@endslot

<!-- mettre le contenu du formulaire ici -->
<div class="row">

  <div class="col-sm-6">
    <label>AdresseIP</label>
    <input class="form-control input-sm " value="@if(isset($object) &&  $object!=null){{$object->ip}}@endif" name="ip" type="text"  placeholder="" >
  </div>

  <div class="col-sm-12">
    <label >Adresse MAC</label>
    <input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->mac}}@endif" name="mac" type="text"  placeholder="" >
  </div>
  <div class="col-sm-12">
    <label >IMEI</label>
    <input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->imei}}@endif" name="imei" type="text"  placeholder="" >
  </div>

</div>
<!-- mettre le contenu du formulaire ici -->
@endcomponent
