@extends('backend.home')
<!-- ceci est une estension du template home qui inclut en titre le nom du menu sur lequel on est
positionne et le formulaire et la liste de la vue sur laquelle on est

-une vue dans l'application est affichee a partir du ichier context contenu dans son dossier
-dans ce contexte sera defini les variables a interpreter par les vues de ce meme dossier
-->
@section('content')

@yield('context')
@if(isset($monmenu) && \App\Menu::droitLecture($monmenu))
@if(1) <a value="{{URL::to($retour)}}" class="btn btn-round btn-outline btn-default btn-sm liste_opener"><i class="icon wb-arrow-left margin-right-10" aria-hidden="true"></i>{{trans('message.retour')}}</a>
@endif
<h2>{{trans('menu.'.$monmenu)}} <span class="label label-danger">{{--App\User::getFocusedClientName()--}}</span></h2>
<div class="row">

  @if( isset($object) && $object!=null)
  	@include('backend.'.$view.'.customform')
  @endif
  @include('backend.'.$view.'.liste')

</div>
@else
  <script>window.location='/';</script>
  <?php return 0; ?>
@endif
@endsection
