<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
Liste des employés
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >{{trans('message.id')}}</th>
  <th >Photo</th>
  <th >Nom et prénoms</th>
  <th >Tel.</th>
  <th >Date de Naissance</th>
  <th >Date d'entrée</th>
  <th >Code</th>
  <th >Actif</th>
</tr>
@endslot

<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')

  @if(!empty($objects))
    @foreach($objects as $f)
      <tr>
        <td>{{$f->id}}</td>
        <td>
			<img  class="card-img-top w-full" src="@if(isset($f) && $f->photo != null ){{route('images',['type'=>'employes', 'filename'=> $f->photo])}} @else {{ backendasset('photos/placeholder.png') }} @endif" alt="Card image cap" width="50"/>
        </td>
        <td>{{$f->nom}} {{$f->prenom}}</td>
        <td>{{$f->tel}}</td>
        <td>{{$f->datenaiss}}</td>
        <td>{{$f->dateentree}}</td>
        <td>{{$f->code}}</td>
        <td>@include(backendview('includes.checkbutton'))</td>
		<td>@include('backend.includes.customactions')</td>
      </tr>
    @endforeach
  @endif
@endslot


@endcomponent
