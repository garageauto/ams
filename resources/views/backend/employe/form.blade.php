<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-md-8, col-xs-8) du formulaire -->
@slot('class_taille') col-md-10 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
@if(isset($object) && $object!=null)
  Modification des informations relatives à un employé
@else
  Enregistrement d'un employé
@endif

@endslot




<!-- mettre le contenu du formulaire ici -->
<div class="row">
  <div class="col-sm-6">
    <div class="row">
		<div class="col-sm-12">
			<label >Code d'accès<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm " value="@if(isset($object) &&  $object!=null){{$object->code}}@endif"   name="code" type="text"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >Nom<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm " value="@if(isset($object) &&  $object!=null){{$object->nom}}@endif"   name="nom" type="text"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >prénoms</label>
			<input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->prenom}}@endif"   name="prenom" type="text"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >Numero de téléphone</label>
			<input class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->tel}}@endif"   name="tel" type="text"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >date de naissance<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->datenaiss}}@endif"   name="datenaiss" type="date"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >date d'insertion (entreprise)<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm "  value="@if(isset($object) &&  $object!=null){{$object->dateentree}}@endif"   name="dateentree" type="date"  placeholder="" >
		</div>
	</div>
  </div>

  <div class="col-sm-6">

        <label >{{trans('message.image')}}</label>
        <div class="card " id="img-card">
          <img id="tmpview" class="card-img-top w-full" src="@if(isset($object) && $object->photo != null ){{route('images',['type'=>'employes', 'filename'=> $object->photo])}} @else {{ backendasset('photos/placeholder.png') }} @endif" alt="Card image cap" width="100%"/>
          <img id="preview" style="display:none;" class="card-img-top w-full" src="" alt="Card image cap" width="100%"/>
          <div class="card-block">

            <p class="card-text">
              <input   type="hidden" name="image" id="image"  value="@if(isset($object) ){{$object->photo}}@endif" >
              <input class="form-control input-sm "  type="file" name="photo"   onchange="seeimage(this)" >
            </p>
          </div>
        </div>
  </div>

</div>

<!-- mettre le contenu du formulaire ici -->
@endcomponent
