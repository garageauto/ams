<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
{{trans('message.listetypemenu')}}
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >{{trans('message.id')}}</th>
  <th >{{trans('message.profil')}}</th>
  <th >{{trans('message.description')}}</th>
  <th width="300">{{trans('message.action')}}</th>
</tr>
@endslot

<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')
@foreach($profils as $f)

  <tr>
    <td>{{$f->id}}</td>
    <td>{{$f->nomprofil}}</td>
    <td>{{$f->descprofil}}</td>
    <td>@include('backend.includes.customactions')</td>
  </tr>
@endforeach
@endslot


@endcomponent
