<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-12 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
@if(isset($object) && $object!=null)
  {{trans('message.modificationprofil')}}
@else
  {{trans('message.enregistrementprofil')}}
@endif

@endslot

<!-- mettre le contenu du formulaire ici -->
<input type="hidden" id="rules" name="rules" value="">
  <div class="row">
    <div class="col-xs-6">
      <label >{{trans('message.libelle')}}<sup>*</sup></label>
      <input required class="form-control input-sm " name="nomprofil"  type="text" placeholder="" value="@if($object!=null){{old('nomprofil',$object->nomprofil)}}@else{{old('nomprofil')}}@endif">

    </div>
    <div class="col-xs-6">
      <label >{{trans('message.description')}} </label>
      <textarea class="form-control input-sm" rows="2" name="descprofil" >@if($object!=null){{old('descprofil',$object->descprofil)}}@else{{old('descprofil')}}@endif</textarea>
    </div>
  </div>
  <br/>

  <div class="row">
    <div class="col-xs-12">
      <p  class="bg-primary" style="padding: 5px"><i class="fa fa-key"></i>{{trans('message.gestiojndroitsassocies')}}</p>


      <ul class="arbre" id="monaccordeon"  >
        <?php if($object!=null) {$object_id=$object->id;} else {$object_id=null;} ?>
        @foreach($menus as $menu)
          <li ><div class="row checkover " style="margin-top: 2px" ><div class="col-lg-8  accordion-toggle collapsed"  href="#item{{$menu->id}}" dataparent="#monaccordeon" aria-expanded="false" data-toggle="collapse">{{trans('menu.'.$menu->nommenu)}}@if (count($menu->sousmenus)>0) <i class="fa fa-arrow-down pull-right"></i> @endif</div> <div class="col-lg-4 "><?php  \App\Profil::macroInputDroits($menu->id ,'' , $object_id) ?></div> </div>

            @if (count($menu->sousmenus)>0)
              <ul id="item{{$menu->id}}"  aria-expanded="false" class="collapse" style="height: 0px;">
                @foreach($menu->sousmenus as $sousmenu)
                  <li ><div class="row checkover " style="margin-top: 2px" ><div class="col-lg-8 accordion-toggle collapsed"  href="#item{{$sousmenu->id}}" aria-expanded="false" data-toggle="collapse"><i class="fa {{$sousmenu->iconemenu}}"></i>{{trans('menu.'.$sousmenu->nommenu)}} @if (count($sousmenu->sousmenus)>0)  <i class="fa fa-arrow-down pull-right"></i> @endif</div>

                      <div class="col-lg-4 "><?php  \App\Profil::macroInputDroits($sousmenu->id , $menu->id , $object_id) ?></div> </div>
                    @if (count($sousmenu->sousmenus)>0)
                      <ul id="item{{$sousmenu->id}}"  aria-expanded="false" class="collapse" style="height: 0px;">
                        @foreach($sousmenu->sousmenus as $sousmenu2)
                          <li ><div class="row checkover" style="margin-top: 2px"><div class="col-lg-8 accordion-toggle collapsed"  href="#item{{$sousmenu2->id}}" aria-expanded="false" data-toggle="collapse"><i class="fa {{$sousmenu2->iconemenu}}"></i>{{trans('menu.'.$sousmenu2->nommenu)}} @if (count($sousmenu2->sousmenus)>0)  <i class="fa fa-arrow-down pull-right"></i> @endif</div>

                              <div class="col-lg-4"> <?php  \App\Profil::macroInputDroits($sousmenu2->id, $menu->id.'_'.$sousmenu->id , $object_id) ?></div> </div>
                            @if (count($sousmenu2->sousmenus)>0)
                              <ul id="item{{$sousmenu2->id}}"  aria-expanded="false" class="collapse" style="height: 0px;">
                                @foreach($sousmenu2->sousmenus as $sousmenu3)
                                  <li ><div class="row checkover"><div class="col-lg-8"><i class="fa {{$sousmenu3->iconemenu}}"></i>{{trans('menu.'.$sousmenu3->nommenu)}}</div> <div class="col-lg-4">
                                        <?php  \App\Profil::macroInputDroits($sousmenu3->id, $menu->id.'_'.$sousmenu->id.'_'.$sousmenu2->id , $object_id) ?>
                                      </div> </div>
                                  </li>
                                @endforeach
                              </ul>
                            @endif
                          </li>
                        @endforeach
                      </ul>
                    @endif
                  </li>
                @endforeach
              </ul>
            @endif
          </li>
        @endforeach
      </ul>


    </div>

  </div>

<!-- mettre le contenu du formulaire ici -->
<script>
  //fonctions pour mettre a jour les inputs du formulaire de gestion des droits
  //cette fonction peut etre modulee
  function mettre_a_jour_inputs()
  {

    $('.middle-pos').each( function()
    {
      $(this).removeClass('middle-pos');

      $(this).children('label').each(function()
      {
        if ($(this).hasClass('active')  )
        {
          $('#rules').val($('#rules').val()+'_'+$(this).attr('id'));
        }
      });

    }).addClass('middle-pos');

  }
  $('.formulairemodal').on('submit', function(e) {
    e.preventDefault();
    mettre_a_jour_inputs();
  });
</script>
@endcomponent
