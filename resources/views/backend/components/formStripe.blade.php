@if (\App\Menu::droitAjout($monmenu) || (\App\Menu::droitModification($monmenu) && isset($object) &&  $object!=null))

  <div class="{{$class_taille}}">
    <div class="panel ">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{$title}}
        </h3>

      </div>
      <div class="panel-body" style="padding: 10px">

        <!-- formulaire -->

        <form class=""  method="POST" action="{{ url($views) }}" resource="{{URL::to($views)}}" enctype="multipart/form-data" id="payment-form">
          @include(backendview('includes.basic_hidden_button'))
          <div class="box-body" >
            {{$slot}}
          </div><!-- /.box-body -->

          @include(backendview('includes.registerbuttonStripe'))

        </form>

      </div>
    </div>
  </div>

@endif


