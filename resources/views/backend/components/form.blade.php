@if (\App\Menu::droitAjout($monmenu) || (\App\Menu::droitModification($monmenu) && isset($object) &&  $object!=null))

  <div class="{{$class_taille}}">
    <div class="panel ">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{$title}}
        </h3>

      </div>
      <div class="panel-body" style="padding: 10px">



        <!-- formulaire -->

        <form class="form formulairemodal "  method="POST" action="@if(isset($mode)) {{ url($modeurl) }} @else {{ url($monmenu) }} @endif" resource="{{ url($monmenu) }}" enctype="multipart/form-data" id="payement-form">

          @include(backendview('includes.basic_hidden_button'))
          <div class="box-body" >
            
            {{$slot}}
          </div><!-- /.box-body -->

          @include(backendview('includes.registerbutton'))

        </form>

      </div>

    </div>
  </div>

@endif


