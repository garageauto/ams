<div class="col-md-12">
  <div class="panel panel-info ladda-panel">
    <div class="panel-heading ">
      <h3 class="panel-title sm">{{$title}}</h3>
    </div>
    <div class="panel-body " style="margin: 10px">


      <table class="table table-hover dataTable width-full {{$other_class}}" data-plugin="dataTable"  role="grid" >
        <thead>
        {{$Thead}}
        </thead>

        <tbody>
        {{$Tbody}}
        </tbody>
      </table>

    </div>
  </div>
</div>
