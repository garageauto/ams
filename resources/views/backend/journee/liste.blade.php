<!-- passer les variables du context en parametre -->
@component(backendview('components.list'),compact('monmenu','view','views','object','objects'))
@slot('title')
<!-- mettre le titre de la liste ici -->
Paramètre
@endslot

<!-- entetes de la liste -->
@slot('Thead')
<tr role="row">
  <th >Journée</th>
  <th >type</th>
  <th >Detail</th>
  <th width="300">{{trans('message.action')}}</th>
</tr>
@endslot
heuredeb_taf	heurefin_taf	heuredeb_pause	heurefin_pause	raisonsociale	logo	adresse
<!-- nous pouvons utiliser le slot other_class pour ajouter des classes au tableau contenant la liste  -->
@slot('other_class') @endslot
<!-- contenu de la liste -->
@slot('Tbody')
  @if(!empty($objects))
    @foreach($objects as $f)

		@php
			$datejour=date('Y-m-d',strtotime($f->date));
			$dateref=date('Y-m-d');
		@endphp
      <tr @if ($datejour == $dateref) class="bg-success" title="journée en cours..." @endif >
        <td>{{$f->date}}</td>
        <td>
		@if($f->type==1) <span class="label label-primary"> Journée normale </span>
		@elseif($f->type==2)  <span class="label label-warning">  Demi Journée </span>
		@else  <span class="label label-danger"> Autre </span> @endif 
		</td>
        <td>Horaires: 
		@if($f->type==2)
			<span class="label label-primary">{{$f->heuredeb_taf}} - {{$f->heurefin_taf}}</span>
		@else
			<span class="label label-primary">{{$f->heuredeb_taf}} - {{$f->heuredeb_pause}}</span>
			<span class="label label-primary">{{$f->heurefin_pause}} - {{$f->heurefin_taf}}</span>
		@endif	
		</td>
        <td>
		@if ($datejour > $dateref)
			@include('backend.includes.customactions')
		@else
			<span title="Edition impossible" >...</span>
		@endif
		</td>
      </tr>
    @endforeach
  @else
    <tr>
        <td></td>
        <td>{{trans('message.Aucun résultat')}}</td>
        <td></td>
        <td></td>
        
      </tr>
  @endif
@endslot


@endcomponent
