<!-- passer les variables du context en parametre -->
@component(backendview('components.form'),compact('monmenu','view','views','object','objects'))
<!-- class_taille represente la classe (col-m-4, col-xs-2) du formulaire -->
@slot('class_taille') col-md-6 @endslot
@slot('title')
<!-- mettre le titre du formulaire ici
la premiere condition represente le titre dans un formulaire
de modification et la seconde une insertion
-->
Planning des journées
@endslot

<!-- mettre le contenu du formulaire ici -->
<div class="row">
  <div class="col-sm-12">
    <div class="row">
		<div class="col-sm-12">
			<label >Journée<sup title="Champ obligatoire">*</sup></label>
			<input required class="form-control input-sm " name="date" value="@if(isset($object) &&  $object!=null){{$object->date}}@endif"    type="date"  placeholder="" >
		</div>
		<div class="col-sm-12">
			<label >Type de journée<sup title="Champ obligatoire">*</sup></label>
			<select name="type" id="typehours" required onchange="checkthehours()">
			<option value="1">Normale<option>
			<option value="2">Demi journée<option>
			<option value="3">Autre<option>
			</select>
		</div>
		<div class="col-sm-12 text-warning"><small>
		*Dans le cas d'une journée "Normale" les informations complémentaires (heures debut/fin) 
		signifiées dans les paramètres seront reconduites
		</small></div>
		<div class="col-sm-12 allhours invisible">
			<label >Heure de debut de journée<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               <input data-plugin="clockpicker" data-autoclose="true" name="heuredeb_taf" value="@if(isset($object) &&  $object!=null){{$object->heuredeb_taf}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
            </div>
		</div>
		<div class="col-sm-12 allhours invisible">
			<label >Heure de din de journée<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heurefin_taf" value="@if(isset($object) &&  $object!=null){{$object->heurefin_taf}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		<div class="col-sm-12 allhours coffehours invisible">
			<label >Heure de debut de pause<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heuredeb_pause" value="@if(isset($object) &&  $object!=null){{$object->heuredeb_pause}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		<div class="col-sm-12 allhours coffehours invisible">
			<label >Heure de fin de pause<sup title="Champ obligatoire">*</sup></label>
			<div class="input-group"><span class="input-group-addon"><span class="wb-time"></span></span>
               	<input data-plugin="clockpicker" data-autoclose="true" name="heurefin_pause" value="@if(isset($object) &&  $object!=null){{$object->heurefin_pause}}@endif" class="form-control input-sm "    type="text"  placeholder="" >
			</div>
		</div>
		
		
	</div>
  </div>

</div>



<!-- mettre le contenu du formulaire ici -->
@endcomponent
