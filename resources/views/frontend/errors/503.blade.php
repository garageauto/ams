<html>
	<head>
    <title>503</title>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{URL::to('resources/images/logo.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{URL::to('resources/images/logosmall.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL::to('resources/images/logosmall.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL::to('resources/images/logo114.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL::to('resources/images/logo144.png')}}">
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #666666;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">De retour bientot...</div>
			</div>
		</div>
	</body>
</html>
