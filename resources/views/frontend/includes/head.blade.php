<head>
  @include(frontendview('includes.meta'))
  @include(frontendview('includes.title'))
  @include(frontendview('includes.css'))
  @yield('css')
</head>
