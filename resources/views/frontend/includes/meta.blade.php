<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{csrf_token()}}" >

<link rel="apple-touch-icon" sizes="60x60" href="{{ backendasset('images/favicon.ico') }}">
<link rel="icon" type="image/png" 		   href="{{ backendasset('images/favicon.ico') }}" sizes="60x60" /><!-- Recommendé -->
<link rel="icon" type="image/png" 		   href="{{ backendasset('images/favicon.ico') }}" sizes="96x96" />
<link rel="icon" type="image/png" 		   href="{{ backendasset('images/favicon.ico') }}"  />


@yield('footer_meta')
@yield('content_meta')

