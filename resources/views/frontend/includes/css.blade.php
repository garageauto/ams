
<!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="../../maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{frontendasset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{frontendasset('css/paper-kit.min89bf.css?v=2.2.1')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{frontendasset('demo/demo.css')}}" rel="stylesheet" />

  <style>
	body {
		color: #333333;
		font-size: 14px;
		font-weight: 400;
		font-family: Montserrat,Helvetica,Arial,sans-serif;
	}
	.h3, .h4, .h5, .h6, .navbar, .td-name, a, button, h1, h2, h3, h4, h5, h6, input, p, select, td, textarea {
		font-weight: 400 !important;
		font-family: Montserrat,Helvetica,Arial,sans-serif;
	}
	.card-description, .description{
		color: #333333;
	}
	.card a:not(.btn) {
    color: #33c775;
	font-weight: 600;
	}
  </style>	