@extends('frontend.template')
@section('meta_title')
    <title>404 - Page non trouvée</title>
@endsection
@section('content')
<div class="container">
    <h1>ERREUR 404: Page non trouvée</h1>
    <p>Cette page est indisponible. Nous vous invitons à cliquer sur le lien <a href="{{url('/')}}">retour</a> pour retourner à l'accueil.</p>
</div>
@endsection
