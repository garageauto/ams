<nav class="navbar navbar-expand-lg fixed-top  " >
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="{{url('/')}}" rel="tooltip" title="{{trans('message.applicationname')}}" data-placement="bottom" target="">
		{{trans('message.applicationname')}}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
        <ul class="navbar-nav ml-auto">
		
          <li class=" navbar-brand">
            <a href="#">{{\App\Redempteur::affiche_date(date('Y-m-d'))}} 
			@if (\App\Journee::findDate2()) <span class="text-success">Journée en cours...</span> @endif 
			@if (!\App\Journee::findDate2()) <span class="text-danger">Journée invalide...</span> @endif 
			</a>
          </li> 
		  <li class="nav-item">
            <a class="btn btn-round btn-success" href="{{url('home')}}" >
               Connexion
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>




