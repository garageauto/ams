@extends('frontend.template')

@section('meta_title')
  <title>{{trans('message.applicationname')}}</title>
@endsection


@section('content_meta')

 
  <meta name="keywords" content="{{trans('message.keyword_meta')}}">
  <meta property="og:title" content="{{trans('message.applicationname')}}" />
  <meta name="classification" content="art">
  <meta property="og:title" content="{{trans('message.applicationname')}} "/>

@endsection


@section('content')
  <div class="container">
  <h1 class="text-center">Control des présences</h1>
  <h3  class="text-center">Cliquez sur le bouton "Notifier mon entrée" ou "Notifier ma sortie" pour une bonne comptabilisation de vos horaires de travail </h3>
  
  <div class="row">
  <div class="col-md-1 col-xs-1"> &nbsp </div>
  <div class="col-md-10 col-xs-10 "> 
  @if (!\App\Journee::findDate2()) <span class="text-danger">Journée invalide...</span>
  @else 
	  @php($objects=\App\Employe::where('actif',1)->get())
      @php(collect($objects))
      <post-presence :employes="{{$objects}}"></post-presence>
  @endif 
  
  
  </div>
  <div class="col-md-1 col-xs-1"> &nbsp </div>
  </div><!-- /.col -->
  </div>
@endsection
