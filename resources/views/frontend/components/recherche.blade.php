@extends('frontend.template')
@section('content')

    <div class="col-md-9">
        @php($class_val='col-md-4')
        <div class="row">
            <div class="nav-tabs-hoizontal" style="width: 100%" >
                <ul style="background: #15a272; padding: 10px;margin-bottom: 10px; " class="nav  nav-tabs-line text-uppercase"
                    data-plugin="nav-tabs" role="tablist">
                    <li onclick="$('.tab-pane').removeClass('active');$('#rubrik1').addClass('active')"
                        class="presentation-tab col-sm-3 text-center" role="presentation">
                        <a style="color: #ffffff; padding: 10px 10px;" href="javascript:;">
                            {{trans('message.articles')}}
                        </a>
                    </li>

                    <li onclick="$('.tab-pane').removeClass('active');$('#rubrik2').addClass('active')"
                        class="presentation-tab col-sm-3 text-center" role="presentation">
                        <a style="color: #ffffff; padding: 10px 10px;" href="javascript:;">
                            {{trans('message.salons')}}
                        </a>
                    </li>

                    <li onclick="$('.tab-pane').removeClass('active');$('#rubrik3').addClass('active')"
                        class="presentation-tab col-sm-3 text-center" role="presentation">
                        <a style="color: #ffffff; padding: 10px 10px;" href="javascript:;">
                            {{trans('message.annonceurs')}}
                        </a>
                    </li>

                    <li onclick="$('.tab-pane').removeClass('active');$('#rubrik4').addClass('active')"
                        class="presentation-tab col-sm-3 text-center" role="presentation">
                        <a style="color: #ffffff; padding: 10px 10px;" href="javascript:;">
                            {{trans('message.produits')}}
                        </a>
                    </li>


                </ul>

                <div style="padding: 0px" class="tab-content padding-vertical-15">
                    <div class="tab-pane active" id="rubrik1" role="tabpanel">
                    </div>
                    <div class="tab-pane " id="rubrik2" role="tabpanel">
                        <div class="row">
                            <post-search></post-search>
                        </div>
                        @include('frontend.components.list-salons')
                    </div>
                    <div class="tab-pane " id="rubrik3" role="tabpanel">
                        <div class="row">
                            <post-search></post-search>
                        </div>
                        @include('frontend.components.list-annonceurs')
                    </div>
                    <div class="tab-pane " id="rubrik4" role="tabpanel">
                        <div class="row">
                            <post-search></post-search>
                        </div>
                        <br/>
                        @include('frontend.components.list-produits')
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
