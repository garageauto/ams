@extends('frontend.template')

@section('meta_title')
    <title>{{trans('message.applicationname')}} - {{$article->titre}}</title>
@endsection


@section('content_meta')


    <meta name="description" content="{{htmlentities(strval(substr($article->description, 0, 200) . '...'), ENT_NOQUOTES)}}">
    @php($split_title=explode(' ',$article->titre))
    <meta name="keywords" content="@foreach($split_title as $split_t) {{$split_t}}, @endforeach">
    <meta property="og:title" content="{{$article->titre}}" />

    <meta property="og:description" content="{{htmlentities(strval(substr($article->description, 0, 200) . '...'), ENT_NOQUOTES)}}" />
    <meta name="classification" content="{{$article->titre}}">
    <meta property="og:title" content="{{trans('message.applicationname')}} - {{$article->titre}}"/>
    @if(isset($article->images) && isset($article->images[0]) && $article->images[0]!=null && $article->images[0]->nompiece!=null)
        <link rel="apple-touch-startup-image" href="{{url('/download-mini-picture-40/files/'.$article->images[0]->nompiece)}}" />
        <meta property="og:image"  content="{{url('/download-mini-picture-40/files/'.$article->images[0]->nompiece)}}"/>
    @endif
    <meta property="og:description" content="{{htmlentities(strval(substr($article->description, 0, 200) . '...'), ENT_NOQUOTES)}}"/>

@endsection


@section('content')
  <div class="section section-white">
        <div class="container">
          
          
          <div class="article">
            <div class="row">
              <div class="col-md-8 ml-auto mr-auto">
                <div class="card card-blog card-plain text-justify ">
				<div class="row">
					<div class="col-md-12">
				 <div class=" ">
                    <a href="#pablo">
					@if(isset($article->images) && isset($article->images[0]) && $article->images[0]!=null && $article->images[0]->nompiece!=null)
                      <img class="img img-raised" src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}" width="100%">
					@endif
                    </a>
                  </div>
					</div>
					<div class="col-md-12">
				<div class="">
                    
                    <a href="javascrip: void(0);">
                      <h3 style="">{{$article->titre}}</h3>
                    </a>
                    <div class="card-description" style="font-weight: 500"><?php echo $article->descriptioncomplete; ?></div>
                  </div>
					</div>
				</div>
                 
                  
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
@endsection
