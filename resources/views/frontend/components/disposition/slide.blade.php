@if(isset($articles) && count($articles)>0 && isset($menu))
	
<div class="row" id="{{$menu->nommenu}}">
        <div class="col-md-12 ml-auto mr-auto">
          <div class="card card-raised page-carousel" style="margin:0px">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
			  @foreach($articles as $article)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->iteration}}" @if($loop->iteration==1) class="active" @endif></li>
			  @endforeach	
              </ol>
			  
              <div class="" role="listbox">
				@foreach($articles as $article)
					<div class="carousel-item @if($loop->iteration==1) active @endif">
					@if(count($article->images)>0)
						<img class="d-block img-fluid" width="100%" src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}" alt="{{$article->titre}}">
					@endif
					<div class="carousel-caption d-none d-md-block">
						<h1 style="text-shadow: 2px 2px #555555;">{{$article->titre}}</h2>
						
					</div>
					</div>
				@endforeach
			  </div>
			  
              <a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="btn  btn-success"> < </span>
                <span class="sr-only">Precedant</span>
              </a>
              <a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="btn  btn-success"> > </span>
                <span class="sr-only">Suivant</span>
              </a>
            </div>
          </div>
        </div>
      </div>	

	  @endif