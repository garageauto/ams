@if(isset($articles) && count($articles)>0 && isset($menu))
	
	<div class="section  "  id="{{$menu->nommenu}}">
    <div class="container text-center">
    <div class="row justify-content-md-center">
	@foreach($articles as $article)
      <div class="col-md-12 col-lg-8">
	  <h2 class="title">{{$article->titre}}</h2>
      <h5 class="description">
	  {{$article->description}}
	  </h5>
      </div>
	@endforeach	
        
        </div>
			  
    </div>
    </div>
		
	  @endif