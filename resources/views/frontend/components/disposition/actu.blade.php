@if(isset($articles) && count($articles)>0 && isset($menu))
	
<div class="section section-white" id="{{$menu->nommenu}}">

        <div class="container">
          @foreach($articles as $article)
          <div class="article">

            <div class="row">
              <div class="col-md-8 ml-auto mr-auto">
                <div class="card card-blog card-plain  ">
				<div class="row">
					<div class="col-md-4">
				 <div class=" ">
                    <a href="#pablo">
                      @if(count($article->images)>0)
						  <img class="img img-raised" src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}">
                      @endif
					</a>
                  </div>
					</div>
					<div class="col-md-8">
				<div class="">
                    
                    <a href="javascrip: void(0);" title="{{$article->titre}}">
                      <h3 style="margin:0px">{{htmlentities(strval(substr($article->titre, 0, 50) . '...'), ENT_NOQUOTES)}}</h3>
                    </a>
                    <div class="card-description text-justify">
                      <p>{{htmlentities(strval(substr($article->description, 0, 200) . '...'), ENT_NOQUOTES)}}</p>
					</div>
                  </div>
                  <a href="{{url('article/'.$article->code)}}" class="btn btn-success btn-round btn-sm">Lire la suite</a>
					</div>
				</div>
                 
                  
                </div>
              </div>
            </div>
          </div>
          <hr>
          <br/>
		  @endforeach
        </div>
      </div>
	  @endif