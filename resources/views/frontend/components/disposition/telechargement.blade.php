@if(isset($articles) && count($articles)>0 && isset($menu))
	
<div class="section section-white" id="{{$menu->code}}">

        <div class="container">
          @foreach($articles as $article)
          <div class="article">

            <div class="row">
              <div class="col-md-8 ml-auto mr-auto">
                <div class="card card-blog card-plain text-justify ">
				<div class="row">
					<div class="col-md-8">
						{{$article->titre}}
					</div>
					<div class="col-md-4">
					@if(count($article->fichiers)>0)
					<a href="{{url('/storage/app/public/files/'.$article->fichiers[0]->nompiece)}}" class="btn btn-success btn-round btn-sm">Télécharger</a>
					@endif
					</div>
				</div>
                 
                  
                </div>
              </div>
            </div>
          </div>
          <hr>
          <br/>
		  @endforeach
        </div>
      </div>
	  @endif