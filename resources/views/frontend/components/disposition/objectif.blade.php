@if(isset($articles) && count($articles)>0 && isset($menu))
	

	
<div class="section section-dark text-justify" id="{{$menu->nommenu}}">
    <div class="container tim-container">
	
      @foreach($articles as $article)
	  <div class="row">
	  <div class="col-md-4">
	  @if(count($article->images)>0)
				<img src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}" width="100%"/>
	  @endif
	  </div>
	  <div class="col-md-8">
	  <div class="card-body" style="padding:0px">
                <h2 style="margin:0px">
                  <a href="#pablo" style="color:#FFFFFF" >{{$article->titre}}</a>
				 </h2>
                <div class=""  style="color:#FFFFFF !important">
					<?php echo $article->descriptioncomplete; ?>
				</div>
                <br/>
              </div>
	  </div>
	  </div>
		@endforeach	  
    </div>
		
	  </div>

	  @endif