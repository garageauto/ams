@if(isset($articles) && count($articles)>0 && isset($menu))
	

  <div class="section section-white "  id="{{$menu->nommenu}}">
	
	
	
    <div class="container tim-container">
	<h2 class="card-title">
     <a href="#pablo" >{{$menu->fr}}</a>
	</h2>
    <div class="row">
	@foreach($articles as $article)
      
      <div class="col-md-2 col-sm-6 col-xs-4" title="Cliquez pour accéder au site web">
	  <a target="_blank" href="{{$article->description}}">
          <div class="card card-blog">
			<div class="card-body">
			@if(count($article->images)>0)
				<img src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}" width="100%"/>
			@endif
			<h6 class="author pull-left">{{$article->titre}}</h6>
             
            </div>
           </div>
		   </a>
        </div>
	@endforeach	 	  
    </div>
    </div>
    </div>

	  @endif