@if(isset($articles) && count($articles)>0 && isset($menu))
	
<div class="team-1" id="{{$menu->nommenu}}">
      <div class="container">
        <div class="row">
          <div class="col-md-12 ml-auto mr-auto text-center">
            <h2 class="title">{{$menu->fr}}</h2>
            <h5 class="description">{{$menu->description}} </h5>
          </div>
        </div>
        <div class="row">
		@foreach($articles as $article)
          <div class="col-md-3">
            <div class="card card-profile card-plain">
              <div class="card-avatar">
                <a href="#avatar">
                  @if(count($article->images)>0)
					  <img src="{{url('/download-picture/files/'.$article->images[0]->nompiece)}}" alt="...">
				  @endif
                </a>
              </div>
              <div class="card-body">
                <a href="#paper-kit">
                  <div class="author">
                    <h4 class="card-title">{{$article->titre}}</h4>
                    <h6 class="card-category text-muted">{{$article->description}}</h6>
                  </div>
                </a>
                
              </div>
              
            </div>
          </div> 
		 @endforeach  
            
			</div>
          </div>
        </div>

	  @endif