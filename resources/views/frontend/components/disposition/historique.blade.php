@if(isset($articles) && count($articles)>0 && isset($menu))
	

	
<div class="section section-gray text-justify" id="{{$menu->nommenu}}">
    <div class="container tim-container">
      @foreach($articles as $article)
			  <div class="card-body">
                <h2 class="card-title">
                  <a href="#pablo" >{{$article->titre}}</a>
				 </h2>
                <p class="card-description">
               <?php echo $article->descriptioncomplete; ?>
				</p>
                <br/>
              </div>
		@endforeach	  
    </div>
		
	  </div>

	  @endif