<!DOCTYPE html>
<html lang="fr">
@include('frontend.includes.head')
<body>

@include('frontend.components.navbar')
@include('frontend.components.slide')
<!-- slide -->
  <div class="body" id="app" >
  <br><br><br>
  @if (count($errors) > 0)
    	<div class="alert alert-danger" style="z-index: 9000;position: fixed; top:0px; left:0px">
    		<strong>Désolé!</strong> Les informations saisies sont invalides.<br><br>
    		<ul>
    			@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
    			@endforeach
    		</ul>
    	</div>
    @endif
    @if (Session::has('flash_notice'))
        <div class="alert alert-success" style="z-index: 9000;position: fixed; top:0px; left:0px">
        	{{Session::get('flash_notice')}}
        </div>
    @endif
    @if (Session::has('flash_error'))
        <div class="alert alert-warning" style="z-index: 9000;position: fixed; top:0px; left:0px">
        	{{Session::get('flash_error')}}
        </div>
    @endif
          @yield('content')
  </div>

  @include('frontend.components.footer')


</body>

@include('frontend.includes.all_used_js')
@include('backend.includes.upload_script')
</html>
