class malang{
  constructor() {
    this.state={
      _token:'',
      locale:'fr-tg',
      http:'http://localhost:8000',
      lang_country:[], //devrait etre un tableau contenant la langue et le pays
      lang:'fr',
      lang_id:Number,
      selected_menu:0,
      selected_ssmenu:0,
      country:'tg',
      menu:[],
      message:[],
      annoncepreview:[],
      abuspreview:[],
      validation:[],
      commentaires:[],
      categorieproduit_id:0,
      menulateral_id:Number,
      motrecherche:'',
      menu_lateral_id:Number,
      nbr_navbar_plus:0,
      is_mobile:0,
      regEmail:[],
      user:[],
      produit:[],
      logged_in:0,
      useragent_badge:String,
      userlogin:String,
      userimg:String,
    }
  }
  SET_LOCALE(state, msg){
    state.locale=msg;
    state.lang_country=state.locale.split("-");
    state.lang=state.lang_country[0];
    state.country=state.lang_country[1];
  }
  SET_MENU(state, msg){
    state.menu=msg;
  }
  SET_MESSAGE(state, msg){
    state.message=msg;
  }
  SET_VALIDATION(state, msg){
    state.validation=msg;
  }
  SET_CATEGORIEPORDUITS(state, msg){
    state.categorieproduit_id=msg;
  }
  SET_MENULATERAL(state, msg){
    state.menulateral_id=msg;
  }
  SET_MOTRECHERCHE(state, msg){
    state.motrecherche=msg;
  }
  SET_COMMENTAIRE(state, msg){
    state.commentaire=msg;
  }
  SET_LANG_ID(state, msg){
    state.lang_id=msg;
  }
  SET_TOKEN(state, msg){
    state._token=msg;
  }
  SET_HTTP(state, msg){
    state.http=msg;
  }
  SET_SELECTED_MENU(state, msg){
    state.selected_menu=msg;
  }
  SET_SELECTED_SSMENU(state, msg){
    state.selected_ssmenu=msg;
  }

  SET_MENU_LATERAL_ID(state, msg){
    state.menu_lateral_id=msg;
  }

  SET_USER(state, msg){
    state.user=msg;
  }
  SET_PRODUIT(state, msg){
    state.produit=msg;
  }

  SET_NBR_NAVBAR_PLUS(state, msg){
    state.nbr_navbar_plus=msg;
  }


  SET_USERAGENT_BADGE(state, msg){
    state.useragent_badge=msg;
  }

  SET_USERLOGIN(state, msg){
    state.userlogin=msg;
  }

  SET_USERIMG(state, msg){
    state.userimg=msg;
  }

  SET_LOGGED_ID(state, msg){
    state.logged_in=msg;
  }

  SET_IS_MOBILE(state, msg){
    state.is_mobile=msg;
  }

  IS_MAIL(state, msg){
      state.regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$','i');
      return state.regEmail.test(msg);
  }

  //functions
  htmldecode(s){
  window.HTML_ESC_MAP = {
    "nbsp":" ","iexcl":"¡","cent":"¢","pound":"£","curren":"¤","yen":"¥","brvbar":"¦","sect":"§","uml":"¨","copy":"©","ordf":"ª","laquo":"«","not":"¬","reg":"®","macr":"¯","deg":"°","plusmn":"±","sup2":"²","sup3":"³","acute":"´","micro":"µ","para":"¶","middot":"·","cedil":"¸","sup1":"¹","ordm":"º","raquo":"»","frac14":"¼","frac12":"½","frac34":"¾","iquest":"¿","Agrave":"À","Aacute":"Á","Acirc":"Â","Atilde":"Ã","Auml":"Ä","Aring":"Å","AElig":"Æ","Ccedil":"Ç","Egrave":"È","Eacute":"É","Ecirc":"Ê","Euml":"Ë","Igrave":"Ì","Iacute":"Í","Icirc":"Î","Iuml":"Ï","ETH":"Ð","Ntilde":"Ñ","Ograve":"Ò","Oacute":"Ó","Ocirc":"Ô","Otilde":"Õ","Ouml":"Ö","times":"×","Oslash":"Ø","Ugrave":"Ù","Uacute":"Ú","Ucirc":"Û","Uuml":"Ü","Yacute":"Ý","THORN":"Þ","szlig":"ß","agrave":"à","aacute":"á","acirc":"â","atilde":"ã","auml":"ä","aring":"å","aelig":"æ","ccedil":"ç","egrave":"è","eacute":"é","ecirc":"ê","euml":"ë","igrave":"ì","iacute":"í","icirc":"î","iuml":"ï","eth":"ð","ntilde":"ñ","ograve":"ò","oacute":"ó","ocirc":"ô","otilde":"õ","ouml":"ö","divide":"÷","oslash":"ø","ugrave":"ù","uacute":"ú","ucirc":"û","uuml":"ü","yacute":"ý","thorn":"þ","yuml":"ÿ","fnof":"ƒ","Alpha":"Α","Beta":"Β","Gamma":"Γ","Delta":"Δ","Epsilon":"Ε","Zeta":"Ζ","Eta":"Η","Theta":"Θ","Iota":"Ι","Kappa":"Κ","Lambda":"Λ","Mu":"Μ","Nu":"Ν","Xi":"Ξ","Omicron":"Ο","Pi":"Π","Rho":"Ρ","Sigma":"Σ","Tau":"Τ","Upsilon":"Υ","Phi":"Φ","Chi":"Χ","Psi":"Ψ","Omega":"Ω","alpha":"α","beta":"β","gamma":"γ","delta":"δ","epsilon":"ε","zeta":"ζ","eta":"η","theta":"θ","iota":"ι","kappa":"κ","lambda":"λ","mu":"μ","nu":"ν","xi":"ξ","omicron":"ο","pi":"π","rho":"ρ","sigmaf":"ς","sigma":"σ","tau":"τ","upsilon":"υ","phi":"φ","chi":"χ","psi":"ψ","omega":"ω","thetasym":"ϑ","upsih":"ϒ","piv":"ϖ","bull":"•","hellip":"…","prime":"′","Prime":"″","oline":"‾","frasl":"⁄","weierp":"℘","image":"ℑ","real":"ℜ","trade":"™","alefsym":"ℵ","larr":"←","uarr":"↑","rarr":"→","darr":"↓","harr":"↔","crarr":"↵","lArr":"⇐","uArr":"⇑","rArr":"⇒","dArr":"⇓","hArr":"⇔","forall":"∀","part":"∂","exist":"∃","empty":"∅","nabla":"∇","isin":"∈","notin":"∉","ni":"∋","prod":"∏","sum":"∑","minus":"−","lowast":"∗","radic":"√","prop":"∝","infin":"∞","ang":"∠","and":"∧","or":"∨","cap":"∩","cup":"∪","int":"∫","there4":"∴","sim":"∼","cong":"≅","asymp":"≈","ne":"≠","equiv":"≡","le":"≤","ge":"≥","sub":"⊂","sup":"⊃","nsub":"⊄","sube":"⊆","supe":"⊇","oplus":"⊕","otimes":"⊗","perp":"⊥","sdot":"⋅","lceil":"⌈","rceil":"⌉","lfloor":"⌊","rfloor":"⌋","lang":"〈","rang":"〉","loz":"◊","spades":"♠","clubs":"♣","hearts":"♥","diams":"♦","\"":"quot","amp":"&","lt":"<","gt":">","OElig":"Œ","oelig":"œ","Scaron":"Š","scaron":"š","Yuml":"Ÿ","circ":"ˆ","tilde":"˜","ndash":"–","mdash":"—","lsquo":"‘","rsquo":"’","sbquo":"‚","ldquo":"“","rdquo":"”","bdquo":"„","dagger":"†","Dagger":"‡","permil":"‰","lsaquo":"‹","rsaquo":"›","euro":"€"};
  if(!window.HTML_ESC_MAP_EXP)
    window.HTML_ESC_MAP_EXP = new RegExp("&("+Object.keys(HTML_ESC_MAP).join("|")+");","g");
  return s?s.replace(window.HTML_ESC_MAP_EXP,function(x){
      return HTML_ESC_MAP[x.substring(1,x.length-1)]||x;
    }):s;
}
}


//exportation de la classe pour usage dans les autres composantes
export let message_lang=new malang();
