/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//require('../frontend/slick/slick');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//importer le store premettant de gerer les langues...
import {message_lang} from './store/store';

//importation des composantes

//text
Vue.component('mon-text', require('./components/montext/Mon-text.vue'));
Vue.component('mon-lien', require('./components/montext/Mon-lien.vue'));
Vue.component('mon-bouton', require('./components/montext/Mon-bouton.vue'));
Vue.component('post-img', require('./components/post/Post-img.vue'));
Vue.component('post-img-empty', require('./components/post/Post-img-empty.vue'));
Vue.component('post-presence', require('./components/post/Post-presence.vue'));
Vue.component('post-loader', require('./components/post/Post-loader.vue'));

const app = new Vue({
  el: '#app',
  data: function(){
    return {malang:message_lang.state}
  },

  created: function() {
    
    },

  mounted: function() {

  }
});

